package com.epam.hospital.dao.impl;

import com.epam.hospital.dao.*;
import com.epam.hospital.dao.connection.ConnectionPool;
import com.epam.hospital.dao.exception.ConnectionPoolException;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.*;
import com.epam.hospital.domain.view.AppointmentVO;
import com.epam.hospital.domain.view.DoctorAppointment;
import com.epam.hospital.domain.view.PatientAppointment;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class SqlDaoAppointmentTest {

    private static ConnectionPool pool;
    private static DaoAppointment appointmentDao;
    private static DaoDoctor doctorDao;
    private static DaoUser userDao;
    private static DaoPerson personDao;
    private static DaoMedicalCard medicalCardDao;

    private static User user;
    private static Person personPatient;
    private static Person personDoctor;
    private static Doctor doctor;
    private static MedicalCard medicalCard;
    private static Appointment appointment;

    private static final String UNKNOWN_SPECIALITY = "unknown speciality";
    private static final String ALLERGIST_SPECIALITY_NAME = "allergist";
    private static final int UNKNOWN_DOCTOR_ID = -1;
    private static final long ALLERGIST_SPECIALITY_ID = 1L;
    private static final long ADMIN_ROLE_ID = 1L;
    private static final long UNKNOWN_MEDICAL_CARD_ID = -1L;
    private static final long UNKNOWN_APPOINTMENT_ID = -1L;
    private static final LocalDate CURRENT_DATE = LocalDate.now();

    @BeforeClass
    public static void init() throws ConnectionPoolException, DaoException {
        pool = ConnectionPool.getInstance();
        pool.init();

        appointmentDao = SqlDaoAppointment.getInstance();
        doctorDao = SqlDaoDoctor.getInstance();
        userDao = SqlDaoUser.getInstance();
        personDao = SqlDaoPerson.getInstance();
        medicalCardDao = SqlDaoMedicalCard.getInstance();

        insertUser();
        insertPersonPatient();
        insertPersonDoctor();
        insertDoctor();
        insertMedicalCard();
        insertAppointment();
    }

    private static void insertUser() throws DaoException {
        user = new User();
        user.setPassword(DigestUtils.md5Hex("password"));
        user.setLogin("admin");
        user.setIdRole(ADMIN_ROLE_ID);

        final long userId = userDao.insert(user);
        user.setId(userId);
    }

    private static void insertPersonPatient() throws DaoException {
        personPatient = new Person();
        personPatient.setName("name");
        personPatient.setSecondName("secondName");
        personPatient.setPatronymic("patronymic");
        personPatient.setDateOfBirth(CURRENT_DATE);
        personPatient.setSex("women");

        final long personId = personDao.insert(personPatient);
        personPatient.setId(personId);
    }

    private static void insertPersonDoctor() throws DaoException {
        personDoctor = new Person();
        personDoctor.setName("name");
        personDoctor.setSecondName("secondName");
        personDoctor.setPatronymic("patronymic");
        personDoctor.setDateOfBirth(CURRENT_DATE);
        personDoctor.setSex("women");

        final long personId = personDao.insert(personDoctor);
        personDoctor.setId(personId);
    }

    private static void insertDoctor() throws DaoException {
        doctor = new Doctor();
        doctor.setId(user.getId());
        doctor.setIdPerson(personDoctor.getId());
        doctor.setIdSpeciality(ALLERGIST_SPECIALITY_ID);
        doctor.setRoomNumber(1);

        doctorDao.insert(doctor);
    }

    private static void insertMedicalCard() throws DaoException {
        medicalCard = new MedicalCard();
        medicalCard.setBloodType("1 positive");
        medicalCard.setHeightPatient(1F);
        medicalCard.setIdPerson(personPatient.getId());
        medicalCard.setWeightPatient(1F);

        final long medicalCardId = medicalCardDao.insert(medicalCard);
        medicalCard.setId(medicalCardId);
    }

    private static void insertAppointment() throws DaoException {
        appointment = new Appointment();
        appointment.setDate(CURRENT_DATE);
        appointment.setIdDoctor(doctor.getId());
        appointment.setIdMedicalCard(medicalCard.getId());
        appointment.setTime(LocalTime.now());

        final long appointmentId = appointmentDao.insert(appointment);
        appointment.setId(appointmentId);
    }

    @AfterClass
    public static void dispose() throws ConnectionPoolException, DaoException {
        appointmentDao.deleteAppointment(appointment.getId());
        personDao.delete(personPatient);
        userDao.delete(user);
        personDao.delete(personDoctor);
        pool.dispose();
    }

    @Test
    public void getAppointmentsSuccess() throws Exception {
        final int zeroSize = 0;
        final List<Appointment> appointments = appointmentDao.getAppointments(CURRENT_DATE, doctor.getId());

        Assert.assertNotEquals(zeroSize, appointments.size());
    }

    @Test
    public void getAppointmentsFailed() throws Exception {
        final int zeroSize = 0;
        final List<Appointment> appointments = appointmentDao.getAppointments(CURRENT_DATE, UNKNOWN_DOCTOR_ID);

        Assert.assertEquals(zeroSize, appointments.size());
    }

    @Test
    public void isWrittenReceiptSuccess() throws Exception {
        Assert.assertTrue(appointmentDao.isWrittenReceipt(appointment));
    }

    @Test
    public void isWrittenReceiptFailed() throws Exception {
        appointment.setIdDoctor(UNKNOWN_DOCTOR_ID);
        Assert.assertFalse(appointmentDao.isWrittenReceipt(appointment));

        appointment.setIdDoctor(doctor.getId());
    }

    @Test
    public void isBookingAppointmentSuccess() throws Exception {
        Assert.assertTrue(appointmentDao.isBookingAppointment(appointment.getId()));
    }

    @Test
    public void isBookingAppointmentFailed() throws Exception {
        Assert.assertFalse(appointmentDao.isBookingAppointment(UNKNOWN_APPOINTMENT_ID));
    }

    @Test
    public void getAppointmentVOSuccess() throws Exception {
        final int failedUnexpectedId = 0;
        final AppointmentVO returnedAppointment = appointmentDao.getAppointmentVO(appointment.getId());

        Assert.assertNotEquals(failedUnexpectedId, returnedAppointment);
    }

    @Test
    public void getAppointmentVOFailed() throws Exception {
        final int failedExpectedId = 0;
        final AppointmentVO returnedAppointment = appointmentDao.getAppointmentVO(UNKNOWN_APPOINTMENT_ID);

        Assert.assertEquals(failedExpectedId, returnedAppointment.getId());
    }

    @Test
    public void getPatientAppointmentsSuccess() throws Exception {
        final long zeroSize = 0;
        List<PatientAppointment> patientAppointments = appointmentDao.getPatientAppointments(doctor.getId(), CURRENT_DATE);

        Assert.assertNotEquals(zeroSize, patientAppointments.size());
    }

    @Test
    public void getPatientAppointmentsFailed() throws Exception {
        final long zeroSize = 0;
        List<PatientAppointment> patientAppointments = appointmentDao.getPatientAppointments(UNKNOWN_DOCTOR_ID, CURRENT_DATE);

        Assert.assertEquals(zeroSize, patientAppointments.size());
    }

    @Test
    public void getDoctorAppointmentsSuccess() throws Exception {
        final long zeroSize = 0;
        List<DoctorAppointment> doctorAppointments = appointmentDao.getDoctorAppointments(medicalCard.getId());

        Assert.assertNotEquals(zeroSize, doctorAppointments.size());
    }

    @Test
    public void getDoctorAppointmentsFailed() throws Exception {
        final long zeroSize = 0;
        List<DoctorAppointment> doctorAppointments = appointmentDao.getDoctorAppointments(UNKNOWN_MEDICAL_CARD_ID);

        Assert.assertEquals(zeroSize, doctorAppointments.size());
    }

    @Test
    public void getAppointmentSuccess() throws Exception {
        final long failUnexpectedId = 0L;
        final Appointment returnedAppointment = appointmentDao.getAppointment(appointment.getId());

        Assert.assertNotEquals(failUnexpectedId, returnedAppointment.getId());
    }

    @Test
    public void getAppointmentFailed() throws Exception {
        final long failExpectedId = 0L;
        final Appointment returnedAppointment = appointmentDao.getAppointment(UNKNOWN_APPOINTMENT_ID);

        Assert.assertEquals(failExpectedId, returnedAppointment.getId());
    }
}
