package com.epam.hospital.dao.impl;

import com.epam.hospital.dao.DaoMedicalCard;
import com.epam.hospital.dao.DaoPerson;
import com.epam.hospital.dao.connection.ConnectionPool;
import com.epam.hospital.dao.exception.ConnectionPoolException;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.MedicalCard;
import com.epam.hospital.domain.bean.Person;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.LocalDate;

public class SqlDaoMedicalCardTest {

    private static ConnectionPool pool;
    private static DaoPerson personDao;
    private static DaoMedicalCard medicalCardDao;
    private static Person person;
    private static MedicalCard medicalCard;

    private static final long UNKNOWN_MEDICAL_CARD_ID = -1L;
    private static final long UNKNOWN_PERSON_ID = -1L;

    @BeforeClass
    public static void init() throws ConnectionPoolException, DaoException {
        pool = ConnectionPool.getInstance();
        pool.init();

        personDao = SqlDaoPerson.getInstance();
        medicalCardDao = SqlDaoMedicalCard.getInstance();

        insertPerson();
        insertMedicalCard();
    }

    private static void insertPerson() throws DaoException {
        person = new Person();
        person.setName("name");
        person.setSecondName("second name");
        person.setPatronymic("patronymic");
        person.setDateOfBirth(LocalDate.now());
        person.setSex("women");

        final long personId = personDao.insert(person);
        person.setId(personId);
    }

    private static void insertMedicalCard() throws DaoException {
        medicalCard = new MedicalCard();
        medicalCard.setBloodType("1 positive");
        medicalCard.setHeightPatient(1F);
        medicalCard.setIdPerson(person.getId());
        medicalCard.setWeightPatient(1F);

        final long medicalCardId = medicalCardDao.insert(medicalCard);
        medicalCard.setId(medicalCardId);
    }

    @AfterClass
    public static void dispose() throws ConnectionPoolException, DaoException {
        personDao.delete(person);
        pool.dispose();
    }

    @Test
    public void getMedicalCardSuccess() throws Exception {
        final MedicalCard returnedMedicalCard = medicalCardDao.getMedicalCard(medicalCard.getId());
        Assert.assertEquals(returnedMedicalCard, medicalCard);
    }

    @Test
    public void getMedicalCardFailed() throws Exception {
        final long failExpectedId = 0L;
        final MedicalCard returnedMedicalCard = medicalCardDao.getMedicalCard(UNKNOWN_MEDICAL_CARD_ID);

        Assert.assertEquals(failExpectedId, returnedMedicalCard.getId());
    }

    @Test
    public void getIdMedicalCardSuccess() throws Exception {
        final long returnedMedicalCardId = medicalCardDao.getIdMedicalCard(person.getId());
        Assert.assertEquals(medicalCard.getId(), returnedMedicalCardId);
    }

    @Test
    public void getIdMedicalCardFailed() throws Exception {
        final long failedExpectedId = 0L;
        final long returnedMedicalCardId = medicalCardDao.getIdMedicalCard(UNKNOWN_PERSON_ID);

        Assert.assertEquals(failedExpectedId, returnedMedicalCardId);
    }
}
