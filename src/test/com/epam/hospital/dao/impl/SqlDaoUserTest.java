package com.epam.hospital.dao.impl;

import com.epam.hospital.dao.DaoUser;
import com.epam.hospital.dao.connection.ConnectionPool;
import com.epam.hospital.dao.exception.ConnectionPoolException;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.User;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.*;

public class SqlDaoUserTest {

    private static ConnectionPool pool;
    private static DaoUser userDao;
    private static User user;

    private static final long ADMIN_ROLE_ID = 1L;

    @BeforeClass
    public static void init() throws ConnectionPoolException {
        pool = ConnectionPool.getInstance();
        pool.init();

        userDao = SqlDaoUser.getInstance();

        user = new User();
        user.setPassword(DigestUtils.md5Hex("password"));
        user.setLogin("admin");
        user.setIdRole(ADMIN_ROLE_ID);
    }

    @AfterClass
    public static void dispose() throws ConnectionPoolException, DaoException {
        userDao.delete(user);
        pool.dispose();
    }

    @Test
    public void testFindUserSuccess() throws Exception {
        userDao.insert(user);

        final String testLogin = "admin";
        final String testPassword = DigestUtils.md5Hex("password");

        User findUser = userDao.findUser(testLogin, testPassword);
        Assert.assertNotNull(findUser);
    }

    @Test
    public void testFindUserFailed() throws Exception {
        final int failedExpectedId = 0;
        final String testLogin = "test";
        final String testPassword = DigestUtils.md5Hex("test");

        User findUser = userDao.findUser(testLogin, testPassword);
        Assert.assertEquals(failedExpectedId, findUser.getId());
    }
}
