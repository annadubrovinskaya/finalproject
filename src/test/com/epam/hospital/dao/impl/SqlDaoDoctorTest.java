package com.epam.hospital.dao.impl;

import com.epam.hospital.dao.*;
import com.epam.hospital.dao.connection.ConnectionPool;
import com.epam.hospital.dao.exception.ConnectionPoolException;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.Doctor;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.bean.User;
import com.epam.hospital.domain.view.DoctorVO;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.LocalDate;
import java.util.List;

public class SqlDaoDoctorTest {

    private static ConnectionPool pool;
    private static DaoUser userDao;
    private static DaoPerson personDao;
    private static DaoDoctor doctorDao;
    private static User user;
    private static Person person;
    private static Doctor doctor;

    private static final String UNKNOWN_SPECIALITY = "unknown speciality";
    private static final String ALLERGIST_SPECIALITY_NAME = "allergist";
    private static final int UNKNOWN_DOCTOR_ID = -1;
    private static final long ALLERGIST_SPECIALITY_ID = 1L;
    private static final long ADMIN_ROLE_ID = 1L;


    @BeforeClass
    public static void init() throws ConnectionPoolException, DaoException {
        pool = ConnectionPool.getInstance();
        pool.init();

        userDao = SqlDaoUser.getInstance();
        personDao = SqlDaoPerson.getInstance();
        doctorDao = SqlDaoDoctor.getInstance();

        insertUser();
        insertPerson();
        insertDoctor();
    }

    private static void insertUser() throws DaoException {
        user = new User();
        user.setIdRole(ADMIN_ROLE_ID);
        user.setLogin("login");
        user.setPassword("password");

        final long userId = userDao.insert(user);
        user.setId(userId);
    }

    private static void insertPerson() throws DaoException {
        person = new Person();
        person.setName("name");
        person.setSecondName("second name");
        person.setPatronymic("patronymic");
        person.setDateOfBirth(LocalDate.now());
        person.setSex("women");

        final long personId = personDao.insert(person);
        person.setId(personId);
    }

    private static void insertDoctor() throws DaoException {
        doctor = new Doctor();
        doctor.setId(user.getId());
        doctor.setIdPerson(person.getId());
        doctor.setIdSpeciality(ALLERGIST_SPECIALITY_ID);
        doctor.setRoomNumber(1);

        doctorDao.insert(doctor);
    }

    @AfterClass
    public static void dispose() throws DaoException, ConnectionPoolException {
        userDao.delete(user);
        personDao.delete(person);
        pool.dispose();
    }

    @Test
    public void getDoctorsWithoutSchedule() throws Exception {
        final int zeroSize = 0;
        final List<DoctorVO> doctors = doctorDao.getDoctorsWithoutSchedule();

        Assert.assertNotEquals(zeroSize, doctors.size());
    }

    @Test
    public void hasSpecialityScheduleSuccess() throws Exception {
        final boolean result = doctorDao.hasSpecialitySchedule(ALLERGIST_SPECIALITY_NAME);
        Assert.assertTrue(result);
    }

    @Test
    public void hasSpecialityScheduleFail() throws Exception {
        final boolean result = doctorDao.hasSpecialitySchedule(UNKNOWN_SPECIALITY);
        Assert.assertFalse(result);
    }

    @Test
    public void getDoctorIdSuccess() throws Exception {
        final long doctorId = doctorDao.getDoctorId(person, ALLERGIST_SPECIALITY_NAME);
        Assert.assertEquals(user.getId(), doctorId);
    }

    @Test
    public void getDoctorIdFailed() throws Exception {
        final int failedExpectedId = 0;
        final long doctorId = doctorDao.getDoctorId(person, UNKNOWN_SPECIALITY);
        Assert.assertEquals(failedExpectedId, doctorId);
    }

    @Test
    public void getRoomNumberSuccess() throws Exception {
        final int roomNumber = doctorDao.getRoomNumber(doctor.getId());
        Assert.assertEquals(doctor.getRoomNumber(), roomNumber);
    }

    @Test
    public void getRoomNumberFailed() throws Exception {
        final int failedExpectedId = 0;
        final int roomNumber = doctorDao.getRoomNumber(UNKNOWN_DOCTOR_ID);
        Assert.assertEquals(failedExpectedId, roomNumber);
    }

    @Test
    public void findDoctorSpecialitySuccess() throws Exception {
        final String speciality = doctorDao.findDoctorSpeciality(doctor.getId());
        Assert.assertEquals(ALLERGIST_SPECIALITY_NAME, speciality);
    }

    @Test
    public void findDoctorSpecialityFailed() throws Exception {
        final String speciality = doctorDao.findDoctorSpeciality(UNKNOWN_DOCTOR_ID);
        Assert.assertNull(speciality);
    }
}
