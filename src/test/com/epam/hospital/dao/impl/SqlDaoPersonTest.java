package com.epam.hospital.dao.impl;

import com.epam.hospital.dao.DaoPerson;
import com.epam.hospital.dao.connection.ConnectionPool;
import com.epam.hospital.dao.exception.ConnectionPoolException;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.Person;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.LocalDate;

public class SqlDaoPersonTest {

    private static ConnectionPool pool;
    private static DaoPerson personDao;
    private static Person person;

    @BeforeClass
    public static void init() throws ConnectionPoolException, DaoException {
        pool = ConnectionPool.getInstance();
        pool.init();

        personDao = SqlDaoPerson.getInstance();

        person = new Person();
        person.setName("name");
        person.setSecondName("secondName");
        person.setPatronymic("patronymic");
        person.setDateOfBirth(LocalDate.now());
        person.setSex("women");

        final long personId = personDao.insert(person);
        person.setId(personId);
    }

    @AfterClass
    public static void dispose() throws ConnectionPoolException, DaoException {
        personDao.delete(person);
        pool.dispose();
    }

    @Test
    public void getIdPersonSuccess() throws Exception {
        final long personId = personDao.getIdPerson(person);
        Assert.assertNotNull(personId);
    }

    @Test
    public void getIdPersonFailed() throws Exception {
        final int failedExpectedId = 0;

        Person anotherPerson = new Person();
        anotherPerson.setName("name1");
        anotherPerson.setSecondName("secondName1");
        anotherPerson.setPatronymic("patronymic1");
        anotherPerson.setDateOfBirth(LocalDate.now());

        final long anotherPersonId = personDao.getIdPerson(anotherPerson);
        Assert.assertEquals(failedExpectedId, anotherPersonId);
    }
}
