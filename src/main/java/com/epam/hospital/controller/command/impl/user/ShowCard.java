package com.epam.hospital.controller.command.impl.user;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.domain.view.InspectionVO;
import com.epam.hospital.domain.bean.MedicalCard;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IInspectionService;
import com.epam.hospital.model.service.IMedicalCardService;
import com.epam.hospital.model.service.IMedicalCardVOService;
import com.epam.hospital.model.service.impl.InspectionService;
import com.epam.hospital.model.service.impl.MedicalCardService;
import com.epam.hospital.model.service.impl.MedicalCardVOService;
import com.epam.hospital.model.util.DateTimeManager;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ShowCard implements Command {
    private static final String CASE_HISTORY_PAGE = "/caseHistory";
    private static final String PATIENT_CARD_PAGE = "/patientCard";
    private static final String LAST_NAME = "lastName";
    private static final String FIRST_NAME = "firstName";
    private static final String PATRONYMIC = "patronymic";
    private static final String DATE = "date";
    private static final String PATIENT = "patient";
    private static final String MEDICAL_CARD = "medical_card";
    private static final String INSPECTION = "inspection";
    private static final String ERROR_MESSAGE = "error";

    private IMedicalCardVOService medicalCardVOService = MedicalCardVOService.getInstance();
    private IMedicalCardService medicalCardService = MedicalCardService.getInstance();
    private IInspectionService inspectionService = InspectionService.getInstance();

    private ShowCard(){}

    private static class SingletonHolder {
        private static final ShowCard INSTANCE = new ShowCard();
    }

    public static ShowCard getInstance() {
        return  ShowCard.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        try {
            Person patientPD = new Person();
            patientPD.setName(request.getParameter(FIRST_NAME));
            patientPD.setSecondName(request.getParameter(LAST_NAME));
            patientPD.setPatronymic(request.getParameter(PATRONYMIC));
            patientPD.setDateOfBirth(DateTimeManager.getDate(request.getParameter(DATE)));

            long idMedicalCard = medicalCardVOService.getIdMedicalCard(patientPD);
            List<InspectionVO> inspectionVO = inspectionService.getInspectionVO(idMedicalCard);
            MedicalCard medicalCard = medicalCardService.getMedicalCard(idMedicalCard);

            request.setAttribute(INSPECTION, inspectionVO);
            request.setAttribute(PATIENT, patientPD);
            request.setAttribute(MEDICAL_CARD, medicalCard);

            return CASE_HISTORY_PAGE;
        } catch (NumberFormatException e) {
            request.setAttribute(ERROR_MESSAGE, ErrorMessage.INVALID_DATA.getValue());
        }

        return PATIENT_CARD_PAGE;
    }
}
