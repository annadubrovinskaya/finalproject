package com.epam.hospital.controller.command.impl.admin;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IPatientService;
import com.epam.hospital.model.service.IPersonService;
import com.epam.hospital.model.service.impl.PatientService;
import com.epam.hospital.model.service.impl.PersonService;
import com.epam.hospital.model.util.DateTimeManager;

import javax.servlet.http.HttpServletRequest;

public class DeleteCard implements Command {
    private static final String CARD_DELETE_PAGE = "/cardDelete";
    private static final String LAST_NAME = "lastName";
    private static final String FIRST_NAME = "firstName";
    private static final String PATRONYMIC = "patronymic";
    private static final String DATE = "date";
    private static final String SUCCESS = "success";
    private static final String ERROR_MESSAGE = "error";

    private IPatientService patientService = PatientService.getInstance();

    private DeleteCard(){}

    private static class SingletonHolder {
        private static final DeleteCard INSTANCE = new DeleteCard();
    }

    public static DeleteCard getInstance() {
        return  DeleteCard.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        try {
            Person person = new Person();
            person.setName(request.getParameter(FIRST_NAME));
            person.setSecondName(request.getParameter(LAST_NAME));
            person.setPatronymic(request.getParameter(PATRONYMIC));
            person.setDateOfBirth(DateTimeManager.getDate(request.getParameter(DATE)));

            patientService.delete(person);
            request.setAttribute(SUCCESS, SUCCESS);

        } catch (NumberFormatException e) {
            request.setAttribute(ERROR_MESSAGE, ErrorMessage.INVALID_DATA.getValue());
        }

        return CARD_DELETE_PAGE;
    }
}
