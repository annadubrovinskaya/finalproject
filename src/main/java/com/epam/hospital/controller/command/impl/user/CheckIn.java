package com.epam.hospital.controller.command.impl.user;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.controller.util.SessionManager;
import com.epam.hospital.domain.bean.Doctor;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.bean.Speciality;
import com.epam.hospital.domain.bean.User;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.model.service.IDoctorService;
import com.epam.hospital.model.exception.*;
import com.epam.hospital.model.service.IDoctorVOService;
import com.epam.hospital.model.service.IRoleService;
import com.epam.hospital.model.service.impl.DoctorService;
import com.epam.hospital.model.service.impl.DoctorVOService;
import com.epam.hospital.model.service.impl.RoleService;
import com.epam.hospital.model.util.DateTimeManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class CheckIn implements Command {
    private static final String MAIN_USER_PAGE = "/indexUser";
    private static final String REGISTRATION_PAGE = "/registration";
    private static final String LAST_NAME = "lastName";
    private static final String FIRST_NAME = "firstName";
    private static final String PATRONYMIC = "patronymic";
    private static final String DATE = "date";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String ROOM_NUMBER = "roomNumber";
    private static final String SPECIALITY = "speciality";
    private static final String SEX = "sex";
    private static final String ID_DOCTOR = "id_doctor";
    private static final String ROLE = "role";
    private static final String ERROR_MESSAGE = "error";

    private IDoctorVOService doctorVOService = DoctorVOService.getInstance();
    private IRoleService roleService = RoleService.getInstance();

    private CheckIn(){}

    private static class SingletonHolder {
        private static final CheckIn INSTANCE = new CheckIn();
    }

    public static CheckIn getInstance() {
        return  CheckIn.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        try {
            Person person = new Person();
            person.setName(request.getParameter(FIRST_NAME));
            person.setSecondName(request.getParameter(LAST_NAME));
            person.setPatronymic(request.getParameter(PATRONYMIC));
            person.setDateOfBirth(DateTimeManager.getDate(request.getParameter(DATE)));
            person.setSex(request.getParameter(SEX));

            User user = new User();
            user.setLogin(request.getParameter(LOGIN));
            user.setPassword(request.getParameter(PASSWORD));

            Doctor doctor = new Doctor();
            doctor.setRoomNumber(Integer.parseInt(request.getParameter(ROOM_NUMBER)));

            Speciality speciality = new Speciality();
            speciality.setName(request.getParameter(SPECIALITY));

            long idUser = doctorVOService.checkInDoctor(person, user, doctor, speciality);
            String role = roleService.getRoleName(user.getIdRole());
            HttpSession session = request.getSession();

            session.setAttribute(ID_DOCTOR, idUser);
            session.setAttribute(ROLE, role);
            session.setAttribute(SPECIALITY, speciality.getName());

            SessionManager.setDateAttribute(session);

            return MAIN_USER_PAGE;

        } catch (NumberFormatException e) {
            request.setAttribute(ERROR_MESSAGE, ErrorMessage.INVALID_DATA.getValue());
        }

        return REGISTRATION_PAGE;
    }
}