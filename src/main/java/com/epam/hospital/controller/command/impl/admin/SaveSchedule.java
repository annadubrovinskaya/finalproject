package com.epam.hospital.controller.command.impl.admin;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IScheduleService;
import com.epam.hospital.model.service.IScheduleVOService;
import com.epam.hospital.model.service.impl.ScheduleService;
import com.epam.hospital.model.service.impl.ScheduleVOService;

import javax.servlet.http.HttpServletRequest;
import java.time.DayOfWeek;

public class SaveSchedule implements Command {
    private static final String NEW_SCHEDULE_PAGE = "/newSchedule";
    private static final String SUCCESS = "success";
    private static final String LAST_NAME = "lastName";
    private static final String FIRST_NAME = "firstName";
    private static final String PATRONYMIC = "patronymic";
    private static final String SPECIALITY = "speciality";

    private IScheduleVOService scheduleVOService = ScheduleVOService.getInstance();

    private SaveSchedule(){}

    private static class SingletonHolder {
        private static final SaveSchedule INSTANCE = new SaveSchedule();
    }

    public static SaveSchedule getInstance() {
        return  SaveSchedule.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        Person doctorPD = new Person();
        doctorPD.setName(request.getParameter(FIRST_NAME));
        doctorPD.setSecondName(request.getParameter(LAST_NAME));
        doctorPD.setPatronymic(request.getParameter(PATRONYMIC));
        String specialityName = request.getParameter(SPECIALITY);

        String[] daysWeek = {request.getParameter(DayOfWeek.MONDAY.toString().toLowerCase()),
                request.getParameter(DayOfWeek.TUESDAY.toString().toLowerCase()),
                request.getParameter(DayOfWeek.WEDNESDAY.toString().toLowerCase()),
                request.getParameter(DayOfWeek.THURSDAY.toString().toLowerCase()),
                request.getParameter(DayOfWeek.FRIDAY.toString().toLowerCase()),
                request.getParameter(DayOfWeek.SATURDAY.toString().toLowerCase()),
                request.getParameter(DayOfWeek.SUNDAY.toString().toLowerCase())};

        scheduleVOService.updateSchedule(doctorPD, specialityName, daysWeek);
        request.setAttribute(SUCCESS, SUCCESS);

        return NEW_SCHEDULE_PAGE;
    }
}