package com.epam.hospital.controller.command.impl.user;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.domain.view.DoctorAvailableTime;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IAppointmentService;
import com.epam.hospital.model.service.IAppointmentVOService;
import com.epam.hospital.model.service.impl.AppointmentService;
import com.epam.hospital.model.service.impl.AppointmentVOService;
import com.epam.hospital.model.util.DateTimeManager;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;

public class FindReceiptTime implements Command {
    private static final String APPOINTMENT_PAGE = "/appointment";
    private static final String FILL_APPOINT_DOCTOR_PAGE = "/fillAppointDoctor";
    private static final String DATE = "date";
    private static final String ID_DOCTOR = "id_doctor";
    private static final String AVAILABLE_TIME = "available_time";
    private static final String ERROR_MESSAGE = "error";

    private IAppointmentVOService appointmentVOService = AppointmentVOService.getInstance();

    private FindReceiptTime(){}

    private static class SingletonHolder {
        private static final FindReceiptTime INSTANCE = new FindReceiptTime();
    }

    public static FindReceiptTime getInstance() {
        return  FindReceiptTime.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        try {
            LocalDate requiredDate = DateTimeManager.getDate(request.getParameter(DATE));
            long idDoctor = (long)request.getSession().getAttribute(ID_DOCTOR);

            DoctorAvailableTime availableTime = appointmentVOService.getAvailableTime(requiredDate, idDoctor);
            request.getSession().setAttribute(AVAILABLE_TIME, availableTime);

        } catch (NumberFormatException e) {
            request.setAttribute(ERROR_MESSAGE, ErrorMessage.INVALID_DATA.getValue());

            return APPOINTMENT_PAGE;
        }

        return FILL_APPOINT_DOCTOR_PAGE;
    }
}