package com.epam.hospital.controller.command.impl.admin;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.impl.SpecialityService;

import javax.servlet.http.HttpServletRequest;

public class ForwardEditSchedule implements Command {
    private static final String EDIT_SCHEDULE_PAGE = "/editSchedule";
//    private static final String SPECIALITIES = "specialities";
    private static final String COMMAND_SCHEDULE = "command_schedule";
    private static final String COMMAND_VALUE = "find_schedule";

    private ForwardEditSchedule(){}

    private static class SingletonHolder {
        private static final ForwardEditSchedule INSTANCE = new ForwardEditSchedule();
    }

    public static ForwardEditSchedule getInstance() {
        return  ForwardEditSchedule.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        request.getSession().setAttribute(COMMAND_SCHEDULE, COMMAND_VALUE);
//        request.getSession().setAttribute(SPECIALITIES, SpecialityService.getInstance().getListSpecialities());

        return EDIT_SCHEDULE_PAGE;
    }
}
