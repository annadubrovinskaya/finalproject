package com.epam.hospital.controller.command.impl.common;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.controller.util.SessionManager;
import com.epam.hospital.domain.bean.User;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IDoctorService;
import com.epam.hospital.model.service.ISpecialityService;
import com.epam.hospital.model.service.IUserService;
import com.epam.hospital.model.service.impl.DoctorService;
import com.epam.hospital.model.service.impl.SpecialityService;
import com.epam.hospital.model.service.impl.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class Authorization implements Command {
    private static final String MAIN_ADMIN_PAGE = "/indexAdmin";
    private static final String MAIN_USER_PAGE = "/indexUser";
    private static final String AUTHORIZATION_PAGE = "/authorization";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String ROLE = "role";
    private static final String ROLE_ADMIN = "admin";
    private static final String ROLE_DOCTOR = "doctor";
    private static final String ID_DOCTOR = "id_doctor";
    private static final String SPECIALITY = "speciality";
    private static final String SPECIALITIES = "specialities";

    private IUserService userService = UserService.getInstance();
    private IDoctorService doctorService = DoctorService.getInstance();
    private ISpecialityService specialityService = SpecialityService.getInstance();

    private Authorization(){}

    private static class SingletonHolder {
        private static final Authorization INSTANCE = new Authorization();
    }

    public static Authorization getInstance() {
        return  Authorization.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        HttpSession session = request.getSession();

        SessionManager.setBloodTypes(session);
        SessionManager.setDateAttribute(session);
        session.setAttribute(SPECIALITIES, specialityService.getListSpecialities());

        User user = new User();
        user.setLogin(request.getParameter(LOGIN));
        user.setPassword(request.getParameter(PASSWORD));

        String role = userService.checkAuthorization(user);

        switch (role) {
            case ROLE_ADMIN:
                session.setAttribute(ROLE, ROLE_ADMIN);
                return MAIN_ADMIN_PAGE;

            case ROLE_DOCTOR:
                String specialityName = doctorService.findDoctorSpeciality(user.getId());
                session.setAttribute(ID_DOCTOR, user.getId());
                session.setAttribute(SPECIALITY, specialityName);
                session.setAttribute(ROLE, ROLE_DOCTOR);
                return MAIN_USER_PAGE;
        }

        return AUTHORIZATION_PAGE;
    }
}