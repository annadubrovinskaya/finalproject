package com.epam.hospital.controller.command.impl.user;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.domain.view.PatientAppointment;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IAppointmentService;
import com.epam.hospital.model.service.impl.AppointmentService;
import com.epam.hospital.model.util.DateTimeManager;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.List;

public class ShowPatientAppoint implements Command {
    private static final String DATA_VIEW_DOCTOR_PAGE = "/dvDoctor";
    private static final String PATIENT_APP_PAGE = "/patientAppointment";
    private static final String DATE = "date";
    private static final String ID_DOCTOR = "id_doctor";
    private static final String APPOINTMENT = "appointment";
    private static final String ERROR_MESSAGE = "error";

    private IAppointmentService appointmentService = AppointmentService.getInstance();

    private ShowPatientAppoint(){}

    private static class SingletonHolder {
        private static final ShowPatientAppoint INSTANCE = new ShowPatientAppoint();
    }

    public static ShowPatientAppoint getInstance() {
        return  ShowPatientAppoint.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        try {
            LocalDate date = DateTimeManager.getDate(request.getParameter(DATE));
            long idDoctor = (long)request.getSession().getAttribute(ID_DOCTOR);
            List<PatientAppointment> appointmentList = appointmentService.getPatientAppointments(idDoctor, date);

            request.setAttribute(APPOINTMENT, appointmentList);

            return PATIENT_APP_PAGE;

        } catch (NumberFormatException e) {
            request.setAttribute(ERROR_MESSAGE, ErrorMessage.INVALID_DATA.getValue());
        }

        return DATA_VIEW_DOCTOR_PAGE;
    }
}
