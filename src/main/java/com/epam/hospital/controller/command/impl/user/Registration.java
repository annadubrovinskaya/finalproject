package com.epam.hospital.controller.command.impl.user;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.impl.SpecialityService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;

public class Registration implements Command {
    private static final String REGISTRATION_PAGE = "/registration";
    private static final String SPECIALITIES = "specialities";
    private static final int ADDED_MAX_YEARS = 70;
    private static final int ADDED_MIN_YEARS = 25;
    private static final String MAX_DATE = "maxDateBirthDoctor";
    private static final String MIN_DATE = "minBirthDoctor";

    private Registration(){}

    private static class SingletonHolder {
        private static final Registration INSTANCE = new Registration();
    }

    public static Registration getInstance() {
        return  Registration.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        LocalDate nowDate = LocalDate.now();
        LocalDate maxDate = nowDate.minusYears(ADDED_MIN_YEARS);
        LocalDate minDate = nowDate.minusYears(ADDED_MAX_YEARS);
        HttpSession session = request.getSession();

        session.setAttribute(MAX_DATE, maxDate);
        session.setAttribute(MIN_DATE, minDate);
        session.setAttribute(SPECIALITIES, SpecialityService.getInstance().getListSpecialities());

        return REGISTRATION_PAGE;
    }
}
