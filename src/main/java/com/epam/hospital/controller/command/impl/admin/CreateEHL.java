package com.epam.hospital.controller.command.impl.admin;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.domain.bean.MedicalCard;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IMedicalCardService;
import com.epam.hospital.model.service.IMedicalCardVOService;
import com.epam.hospital.model.service.impl.MedicalCardService;
import com.epam.hospital.model.service.impl.MedicalCardVOService;
import com.epam.hospital.model.util.DateTimeManager;

import javax.servlet.http.HttpServletRequest;

public class CreateEHL implements Command {
    private static final String MAIN_ADMIN_PAGE = "/indexAdmin";
    private static final String LAST_NAME = "lastName";
    private static final String FIRST_NAME = "firstName";
    private static final String PATRONYMIC = "patronymic";
    private static final String DATE = "date";
    private static final String SEX = "sex";
    private static final String HEIGHT = "height";
    private static final String WEIGHT = "weight";
    private static final String BLOOD_TYPE = "blood_type";
    private static final String SUCCESS = "success";
    private static final String ERROR_MESSAGE = "error";

    private IMedicalCardVOService medicalCardVOService = MedicalCardVOService.getInstance();

    private CreateEHL(){}

    private static class SingletonHolder {
        private static final CreateEHL INSTANCE = new CreateEHL();
    }

    public static CreateEHL getInstance() {
        return  CreateEHL.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        try {
            Person person = new Person();
            person.setName(request.getParameter(FIRST_NAME));
            person.setSecondName(request.getParameter(LAST_NAME));
            person.setPatronymic(request.getParameter(PATRONYMIC));
            person.setDateOfBirth(DateTimeManager.getDate(request.getParameter(DATE)));
            person.setSex(request.getParameter(SEX));

            MedicalCard medicalCard = new MedicalCard();
            medicalCard.setHeightPatient(Float.parseFloat(request.getParameter(HEIGHT)));
            medicalCard.setWeightPatient(Float.parseFloat(request.getParameter(WEIGHT)));
            medicalCard.setBloodType(request.getParameter(BLOOD_TYPE));

            medicalCardVOService.createCard(person, medicalCard);

            request.setAttribute(SUCCESS, SUCCESS);

        } catch (NumberFormatException e) {
            request.setAttribute(ERROR_MESSAGE, ErrorMessage.INVALID_DATA.getValue());
        }

        return MAIN_ADMIN_PAGE;
    }
}
