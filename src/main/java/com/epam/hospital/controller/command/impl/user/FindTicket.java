package com.epam.hospital.controller.command.impl.user;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.domain.bean.Appointment;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.domain.view.AppointmentVO;
import com.epam.hospital.domain.bean.Diagnosis;
import com.epam.hospital.domain.bean.MedicalCard;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IAppointmentService;
import com.epam.hospital.model.service.impl.AppointmentService;
import com.epam.hospital.model.service.impl.DiagnosisService;
import com.epam.hospital.model.service.impl.MedicalCardService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public class FindTicket implements Command {
    private static final String MAIN_USER_PAGE = "/indexUser";
    private static final String INSPECTION_PAGE = "/inspection";
    private static final String DIAGNOSIS = "diagnosis";
    private static final String TICKET_NUMBER = "ticket_number";
    private static final String APPOINTMENT = "appointment";
    private static final String MEDICAL_CARD = "medical_card";
    private static final String ERROR_MESSAGE = "error";

    private IAppointmentService appointmentService = AppointmentService.getInstance();
    private MedicalCardService medicalCardService = MedicalCardService.getInstance();
    private DiagnosisService diagnosisService = DiagnosisService.getInstance();

    private FindTicket(){}

    private static class SingletonHolder {
        private static final FindTicket INSTANCE = new FindTicket();
    }

    public static FindTicket getInstance() {
        return  FindTicket.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        try {
            long idAppointment = Long.parseLong(request.getParameter(TICKET_NUMBER));
            Appointment appointment = appointmentService.getAppointment(idAppointment);
            MedicalCard medicalCard = medicalCardService.getMedicalCard(appointment.getIdMedicalCard());
            AppointmentVO appointmentVO = appointmentService.getAppointmentVO(idAppointment);
            List<Diagnosis> diagnosisList = diagnosisService.getDiagnosis();
            HttpSession session = request.getSession();

            session.setAttribute(APPOINTMENT, appointmentVO);
            session.setAttribute(DIAGNOSIS, diagnosisList);
            request.setAttribute(MEDICAL_CARD, medicalCard);

        } catch (NumberFormatException e) {
            request.setAttribute(ERROR_MESSAGE, ErrorMessage.INVALID_DATA.getValue());

            return MAIN_USER_PAGE;
        }

        return INSPECTION_PAGE;
    }
}
