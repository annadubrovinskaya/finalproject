package com.epam.hospital.controller.command.impl.admin;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.domain.bean.Appointment;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IAppointmentService;
import com.epam.hospital.model.service.impl.AppointmentService;

import javax.servlet.http.HttpServletRequest;

public class CancelAppointment implements Command {
    private static final String APPOINTMENT_CANCEL_PAGE = "/appointmentCancel";
    private static final String TICKET_NUMBER = "ticket_number";
    private static final String SUCCESS = "success";
    private static final String ERROR_MESSAGE = "error";

    private IAppointmentService appointmentService = AppointmentService.getInstance();

    private CancelAppointment(){}

    private static class SingletonHolder {
        private static final CancelAppointment INSTANCE = new CancelAppointment();
    }

    public static CancelAppointment getInstance() {
        return  CancelAppointment.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        try {
            Appointment appointment = new Appointment();
            appointment.setId(Long.parseLong(request.getParameter(TICKET_NUMBER)));

            appointmentService.deleteAppointment(appointment.getId());
            request.setAttribute(SUCCESS, SUCCESS);

        } catch (NumberFormatException e) {
            request.setAttribute(ERROR_MESSAGE, ErrorMessage.INVALID_DATA.getValue());
        }

        return APPOINTMENT_CANCEL_PAGE;
    }
}