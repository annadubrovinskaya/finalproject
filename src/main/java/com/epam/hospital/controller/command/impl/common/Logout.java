package com.epam.hospital.controller.command.impl.common;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.controller.util.SessionManager;

import javax.servlet.http.HttpServletRequest;

public class Logout implements Command {
    private static final String INDEX_PAGE = "index.jsp";

    private Logout(){}

    private static class SingletonHolder {
        private static final Logout INSTANCE = new Logout();
    }

    public static Logout getInstance() {
        return  Logout.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) {
        SessionManager.deleteAttributes(request);

        return INDEX_PAGE;
    }
}
