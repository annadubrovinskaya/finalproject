package com.epam.hospital.controller.command.impl.admin;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.domain.view.PatientAppointment;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IAppointmentService;
import com.epam.hospital.model.service.IDoctorService;
import com.epam.hospital.model.service.impl.AppointmentService;
import com.epam.hospital.model.service.impl.DoctorService;
import com.epam.hospital.model.util.DateTimeManager;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.List;

public class ShowDataDoctor implements Command {
    private static final String DATA_VIEW_DOCTOR_PAGE = "/dataViewDoctor";
    private static final String DATA_DOCTOR_PAGE = "/dataDoctor";
    private static final String LAST_NAME = "lastName";
    private static final String FIRST_NAME = "firstName";
    private static final String PATRONYMIC = "patronymic";
    private static final String SPECIALITY = "speciality";
    private static final String DATE = "date";
    private static final String APPOINTMENT = "appointment";
    private static final String ERROR_MESSAGE = "error";

    private IDoctorService doctorService = DoctorService.getInstance();
    private IAppointmentService appointmentService = AppointmentService.getInstance();

    private ShowDataDoctor(){}

    private static class SingletonHolder {
        private static final ShowDataDoctor INSTANCE = new ShowDataDoctor();
    }

    public static ShowDataDoctor getInstance() {
        return  ShowDataDoctor.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        try {
            Person doctorPD = new Person();
            doctorPD.setName(request.getParameter(FIRST_NAME));
            doctorPD.setSecondName(request.getParameter(LAST_NAME));
            doctorPD.setPatronymic(request.getParameter(PATRONYMIC));

            String specialityName = request.getParameter(SPECIALITY);
            LocalDate date = DateTimeManager.getDate(request.getParameter(DATE));
            long idDoctor = doctorService.getDoctorId(doctorPD, specialityName);
            List<PatientAppointment> appointmentList = appointmentService.getPatientAppointments(idDoctor, date);

            request.setAttribute(APPOINTMENT, appointmentList);

            return DATA_DOCTOR_PAGE;

        } catch (NumberFormatException e) {
            request.setAttribute(ERROR_MESSAGE, ErrorMessage.INVALID_DATA.getValue());
        }

        return DATA_VIEW_DOCTOR_PAGE;
    }
}