package com.epam.hospital.controller.command.impl.admin;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.domain.view.DoctorAppointment;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IAppointmentService;
import com.epam.hospital.model.service.IAppointmentVOService;
import com.epam.hospital.model.service.impl.AppointmentService;
import com.epam.hospital.model.service.impl.AppointmentVOService;
import com.epam.hospital.model.util.DateTimeManager;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ShowDataPatient implements Command {
    private static final String DATA_VIEW_PATIENT_PAGE = "/dataViewPatient";
    private static final String DATA_PATIENT_PAGE = "/dataPatient";
    private static final String LAST_NAME = "lastName";
    private static final String FIRST_NAME = "firstName";
    private static final String PATRONYMIC = "patronymic";
    private static final String DATE = "date";
    private static final String APPOINTMENT = "appointment";
    private static final String ERROR_MESSAGE = "error";

    private IAppointmentVOService appointmentVOService = AppointmentVOService.getInstance();

    private ShowDataPatient(){}

    private static class SingletonHolder {
        private static final ShowDataPatient INSTANCE = new ShowDataPatient();
    }

    public static ShowDataPatient getInstance() {
        return  ShowDataPatient.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        try {
            Person patientPD = new Person();
            patientPD.setName(request.getParameter(FIRST_NAME));
            patientPD.setSecondName(request.getParameter(LAST_NAME));
            patientPD.setPatronymic(request.getParameter(PATRONYMIC));
            patientPD.setDateOfBirth(DateTimeManager.getDate(request.getParameter(DATE)));

            List<DoctorAppointment> appointmentList = appointmentVOService.getDoctorAppointments(patientPD);

            request.setAttribute(APPOINTMENT, appointmentList);

            return DATA_PATIENT_PAGE;

        } catch (NumberFormatException e) {
            request.setAttribute(ERROR_MESSAGE, ErrorMessage.INVALID_DATA.getValue());
        }

        return DATA_VIEW_PATIENT_PAGE;
    }
}
