package com.epam.hospital.controller.command;

import com.epam.hospital.controller.command.impl.admin.*;
import com.epam.hospital.controller.command.impl.common.Authorization;
import com.epam.hospital.controller.command.impl.common.Logout;
import com.epam.hospital.controller.command.impl.common.MakeAppointment;
import com.epam.hospital.controller.command.impl.user.*;

import java.util.HashMap;
import java.util.Map;

public class CommandManager {
    private Map<String, Command> commandMap;

    private CommandManager(){
        commandMap = new HashMap<>();
        commandMap.put(CommandType.AUTHORIZATION.toString().toLowerCase(), Authorization.getInstance());
        commandMap.put(CommandType.REGISTRATION.toString().toLowerCase(), Registration.getInstance());
        commandMap.put(CommandType.CHECK_IN.toString().toLowerCase(), CheckIn.getInstance());
        commandMap.put(CommandType.FORWARD_SCHEDULE.toString().toLowerCase(), ForwardSchedule.getInstance());
        commandMap.put(CommandType.CREATE_EHL.toString().toLowerCase(), CreateEHL.getInstance());
        commandMap.put(CommandType.CREATE_SCHEDULE.toString().toLowerCase(), CreateSchedule.getInstance());
        commandMap.put(CommandType.FIND_TIME_RECEIPT_SPECIALITY.toString().toLowerCase(), FindTimeReceiptSpeciality.getInstance());
        commandMap.put(CommandType.CANCEL_APPOINTMENT.toString().toLowerCase(), CancelAppointment.getInstance());
        commandMap.put(CommandType.FIND_TICKET.toString().toLowerCase(), FindTicket.getInstance());
        commandMap.put(CommandType.SAVE_INSPECTION.toString().toLowerCase(), SaveInspection.getInstance());
        commandMap.put(CommandType.FIND_RECEIPT_TIME.toString().toLowerCase(), FindReceiptTime.getInstance());
        commandMap.put(CommandType.SHOW_CARD.toString().toLowerCase(), ShowCard.getInstance());
        commandMap.put(CommandType.DELETE_CARD.toString().toLowerCase(), DeleteCard.getInstance());
        commandMap.put(CommandType.LOGOUT.toString().toLowerCase(), Logout.getInstance());
        commandMap.put(CommandType.FORWARD_EDIT_SCHEDULE.toString().toLowerCase(), ForwardEditSchedule.getInstance());
        commandMap.put(CommandType.FIND_SCHEDULE.toString().toLowerCase(), FindSchedule.getInstance());
        commandMap.put(CommandType.SHOW_DATA_DOCTOR.toString().toLowerCase(), ShowDataDoctor.getInstance());
        commandMap.put(CommandType.SHOW_DATA_PATIENT.toString().toLowerCase(), ShowDataPatient.getInstance());
        commandMap.put(CommandType.SAVE_SCHEDULE.toString().toLowerCase(), SaveSchedule.getInstance());
        commandMap.put(CommandType.SHOW_PATIENT_APPOINT.toString().toLowerCase(), ShowPatientAppoint.getInstance());
        commandMap.put(CommandType.SHOW_SCHEDULE.toString().toLowerCase(), ShowSchedule.getInstance());
        commandMap.put(CommandType.SHOW_DOC_SCHEDULE.toString().toLowerCase(), ShowDocSchedule.getInstance());
        commandMap.put(CommandType.FORWARD_DOC_SCHEDULE.toString().toLowerCase(), ForwardDocSchedule.getInstance());
        commandMap.put(CommandType.MAKE_APPOINTMENT.toString().toLowerCase(), MakeAppointment.getInstance());
    }

    private static class SingletonHolder {
        private static final CommandManager INSTANCE = new CommandManager();
    }

    public static CommandManager getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public Command getCommand(String command) {
        return commandMap.get(command);
    }
}
