package com.epam.hospital.controller.command.impl.admin;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.domain.view.DoctorVO;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.impl.DoctorService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public class ForwardSchedule implements Command {
    private static final String SCHEDULE_PAGE = "/doctorSchedule";
    private static final String DOCTORS_ATTRIBUTE = "doctors";

    private ForwardSchedule(){}

    private static class SingletonHolder {
        private static final ForwardSchedule INSTANCE = new ForwardSchedule();
    }

    public static ForwardSchedule getInstance() {
        return  ForwardSchedule.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        List<DoctorVO> doctors = DoctorService.getInstance().getDoctorsWithoutSchedule();
        request.getSession().setAttribute(DOCTORS_ATTRIBUTE, doctors);

        return SCHEDULE_PAGE;
    }
}