package com.epam.hospital.controller.command.impl.admin;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.bean.ScheduleVisit;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IScheduleService;
import com.epam.hospital.model.service.IScheduleVOService;
import com.epam.hospital.model.service.impl.ScheduleService;
import com.epam.hospital.model.service.impl.ScheduleVOService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class ShowDocSchedule implements Command {
    private static final String SCHEDULE_PAGE = "/showDocSchedule";
    private static final String EDIT_SCHEDULE_PAGE = "/editSchedule";
    private static final String LAST_NAME = "lastName";
    private static final String FIRST_NAME = "firstName";
    private static final String PATRONYMIC = "patronymic";
    private static final String SPECIALITY = "speciality";
    private static final String SCHEDULE = "schedule";
    private static final String COMMAND_SCHEDULE = "command_schedule";
    private static final String COMMAND_VALUE = "show_doc_schedule";
    private static final String ERROR_MESSAGE = "error";

    private IScheduleVOService scheduleVOService = ScheduleVOService.getInstance();

    private ShowDocSchedule(){}

    private static class SingletonHolder {
        private static final ShowDocSchedule INSTANCE = new ShowDocSchedule();
    }

    public static ShowDocSchedule getInstance() {
        return  ShowDocSchedule.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        try {
            request.setAttribute(COMMAND_SCHEDULE, COMMAND_VALUE);

            Person doctorPD = new Person();
            doctorPD.setName(request.getParameter(FIRST_NAME));
            doctorPD.setSecondName(request.getParameter(LAST_NAME));
            doctorPD.setPatronymic(request.getParameter(PATRONYMIC));

            String specialityName = request.getParameter(SPECIALITY);
            Map<String, ScheduleVisit> schedule = scheduleVOService.getSchedule(doctorPD, specialityName);
            request.getSession().setAttribute(SCHEDULE, schedule);

            return SCHEDULE_PAGE;

        } catch (NumberFormatException e) {
            request.setAttribute(ERROR_MESSAGE, ErrorMessage.INVALID_DATA.getValue());
        }

        return EDIT_SCHEDULE_PAGE;
    }
}