package com.epam.hospital.controller.command.impl.user;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.domain.bean.Appointment;
import com.epam.hospital.domain.bean.MedicalCard;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.domain.view.InspectionVO;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IInspectionService;
import com.epam.hospital.model.service.IInspectionVOService;
import com.epam.hospital.model.service.impl.InspectionService;
import com.epam.hospital.model.service.impl.InspectionVOService;
import com.epam.hospital.model.util.DateTimeManager;

import javax.servlet.http.HttpServletRequest;

public class SaveInspection implements Command {
    private static final String INSPECTION_PAGE = "/inspection";
    private static final String TICKET_NUMBER = "ticket_number";
    private static final String DATE = "date";
    private static final String CONCLUSION = "conclusion";
    private static final String DIAGNOSIS = "diagnosis";
    private static final String SUCCESS = "success";
    private static final String SPECIALITY = "speciality";
    private static final String WEIGHT = "weight";
    private static final String HEIGHT = "height";
    private static final String ERROR_MESSAGE = "error";

    private IInspectionVOService inspectionVService = InspectionVOService.getInstance();

    private SaveInspection(){}

    private static class SingletonHolder {
        private static final SaveInspection INSTANCE = new SaveInspection();
    }

    public static SaveInspection getInstance() {
        return  SaveInspection.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        try {
            Appointment appointment = new Appointment();
            appointment.setId(Integer.parseInt(request.getParameter(TICKET_NUMBER)));

            InspectionVO inspectionVO = new InspectionVO();
            inspectionVO.setDate(DateTimeManager.getDate(request.getParameter(DATE)));
            inspectionVO.setConclusion(request.getParameter(CONCLUSION));
            inspectionVO.setSpeciality((String) request.getSession().getAttribute(SPECIALITY));
            inspectionVO.setDiagnosis(request.getParameter(DIAGNOSIS));

            MedicalCard medicalCard = new MedicalCard();
            medicalCard.setHeightPatient(Float.parseFloat(request.getParameter(HEIGHT)));
            medicalCard.setWeightPatient(Float.parseFloat(request.getParameter(WEIGHT)));

            inspectionVService.saveInspection(inspectionVO, medicalCard, appointment.getId());

            request.setAttribute(SUCCESS, SUCCESS);

        } catch (NumberFormatException e) {
            request.setAttribute(ERROR_MESSAGE, ErrorMessage.INVALID_DATA.getValue());
        }

        return INSPECTION_PAGE;
    }
}