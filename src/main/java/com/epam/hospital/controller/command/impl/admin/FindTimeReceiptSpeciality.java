package com.epam.hospital.controller.command.impl.admin;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.domain.view.DoctorAppointment;
import com.epam.hospital.domain.view.DoctorAvailableTime;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IAppointmentService;
import com.epam.hospital.model.service.IAppointmentVOService;
import com.epam.hospital.model.service.impl.AppointmentService;
import com.epam.hospital.model.service.impl.AppointmentVOService;
import com.epam.hospital.model.util.DateTimeManager;

import javax.servlet.http.HttpServletRequest;

public class FindTimeReceiptSpeciality implements Command {
    private static final String FILL_APPOINTMENTS_PAGE = "/fillAppointAdmin";
    private static final String APPOINTMENTS_PAGE = "/appointmentSpeciality";
    private static final String SPECIALITY = "speciality";
    private static final String DATE = "date";
    private static final String AVAILABLE_TIME = "available_time";
    private static final String ERROR_MESSAGE = "error";

    private IAppointmentVOService appointmentVOService = AppointmentVOService.getInstance();

    private FindTimeReceiptSpeciality(){}

    private static class SingletonHolder {
        private static final FindTimeReceiptSpeciality INSTANCE = new FindTimeReceiptSpeciality();
    }

    public static FindTimeReceiptSpeciality getInstance() {
        return  FindTimeReceiptSpeciality.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        try {
            DoctorAppointment doctorAppointment = new DoctorAppointment();
            doctorAppointment.setDate(DateTimeManager.getDate(request.getParameter(DATE)));
            doctorAppointment.setSpeciality(request.getParameter(SPECIALITY));

            DoctorAvailableTime availableTime = appointmentVOService.getAvailableTime(doctorAppointment);
            request.getSession().setAttribute(AVAILABLE_TIME, availableTime);

            return FILL_APPOINTMENTS_PAGE;

        } catch (NumberFormatException e) {
            request.setAttribute(ERROR_MESSAGE, ErrorMessage.INVALID_DATA.getValue());

            return APPOINTMENTS_PAGE;
        }
    }
}