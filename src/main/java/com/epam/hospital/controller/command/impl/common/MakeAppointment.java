package com.epam.hospital.controller.command.impl.common;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.controller.util.RequestManager;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.domain.view.DoctorAppointment;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.*;
import com.epam.hospital.model.service.impl.*;
import com.epam.hospital.model.util.DateTimeManager;
import com.epam.hospital.model.util.PersonManager;

import javax.servlet.http.HttpServletRequest;

public class MakeAppointment implements Command {
    private static final String APPOINTMENT_PAGE = "fillAppointDoctor";
    private static final String TICKET_APPOINTMENT_DOC_PAGE = "/ticketAppointmentDoc";
    private static final String TICKET_APPOINTMENT_PAGE = "/ticketAppointment";
    private static final String AVAILABLE_DATE = "available_date";
    private static final String AVAILABLE_TIME = "available_time";
    private static final String DOCTOR = "doctor";
    private static final String SPECIALITY = "speciality";
    private static final String LAST_NAME = "lastName";
    private static final String FIRST_NAME = "firstName";
    private static final String PATRONYMIC = "patronymic";
    private static final String DATE_BIRTH = "dateBirthPatient";
    private static final String ID_APPOINTMENT = "idAppointment";
    private static final String ROOM_NUMBER = "roomNumber";
    private static final String TIME = "time";
    private static final String ERROR_MESSAGE = "error";

    private IAppointmentVOService appointmentVOService = AppointmentVOService.getInstance();
    private IAppointmentService appointmentService = AppointmentService.getInstance();

    private MakeAppointment(){}

    private static class SingletonHolder {
        private static final MakeAppointment INSTANCE = new MakeAppointment();
    }

    public static MakeAppointment getInstance() {
        return  MakeAppointment.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        String referPage = RequestManager.getReferPage(request);
        appointmentService.deleteExpiredTicket();

        try {
            String doctorName = request.getParameter(DOCTOR);
            Person doctorPD = PersonManager.getPersonalData(doctorName);
            String specialityName = request.getParameter(SPECIALITY);

            Person person = new Person();
            person.setName(request.getParameter(FIRST_NAME));
            person.setSecondName(request.getParameter(LAST_NAME));
            person.setPatronymic(request.getParameter(PATRONYMIC));
            person.setDateOfBirth(DateTimeManager.getDate(request.getParameter(DATE_BIRTH)));

            DoctorAppointment appointment = new DoctorAppointment();
            appointment.setNameDoctor(doctorPD.getName());
            appointment.setSecondNameDoctor(doctorPD.getSecondName());
            appointment.setPatronymicDoctor(doctorPD.getPatronymic());
            appointment.setSpeciality(specialityName);
            appointment.setDate(DateTimeManager.getDate(request.getParameter(AVAILABLE_DATE)));
            appointment.setTime(DateTimeManager.getTime(request.getParameter(AVAILABLE_TIME)));

            long idAppointment = appointmentVOService.makeAppointment(appointment, person);

            request.setAttribute(ID_APPOINTMENT, idAppointment);
            request.setAttribute(ROOM_NUMBER, appointment.getRoomNumber());
            request.setAttribute(TIME, appointment.getTime());

            if (referPage.equals(APPOINTMENT_PAGE)) {
                return TICKET_APPOINTMENT_DOC_PAGE;
            } else {
                return TICKET_APPOINTMENT_PAGE;
            }

        } catch (NumberFormatException e) {
            request.setAttribute(ERROR_MESSAGE, ErrorMessage.INVALID_DATA.getValue());
        }

        return referPage;
    }
}