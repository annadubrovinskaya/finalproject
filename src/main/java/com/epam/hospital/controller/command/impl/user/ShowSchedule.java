package com.epam.hospital.controller.command.impl.user;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.domain.bean.ScheduleVisit;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IScheduleService;
import com.epam.hospital.model.service.impl.ScheduleService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

public class ShowSchedule implements Command {
    private static final String SCHEDULE_PAGE = "/schedule";
    private static final String SCHEDULE = "schedule";
    private static final String ID_DOCTOR = "id_doctor";

    private IScheduleService scheduleService = ScheduleService.getInstance();

    private ShowSchedule(){}

    private static class SingletonHolder {
        private static final ShowSchedule INSTANCE = new ShowSchedule();
    }

    public static ShowSchedule getInstance() {
        return  ShowSchedule.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        HttpSession session = request.getSession();

        long idDoctor = (long)session.getAttribute(ID_DOCTOR);
        Map<String, ScheduleVisit> schedule = scheduleService.getSchedule(idDoctor);

        session.setAttribute(SCHEDULE, schedule);

        return SCHEDULE_PAGE;
    }
}
