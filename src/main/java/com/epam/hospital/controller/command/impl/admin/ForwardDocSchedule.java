package com.epam.hospital.controller.command.impl.admin;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.impl.SpecialityService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ForwardDocSchedule implements Command {
    private static final String EDIT_SCHEDULE_PAGE = "/editSchedule";
    private static final String COMMAND_SCHEDULE = "command_schedule";
    private static final String COMMAND_VALUE = "show_doc_schedule";
//    private static final String SPECIALITIES = "specialities";

    private ForwardDocSchedule(){}

    private static class SingletonHolder {
        private static final ForwardDocSchedule INSTANCE = new ForwardDocSchedule();
    }

    public static ForwardDocSchedule getInstance() {
        return  ForwardDocSchedule.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        HttpSession session = request.getSession();
        session.setAttribute(COMMAND_SCHEDULE, COMMAND_VALUE);
//        session.setAttribute(SPECIALITIES, SpecialityService.getInstance().getListSpecialities());

        return EDIT_SCHEDULE_PAGE;
    }
}
