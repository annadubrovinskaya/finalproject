package com.epam.hospital.controller.command;

import com.epam.hospital.model.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;

public interface Command {
    String execute(HttpServletRequest request) throws ServiceException;
}
