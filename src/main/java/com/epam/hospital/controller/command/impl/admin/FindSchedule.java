package com.epam.hospital.controller.command.impl.admin;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.bean.ScheduleVisit;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IScheduleService;
import com.epam.hospital.model.service.IScheduleVOService;
import com.epam.hospital.model.service.impl.ScheduleService;
import com.epam.hospital.model.service.impl.ScheduleVOService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class FindSchedule implements Command {
    private static final String NEW_SCHEDULE_PAGE = "/newSchedule";
    private static final String LAST_NAME = "lastName";
    private static final String FIRST_NAME = "firstName";
    private static final String PATRONYMIC = "patronymic";
    private static final String SPECIALITY = "speciality";
    private static final String SCHEDULE = "schedule";
    private static final String COMMAND_SCHEDULE = "command_schedule";
    private static final String COMMAND_VALUE = "find_schedule";

    private IScheduleVOService scheduleVOService = ScheduleVOService.getInstance();

    private FindSchedule(){}

    private static class SingletonHolder {
        private static final FindSchedule INSTANCE = new FindSchedule();
    }

    public static FindSchedule getInstance() {
        return  FindSchedule.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        Person doctorPD = new Person();
        doctorPD.setName(request.getParameter(FIRST_NAME));
        doctorPD.setSecondName(request.getParameter(LAST_NAME));
        doctorPD.setPatronymic(request.getParameter(PATRONYMIC));

        String specialityName = request.getParameter(SPECIALITY);
        Map<String, ScheduleVisit> schedule = scheduleVOService.getSchedule(doctorPD, specialityName);

        request.setAttribute(COMMAND_SCHEDULE, COMMAND_VALUE);
        request.getSession().setAttribute(SCHEDULE, schedule);

        return NEW_SCHEDULE_PAGE;
    }
}