package com.epam.hospital.controller.command.impl.admin;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.view.DoctorVO;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IDoctorService;
import com.epam.hospital.model.service.IScheduleVOService;
import com.epam.hospital.model.service.impl.DoctorService;
import com.epam.hospital.model.service.impl.ScheduleVOService;
import com.epam.hospital.model.util.DoctorManager;
import com.epam.hospital.model.util.PersonManager;

import javax.servlet.http.HttpServletRequest;
import java.time.DayOfWeek;
import java.util.List;

public class CreateSchedule implements Command {
    private static final String DOCTOR_SCHEDULE_PAGE = "/doctorSchedule";
    private static final String DOCTOR = "doctor";
    private static final String SUCCESS = "success";
    private static final String DOCTORS_ATTRIBUTE = "doctors";

    private IScheduleVOService scheduleVOService = ScheduleVOService.getInstance();
    private IDoctorService doctorService = DoctorService.getInstance();

    private CreateSchedule(){}

    private static class SingletonHolder {
        private static final CreateSchedule INSTANCE = new CreateSchedule();
    }

    public static CreateSchedule getInstance() {
        return  CreateSchedule.SingletonHolder.INSTANCE;
    }

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        String doctorData = request.getParameter(DOCTOR);
        Person doctorPD = PersonManager.getPersonalData(doctorData);
        String specialityName = DoctorManager.getSpeciality(doctorData);

        String[] daysWeek = {request.getParameter(DayOfWeek.MONDAY.toString().toLowerCase()),
                request.getParameter(DayOfWeek.TUESDAY.toString().toLowerCase()),
                request.getParameter(DayOfWeek.WEDNESDAY.toString().toLowerCase()),
                request.getParameter(DayOfWeek.THURSDAY.toString().toLowerCase()),
                request.getParameter(DayOfWeek.FRIDAY.toString().toLowerCase()),
                request.getParameter(DayOfWeek.SATURDAY.toString().toLowerCase()),
                request.getParameter(DayOfWeek.SUNDAY.toString().toLowerCase())};

        scheduleVOService.createSchedule(doctorPD, specialityName, daysWeek);

        request.setAttribute(SUCCESS, SUCCESS);
        List<DoctorVO> doctors = doctorService.getDoctorsWithoutSchedule();
        request.getSession().setAttribute(DOCTORS_ATTRIBUTE, doctors);

        return DOCTOR_SCHEDULE_PAGE;
    }
}