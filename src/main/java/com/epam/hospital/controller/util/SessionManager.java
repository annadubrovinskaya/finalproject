package com.epam.hospital.controller.util;

import com.epam.hospital.domain.enumeration.BloodType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class SessionManager {
    private static final String MAX_DATE = "maxDate";
    private static final String MIN_DATE = "minDate";
    private static final int ADDED_DAYS = 20;
    private static final String MAX_DATE_PAT_BIRTH = "maxPatBirthDate";
    private static final String MIN_DATE_PAT_BIRTH = "minPatBirthDate";
    private static final int ADDED_YEARS = 100;
    private static final String CASE_BLOOD_TYPE = "case_blood_type";

    private SessionManager(){}

    public static void setDateAttribute(HttpSession session) {
        LocalDate nowDate = LocalDate.now();
        LocalDate maxDate = nowDate.plusDays(ADDED_DAYS);
        session.setAttribute(MAX_DATE, maxDate);
        session.setAttribute(MIN_DATE, nowDate);

        LocalDate minDate = maxDate.minusYears(ADDED_YEARS);
        session.setAttribute(MAX_DATE_PAT_BIRTH, nowDate);
        session.setAttribute(MIN_DATE_PAT_BIRTH, minDate);
    }

    public static void setDateAttribute(HttpServletRequest request) {
        setDateAttribute(request.getSession());
    }

    public static void setBloodTypes(HttpSession session) {
        List<String> caseBloodType = new ArrayList<>();
        BloodType[] bloodTypes = BloodType.values();

        for (BloodType bloodType: bloodTypes) {
            caseBloodType.add(bloodType.getValue());
        }

        session.setAttribute(CASE_BLOOD_TYPE, caseBloodType);
    }

    public static void setBloodTypes(HttpServletRequest request) {
        setBloodTypes(request.getSession());
    }

    public static void deleteAttributes(HttpServletRequest request) {
        Enumeration attributes = request.getSession().getAttributeNames();
        while (attributes.hasMoreElements()) {
            String attributeName = (String)attributes.nextElement();
            request.getSession().removeAttribute(attributeName);
        }
    }
}
