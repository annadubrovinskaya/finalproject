package com.epam.hospital.controller.util;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

public class RequestManager {
    private static final String PREFIX_REMOVE_ATTRIBUTE = "remove_";
    private static final String REFERER = "referer";
    private static final String SLASH = "/";

    private RequestManager(){}

    public static void saveRequestParamInSession(HttpServletRequest request) {
        Enumeration parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String paramName = (String)parameterNames.nextElement();
            request.getSession().setAttribute(PREFIX_REMOVE_ATTRIBUTE + paramName, request.getParameter(paramName));
        }

        Enumeration attributes = request.getAttributeNames();
        while (attributes.hasMoreElements()) {
            String attributeName = (String)attributes.nextElement();
            request.getSession().setAttribute(PREFIX_REMOVE_ATTRIBUTE + attributeName, request.getAttribute(attributeName));
        }
    }

    public static String getReferPage(HttpServletRequest request) {
        String page = request.getHeader(REFERER);

        if(page != null) {
            int slashLastIndex = page.lastIndexOf(SLASH);
            page = page.substring(slashLastIndex + 1);
        }

        return page;
    }
}
