package com.epam.hospital.controller;

import com.epam.hospital.controller.command.Command;
import com.epam.hospital.controller.command.CommandManager;
import com.epam.hospital.controller.util.RequestManager;
import com.epam.hospital.listener.ApplicationListener;
import com.epam.hospital.model.exception.ServiceException;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Controller extends HttpServlet {
    private static final String COMMAND_PARAMETER_TITLE = "command";
    private static final String REFERER = "referer";
    private static final String SLASH = "/";
    private static final String ERROR_MESSAGE = "error";
    private static final String LOG_PATH = "/log4j.xml";
    private static final Logger logger = Logger.getLogger(ApplicationListener.class);

    private CommandManager commandManager = CommandManager.getInstance();

    static {
        DOMConfigurator.configure(Controller.class.getResource(LOG_PATH));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String commandType = req.getParameter(COMMAND_PARAMETER_TITLE);

            if (commandType != null) {
                Command command = commandManager.getCommand(commandType);
                String page = command.execute(req);
                RequestDispatcher dispatcher = req.getRequestDispatcher(page);

                if (dispatcher != null) {
                    RequestManager.saveRequestParamInSession(req);
                    resp.sendRedirect(page);
                }
            }
        } catch (ServiceException e) {
            logger.error("ServiceException in method processRequest", e);
            req.setAttribute(ERROR_MESSAGE, e.getMessage());
            processErrorRequest(req, resp);
        }
    }

    private void processErrorRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String referPage = getReferPage(req);

        RequestManager.saveRequestParamInSession(req);
        resp.sendRedirect(referPage);
    }

    private String getReferPage(HttpServletRequest req) {
        String page = req.getHeader(REFERER);

        if (page != null) {
            int slashLastIndex = page.lastIndexOf(SLASH);
            page = page.substring(slashLastIndex + 1);
        }

        return page;
    }
}