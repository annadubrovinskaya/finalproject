package com.epam.hospital.dao;

import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.MedicalCard;

/**
 * Interface defines methods for working with the table medical_card
 * in database hospital.
 */

public interface DaoMedicalCard {

    /**
     * Method allows to insert the object data {@code MedicalCard} in the table medical_card.
     *
     * @param medicalCard contains all information to be entered in the table medical_card.
     * @return {@code idMedicalCard} that contains id inserted record.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    long insert(MedicalCard medicalCard) throws DaoException;

    /**
     * Method allows to update the object data {@code MedicalCard} in the table medical_card.
     *
     * @param medicalCard contains necessary information to update query.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    void updateCard(MedicalCard medicalCard) throws DaoException;

    /**
     * Method gives object {@code MedicalCard} by id medical_card.
     *
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    MedicalCard getMedicalCard(long idMedicalCard) throws DaoException;

    /**
     * Method allows to get {@code idMedicalCard} using {@code idPerson}.
     *
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    long getIdMedicalCard(long idPerson) throws DaoException;
}
