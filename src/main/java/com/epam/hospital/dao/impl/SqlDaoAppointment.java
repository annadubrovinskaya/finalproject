package com.epam.hospital.dao.impl;

import com.epam.hospital.dao.DaoAppointment;
import com.epam.hospital.dao.connection.ConnectionPool;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.view.AppointmentVO;
import com.epam.hospital.domain.view.DoctorAppointment;
import com.epam.hospital.domain.view.PatientAppointment;
import com.epam.hospital.domain.bean.Appointment;
import com.epam.hospital.dao.exception.ConnectionPoolException;
import com.epam.hospital.model.exception.InvalidDataException;
import com.epam.hospital.model.util.DateTimeManager;
import com.mysql.jdbc.Statement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Class implements {@code DaoAppointment} interface and realize
 * its methods for {@code Appointment} objects.
 */

public class SqlDaoAppointment implements DaoAppointment {
    private static final String QUERY_FIND_APPOINTMENT_TIME = "SELECT appointment.time FROM appointment " +
            "WHERE appointment.date = ? AND id_doctor = ?";
    private static final String QUERY_INSERT_APPOINTMENT = "INSERT INTO appointment(date, time, id_medical_card, id_doctor) " +
            "values(?, ?, ?, ?)";
    private static final String QUERY_FIND_APPOINTMENT = "SELECT * FROM appointment " +
            "WHERE appointment.date = ? AND  id_doctor = ? AND id_medical_card = ?";
    private static final String QUERY_FIND_BOOKING_APPOINT = "SELECT * FROM appointment " +
            "WHERE appointment.id = ?";
    private static final String QUERY_DELETE_APPOINTMENT = "DELETE FROM appointment " +
            "WHERE appointment.id = ?";
    private static final String QUERY_FIND_APPOINTMENT_VO = "SELECT appointment.id, appointment.date, person.name, " +
            "person.second_name, person.patronymic FROM appointment " +
            "JOIN medical_card ON medical_card.id = appointment.id_medical_card " +
            "JOIN person ON person.id = medical_card.id_person " +
            "WHERE appointment.id = ?";
    private static final String QUERY_SELECT_DOCTOR_APPOINT = "SELECT appointment.date, appointment.time, " +
            "doctor.room_number, person.name, person.second_name, person.patronymic, speciality.name " +
            "FROM appointment " +
            "JOIN doctor ON doctor.id = appointment.id_doctor " +
            "JOIN speciality ON speciality.id = doctor.id_speciality " +
            "JOIN person ON person.id = doctor.id_person " +
            "WHERE appointment.id_medical_card = ?";
    private static final String QUERY_SELECT_APPOINT_BY_ID = "SELECT appointment.time, person.name, person.second_name, " +
            "person.patronymic FROM appointment " +
            "JOIN medical_card ON medical_card.id = appointment.id_medical_card " +
            "JOIN person ON person.id = medical_card.id_person " +
            "WHERE appointment.id_doctor = ? AND appointment.date = ?";
    private static final String QUERY_FIND_DATA_APPOINTMENT = "SELECT * FROM appointment WHERE id = ?";
    private static final String QUERY_DELETE_EXPIRED_TICKET = "DELETE FROM  appointment WHERE date < CURDATE()";

    private SqlDaoAppointment(){}

    private static class SingletonHolder {
        private static final SqlDaoAppointment INSTANCE = new SqlDaoAppointment();
    }

    public static SqlDaoAppointment getInstance() {
        return  SingletonHolder.INSTANCE;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoAppointment#getAppointments(LocalDate, long)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public List<Appointment> getAppointments(LocalDate date, long idDoctor) throws DaoException {
        List<Appointment> appointments = new ArrayList<>();

        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_FIND_APPOINTMENT_TIME)){
            statement.setString(1, date.toString());
            statement.setLong(2, idDoctor);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Appointment appointment = new Appointment();
                appointment.setTime(DateTimeManager.getTime(resultSet.getString(1)));

                appointments.add(appointment);
            }

        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method getDoctorAppointment", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method getDoctorAppointment", e);
        } catch (InvalidDataException e) {
            throw new DaoException("InvalidDataException in method getDoctorAppointment", e);
        }

        return appointments;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoAppointment#insert(Appointment)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public long insert(Appointment appointment) throws DaoException {
        long generatedId = 0;
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_INSERT_APPOINTMENT, Statement.RETURN_GENERATED_KEYS)){
            statement.setString(1, appointment.getDate().toString());
            statement.setString(2, appointment.getTime().toString());
            statement.setLong(3, appointment.getIdMedicalCard());
            statement.setLong(4, appointment.getIdDoctor());
            statement.executeUpdate();

            ResultSet resultSet = statement.getGeneratedKeys();
            resultSet.next();

            generatedId = resultSet.getLong(1);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method insert", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method insert", e);
        }

        return generatedId;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoAppointment#isWrittenReceipt(Appointment)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public boolean isWrittenReceipt(Appointment appointment) throws DaoException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_FIND_APPOINTMENT)){
            statement.setString(1, appointment.getDate().toString());
            statement.setLong(2, appointment.getIdDoctor());
            statement.setLong(3, appointment.getIdMedicalCard());

            ResultSet resultSet = statement.executeQuery();
            return resultSet.next();
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method isWrittenReceipt", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method isWrittenReceipt", e);
        }
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoAppointment#isBookingAppointment(long)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public boolean isBookingAppointment(long idAppointment) throws DaoException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_FIND_BOOKING_APPOINT)){
            statement.setLong(1, idAppointment);

            ResultSet resultSet = statement.executeQuery();
            return resultSet.next();
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method isBookingAppointment", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method isBookingAppointment", e);
        }
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoAppointment#deleteAppointment(long)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public void deleteAppointment(long idAppointment) throws DaoException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_DELETE_APPOINTMENT)){
            statement.setLong(1, idAppointment);
            statement.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method deleteAppointment", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method deleteAppointment", e);
        }
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoAppointment#getAppointmentVO(long)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public AppointmentVO getAppointmentVO(long idAppointment) throws DaoException {
        AppointmentVO appointmentVO = new AppointmentVO();

        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_FIND_APPOINTMENT_VO)){
            statement.setLong(1, idAppointment);

            ResultSet resultSet = statement.executeQuery();

            if(resultSet.next()) {
                appointmentVO.setId(resultSet.getLong(1));
                appointmentVO.setDate(DateTimeManager.getDate(resultSet.getString(2)));
                appointmentVO.setNamePatient(resultSet.getString(3));
                appointmentVO.setSecondNamePatient(resultSet.getString(4));
                appointmentVO.setPatronymicPatient(resultSet.getString(5));
            }

        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method getAppointmentVO", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method getAppointmentVO", e);
        } catch (InvalidDataException e) {
            throw new DaoException("InvalidDataException in method getAppointmentVO", e);
        }

        return appointmentVO;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoAppointment#getPatientAppointments(long, LocalDate)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public List<PatientAppointment> getPatientAppointments(long idDoctor, LocalDate date) throws DaoException {
        List<PatientAppointment> appointments = new ArrayList<>();

        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_SELECT_APPOINT_BY_ID)){
            statement.setLong(1, idDoctor);
            statement.setString(2, date.toString());

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                PatientAppointment appointment = new PatientAppointment();
                appointment.setTime(DateTimeManager.getTime(resultSet.getString(1)));
                appointment.setNamePatient(resultSet.getString(2));
                appointment.setSecondNamePatient(resultSet.getString(3));
                appointment.setPatronymicPatient(resultSet.getString(4));

                appointments.add(appointment);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method getPatientAppointments", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method getPatientAppointments", e);
        } catch (InvalidDataException e) {
            throw new DaoException("InvalidDataException in method getPatientAppointments", e);
        }

        return appointments;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoAppointment#getDoctorAppointments(long)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public List<DoctorAppointment> getDoctorAppointments(long idMedicalCard) throws DaoException {
        List<DoctorAppointment> appointments = new ArrayList<>();

        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_SELECT_DOCTOR_APPOINT)){
            statement.setLong(1, idMedicalCard);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                DoctorAppointment appointment = new DoctorAppointment();
                appointment.setDate(DateTimeManager.getDate(resultSet.getString(1)));
                appointment.setTime(DateTimeManager.getTime(resultSet.getString(2)));
                appointment.setRoomNumber(resultSet.getInt(3));
                appointment.setNameDoctor(resultSet.getString(4));
                appointment.setSecondNameDoctor(resultSet.getString(5));
                appointment.setPatronymicDoctor(resultSet.getString(6));
                appointment.setSpeciality(resultSet.getString(7));

                appointments.add(appointment);
            }

        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method getDoctorAppointments", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method getDoctorAppointments", e);
        } catch (InvalidDataException e) {
            throw new DaoException("InvalidDataException in method getDoctorAppointments", e);
        }

        return appointments;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoAppointment#getAppointment(long)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public Appointment getAppointment(long idAppointment) throws DaoException {
       Appointment appointment = new Appointment();

        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_FIND_DATA_APPOINTMENT)){
            statement.setLong(1, idAppointment);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                appointment.setId(resultSet.getLong(1));
                appointment.setDate(DateTimeManager.getDate(resultSet.getString(2)));
                appointment.setTime(DateTimeManager.getTime(resultSet.getString(3)));
                appointment.setIdMedicalCard(resultSet.getLong(4));
                appointment.setIdDoctor(resultSet.getLong(5));
            }

        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method getDoctorId", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method getDoctorId", e);
        } catch (InvalidDataException e) {
            throw new DaoException("InvalidDataException in method getDoctorId", e);
        }

        return appointment;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoAppointment#deleteExpiredTicket()
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public void deleteExpiredTicket() throws DaoException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_DELETE_EXPIRED_TICKET)){

            statement.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method deleteExpiredTicket", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method deleteExpiredTicket", e);
        }
    }
}