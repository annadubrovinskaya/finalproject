package com.epam.hospital.dao.impl;

import com.epam.hospital.dao.DaoDiagnosis;
import com.epam.hospital.dao.connection.ConnectionPool;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.Diagnosis;
import com.epam.hospital.dao.exception.ConnectionPoolException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class implements {@code DaoDiagnosis} interface and realize
 * its methods for {@code Diagnosis} objects.
 */

public class SqlDaoDiagnosis implements DaoDiagnosis {
    private static final String QUERY_SELECT_ALL_DIAGNOSIS = "SELECT * FROM diagnosis";
    private static final String QUERY_FIND_ID_DIAGNOSIS = "SELECT id FROM diagnosis WHERE name = ?";
    private static final String COLUMN_NAME = "name";

    private SqlDaoDiagnosis() {
    }

    private static class SingletonHolder {
        private static final SqlDaoDiagnosis INSTANCE = new SqlDaoDiagnosis();
    }

    public static SqlDaoDiagnosis getInstance() {
        return SingletonHolder.INSTANCE;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoDiagnosis#getDiagnosis()
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public List<Diagnosis> getDiagnosis() throws DaoException {
        List<Diagnosis> diagnosises = new ArrayList<>();

        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_SELECT_ALL_DIAGNOSIS)){
            ResultSet resultSet =  statement.executeQuery();

            while(resultSet.next()) {
                Diagnosis diagnosis = new Diagnosis();
                diagnosis.setName(resultSet.getString(COLUMN_NAME));
                diagnosises.add(diagnosis);
            }

        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method getDiagnosis", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method getDiagnosis", e);
        }

        return diagnosises;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoDiagnosis#getIdDiagnosis(String)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public long getIdDiagnosis(String diagnosisName) throws DaoException {
        long idDiagnosis = 0;

        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_FIND_ID_DIAGNOSIS)){
            statement.setString(1, diagnosisName);
            ResultSet resultSet =  statement.executeQuery();

            if(resultSet.next()) {
                idDiagnosis = resultSet.getLong(1);
            }

        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method getIdDiagnosis", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method getIdDiagnosis", e);
        }

        return idDiagnosis;
    }
}
