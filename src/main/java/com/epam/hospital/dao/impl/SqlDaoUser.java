package com.epam.hospital.dao.impl;

import com.epam.hospital.dao.DaoUser;
import com.epam.hospital.dao.connection.ConnectionPool;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.User;
import com.epam.hospital.dao.exception.ConnectionPoolException;

import java.sql.*;

/**
 * Class implements {@code DaoUser} interface and realize
 * its methods for {@code User} objects.
 */

public class SqlDaoUser implements DaoUser {
    private static final String QUERY_FIND_USER = "SELECT * FROM user WHERE login = ? AND password = ?";
    private static final String QUERY_INSERT_USER = "INSERT INTO user (login, password, id_role) values (?, ?, ?)";
    private static final String DELETE_USER = "DELETE FROM user WHERE login=? AND password=? AND id_role=?";

    private SqlDaoUser(){}

    private static class SingletonHolder {
        private static final SqlDaoUser INSTANCE = new SqlDaoUser();
    }

    public static SqlDaoUser getInstance() {
        return  SingletonHolder.INSTANCE;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoUser#findUser(String, String)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public User findUser(String login, String password) throws DaoException {
       User user = new User();

        try (Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(QUERY_FIND_USER)){
            statement.setString(1, login);
            statement.setString(2, password);
            statement.executeQuery();

            ResultSet resultSet = statement.getResultSet();
            if (resultSet.next()) {
                user.setId(resultSet.getLong(1));
                user.setIdRole(resultSet.getLong(4));
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method findUser", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method findUser", e);
        }

        return user;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoUser#insert(User)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public long insert(User user) throws DaoException {
        long generatedId = 0;

        try (Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(QUERY_INSERT_USER, Statement.RETURN_GENERATED_KEYS)){
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setLong(3, user.getIdRole());
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            resultSet.next();

            generatedId = resultSet.getLong(1);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method insert", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method insert", e);
        }

        return generatedId;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoUser#delete(User)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public void delete(User user) throws DaoException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_USER)) {

            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setLong(3, user.getIdRole());

            statement.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method insert", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method delete", e);
        }
    }
}
