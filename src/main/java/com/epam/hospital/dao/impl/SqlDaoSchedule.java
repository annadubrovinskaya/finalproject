package com.epam.hospital.dao.impl;

import com.epam.hospital.dao.DaoSchedule;
import com.epam.hospital.dao.connection.ConnectionPool;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.ScheduleVisit;
import com.epam.hospital.dao.exception.ConnectionPoolException;
import com.epam.hospital.model.exception.InvalidDataException;
import com.epam.hospital.model.util.DateTimeManager;

import java.sql.*;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

/**
 * Class implements {@code DaoSchedule} interface and realize
 * its methods for {@code Schedule} objects.
 */

public class SqlDaoSchedule implements DaoSchedule {
    private static final String QUERY_INSERT_SCHEDULE = "INSERT INTO schedule_visit(day_week, time_from, time_to, " +
            "id_doctor) VALUES(?, ?, ?, ?)";
    private static final String QUERY_DELETE_SCHEDULE = "DELETE FROM schedule_visit WHERE id_doctor = ?";
    private static final String QUERY_DELETE_SCHEDULE_BY_ID = "SELECT * FROM schedule_visit WHERE id_doctor = ?";

    private SqlDaoSchedule(){}

    private static class SingletonHolder {
        private static final SqlDaoSchedule INSTANCE = new SqlDaoSchedule();
    }

    public static SqlDaoSchedule getInstance() {
        return  SingletonHolder.INSTANCE;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoSchedule#insert(ScheduleVisit)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public void insert(ScheduleVisit scheduleVisit) throws DaoException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()){
            PreparedStatement statement = connection.prepareStatement(QUERY_INSERT_SCHEDULE);
            statement.setString(1, scheduleVisit.getDayWeek().toString().toLowerCase());
            statement.setString(2, scheduleVisit.getTimeFrom().toString());
            statement.setString(3, scheduleVisit.getTimeTo().toString());
            statement.setLong(4, scheduleVisit.getIdDoctor());
            statement.executeUpdate();

        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method insert", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method insert", e);
        }
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoSchedule#getSchedule(long)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public List<ScheduleVisit> getSchedule(long idDoctor) throws DaoException {
        List<ScheduleVisit> schedules;

        try (Connection connection = ConnectionPool.getInstance().getConnection()){
            PreparedStatement statement = connection.prepareStatement(QUERY_DELETE_SCHEDULE_BY_ID);
            statement.setLong(1, idDoctor);

            ResultSet resultSet = statement.executeQuery();
            schedules = getSchedulesVisit(resultSet);

        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method getSchedule", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method getSchedule", e);
        }  catch (InvalidDataException e) {
            throw new DaoException("InvalidDataException in method getSchedule", e);
        }

        return schedules;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoSchedule#deleteSchedule(long)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public void deleteSchedule(long idDoctor) throws DaoException {
        try (Connection connection = ConnectionPool.getInstance().getConnection()){
            PreparedStatement statement = connection.prepareStatement(QUERY_DELETE_SCHEDULE);
            statement.setLong(1, idDoctor);
            statement.executeUpdate();

        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method deleteSchedule", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method deleteSchedule", e);
        }
    }

    private List<ScheduleVisit> getSchedulesVisit(ResultSet resultSet) throws SQLException, InvalidDataException {
        List<ScheduleVisit> schedules = new ArrayList<>();

        while (resultSet.next()) {
            ScheduleVisit scheduleVisit = new ScheduleVisit();

            scheduleVisit.setId(resultSet.getLong(1));
            scheduleVisit.setDayWeek(DayOfWeek.valueOf(resultSet.getString(2).toUpperCase()));
            scheduleVisit.setTimeFrom(DateTimeManager.getTime(resultSet.getString(3)));
            scheduleVisit.setTimeTo(DateTimeManager.getTime(resultSet.getString(4)));
            scheduleVisit.setIdDoctor(resultSet.getLong(5));

            schedules.add(scheduleVisit);
        }

        return schedules;
    }
}
