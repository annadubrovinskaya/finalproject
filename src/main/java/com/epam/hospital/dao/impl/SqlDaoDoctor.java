package com.epam.hospital.dao.impl;

import com.epam.hospital.dao.DaoDoctor;
import com.epam.hospital.dao.connection.ConnectionPool;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.view.DoctorVO;
import com.epam.hospital.domain.bean.Doctor;
import com.epam.hospital.domain.bean.Speciality;
import com.epam.hospital.dao.exception.ConnectionPoolException;
import com.epam.hospital.model.exception.InvalidDataException;
import com.epam.hospital.model.util.DateTimeManager;

import java.sql.*;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

/**
 * Class implements {@code DaoDoctor} interface and realize
 * its methods for {@code Doctor} objects.
 */

public class SqlDaoDoctor implements DaoDoctor {
    private static final String QUERY_INSERT_DOCTOR = "INSERT INTO doctor (id, room_number, id_speciality, id_person) " +
            "values(?, ?, ?, ?)";
    private static final String QUERY_FIND_NULL_SCHEDULE = "SELECT person.name, person.second_name, " +
            "person.patronymic, speciality.name FROM doctor JOIN speciality ON speciality.id = doctor.id_speciality " +
            "JOIN person ON person.id = doctor.id_person " +
            "LEFT JOIN schedule_visit ON doctor.id=schedule_visit.id_doctor " +
            "WHERE schedule_visit.id_doctor IS NULL;";
    private static final String QUERY_SELECT_ID = "SELECT doctor.id FROM doctor " +
            "JOIN speciality ON speciality.id = doctor.id_speciality " +
            "JOIN person ON person.id = doctor.id_person " +
            "WHERE  person.name = ? AND person.second_name = ? AND person.patronymic = ? AND speciality.name = ?";
    private static final String QUERY_FIND_SPECIALITY = "SELECT * FROM doctor " +
            "JOIN speciality ON doctor.id_speciality = speciality.id " +
            "JOIN schedule_visit ON doctor.id = schedule_visit.id_doctor " +
            "WHERE speciality.name = ?";
    private static final String QUERY_FIND_ROOM_NUMBER = "SELECT room_number FROM doctor WHERE id = ?";
    private static final String QUERY_FIND_DOCTORS_CERT_SPECIALITY = "SELECT doctor.id, person.name, " +
            "person.second_name, person.patronymic, schedule_visit.time_from, schedule_visit.time_to " +
            "FROM doctor " +
            "JOIN speciality ON doctor.id_speciality = speciality.id " +
            "JOIN schedule_visit ON doctor.id = schedule_visit.id_doctor " +
            "JOIN person ON person.id = doctor.id_person " +
            "WHERE schedule_visit.day_week = ? AND speciality.name = ?";
    private static final String QUERY_FIND_DOCTOR_APPOINTMENT = "SELECT schedule_visit.time_from, schedule_visit.time_to, " +
            "speciality.name, person.name, person.second_name, person.patronymic FROM doctor " +
            "JOIN person ON person.id = doctor.id_person " +
            "JOIN speciality ON doctor.id_speciality = speciality.id " +
            "JOIN schedule_visit ON doctor.id = schedule_visit.id_doctor " +
            "WHERE schedule_visit.day_week = ? AND doctor.id = ?";
    private static final String QUERY_FIND_DOCTOR_SPECIALITY = "SELECT speciality.name FROM speciality " +
            "JOIN doctor ON speciality.id = doctor.id_speciality " +
            "WHERE doctor.id = ?;";

    private SqlDaoDoctor() {
    }

    private static class SingletonHolder {
        private static final SqlDaoDoctor INSTANCE = new SqlDaoDoctor();
    }

    public static SqlDaoDoctor getInstance() {
        return SingletonHolder.INSTANCE;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoDoctor#insert(Doctor)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public void insert(Doctor doctor) throws DaoException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_INSERT_DOCTOR)) {
            statement.setLong(1, doctor.getId());
            statement.setInt(2, doctor.getRoomNumber());
            statement.setLong(3, doctor.getIdSpeciality());
            statement.setLong(4, doctor.getIdPerson());
            statement.executeUpdate();

        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method insert", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method insert", e);
        }
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoDoctor#getDoctorsWithoutSchedule()
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public List<DoctorVO> getDoctorsWithoutSchedule() throws DaoException {
        List<DoctorVO> doctors = new ArrayList<>();

        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_FIND_NULL_SCHEDULE)) {
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                DoctorVO doctor = new DoctorVO();

                doctor.setName(resultSet.getString(1));
                doctor.setSecondName(resultSet.getString(2));
                doctor.setPatronymic(resultSet.getString(3));
                doctor.setSpeciality(resultSet.getString(4));

                doctors.add(doctor);
            }

        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method getDoctorsWithoutSchedule", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method getDoctorsWithoutSchedule", e);
        }

        return doctors;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoDoctor#getDoctorsVO(Speciality, DayOfWeek)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public List<DoctorVO> getDoctorsVO(Speciality speciality, DayOfWeek dayOfWeek) throws DaoException {
        List<DoctorVO> doctorsVO = new ArrayList<>();

        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_FIND_DOCTORS_CERT_SPECIALITY)) {
            statement.setString(1, dayOfWeek.toString().toLowerCase());
            statement.setString(2, speciality.getName());
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                DoctorVO doctorVO = new DoctorVO();

                doctorVO.setId(resultSet.getLong(1));
                doctorVO.setName(resultSet.getString(2));
                doctorVO.setSecondName(resultSet.getString(3));
                doctorVO.setPatronymic(resultSet.getString(4));
                doctorVO.setTimeFrom(DateTimeManager.getTime(resultSet.getString(5)));
                doctorVO.setTimeTo(DateTimeManager.getTime(resultSet.getString(6)));

                doctorsVO.add(doctorVO);
            }

        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method getDoctorsVO", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method getDoctorsVO", e);
        } catch (InvalidDataException e) {
            throw new DaoException("InvalidDataException in method getDoctorsVO", e);
        }

        return doctorsVO;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoDoctor#getDoctorVO(long, DayOfWeek)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public DoctorVO getDoctorVO(long idDoctor, DayOfWeek dayOfWeek) throws DaoException {
        DoctorVO doctorVO = new DoctorVO();

        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_FIND_DOCTOR_APPOINTMENT)) {

            statement.setString(1, dayOfWeek.toString().toLowerCase());
            statement.setLong(2, idDoctor);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                doctorVO.setTimeFrom(DateTimeManager.getTime(resultSet.getString(1)));
                doctorVO.setTimeTo(DateTimeManager.getTime(resultSet.getString(2)));
                doctorVO.setId(idDoctor);
                doctorVO.setSpeciality(resultSet.getString(3));
                doctorVO.setName(resultSet.getString(4));
                doctorVO.setSecondName(resultSet.getString(5));
                doctorVO.setPatronymic(resultSet.getString(6));
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method getDoctorVO", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method getDoctorVO", e);
        } catch (InvalidDataException e) {
            throw new DaoException("InvalidDataException in method getDoctorVO", e);
        }

        return doctorVO;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoDoctor#hasSpecialitySchedule(String)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public boolean hasSpecialitySchedule(String specialityName) throws DaoException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_FIND_SPECIALITY)) {
            statement.setString(1, specialityName);
            ResultSet resultSet = statement.executeQuery();

            return resultSet.next();
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method hasSpeciality", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method hasSpeciality", e);
        }
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoDoctor#getDoctorId(Person, String)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public long getDoctorId(Person person, String specialityName) throws DaoException {
        long doctorId = 0;

        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_SELECT_ID)) {
            statement.setString(1, person.getName());
            statement.setString(2, person.getSecondName());
            statement.setString(3, person.getPatronymic());
            statement.setString(4, specialityName);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                doctorId = resultSet.getLong(1);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method getDoctorId", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method getDoctorId", e);
        }

        return doctorId;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoDoctor#getRoomNumber(long)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public int getRoomNumber(long idDoctor) throws DaoException {
        int roomNumber = 0;

        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_FIND_ROOM_NUMBER)) {
            statement.setLong(1, idDoctor);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                roomNumber = resultSet.getInt(1);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method getRoomNumber", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method getRoomNumber", e);
        }

        return roomNumber;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoDoctor#findDoctorSpeciality(long)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public String findDoctorSpeciality(long idDoctor) throws DaoException {
        String specialityName = null;

        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_FIND_DOCTOR_SPECIALITY)) {
            statement.setLong(1, idDoctor);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                specialityName = resultSet.getString(1);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method findDoctorSpeciality", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method findDoctorSpeciality", e);
        }

        return specialityName;
    }
}