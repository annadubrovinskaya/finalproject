package com.epam.hospital.dao.impl;

import com.epam.hospital.dao.DaoMedicalCard;
import com.epam.hospital.dao.connection.ConnectionPool;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.MedicalCard;
import com.epam.hospital.dao.exception.ConnectionPoolException;

import java.sql.*;

/**
 * Class implements {@code DaoMedicalCard} interface and realize
 * its methods for {@code MedicalCard} objects.
 */

public class SqlDaoMedicalCard implements DaoMedicalCard {
    private static final String QUERY_INSERT_MEDICAL_CARD = "INSERT INTO medical_card (height_patient, weight_patient," +
            "blood_type, id_person) values (?, ?, ?, ?)";
    private static final String QUERY_UPDATE_CARD = "UPDATE medical_card SET height_patient = ?, weight_patient = ? " +
            "WHERE id = ?";
    private static final String QUERY_FIND_PATIENT_CARD = "SELECT * FROM medical_card WHERE id = ?";
    private static final String QUERY_FIND_ID_CARD = "SELECT id FROM medical_card WHERE id_person = ?";

    private SqlDaoMedicalCard() {
    }

    private static class SingletonHolder {
        private static final SqlDaoMedicalCard INSTANCE = new SqlDaoMedicalCard();
    }

    public static SqlDaoMedicalCard getInstance() {
        return SingletonHolder.INSTANCE;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoMedicalCard#insert(MedicalCard)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public long insert(MedicalCard medicalCard) throws DaoException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_INSERT_MEDICAL_CARD, Statement.RETURN_GENERATED_KEYS)) {

            statement.setFloat(1, medicalCard.getHeightPatient());
            statement.setFloat(2, medicalCard.getWeightPatient());
            statement.setString(3, medicalCard.getBloodType());
            statement.setLong(4, medicalCard.getIdPerson());
            statement.executeUpdate();

            ResultSet resultSet = statement.getGeneratedKeys();
            resultSet.next();

            return resultSet.getLong(1);

        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method insert", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method insert", e);
        }
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoMedicalCard#updateCard(MedicalCard)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public void updateCard(MedicalCard medicalCard) throws DaoException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_UPDATE_CARD)) {
            statement.setFloat(1, medicalCard.getHeightPatient());
            statement.setFloat(2, medicalCard.getWeightPatient());
            statement.setLong(3, medicalCard.getId());
            statement.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method updateCard", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method updateCard", e);
        }
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoMedicalCard#getIdMedicalCard(long)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public MedicalCard getMedicalCard(long idMedicalCard) throws DaoException {
        MedicalCard medicalCard = new MedicalCard();

        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_FIND_PATIENT_CARD)) {
            statement.setLong(1, idMedicalCard);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                medicalCard.setId(resultSet.getLong(1));
                medicalCard.setHeightPatient(resultSet.getFloat(2));
                medicalCard.setWeightPatient(resultSet.getFloat(3));
                medicalCard.setBloodType(resultSet.getString(4));
                medicalCard.setIdPerson(resultSet.getLong(5));
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method getCardPatient", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method getCardPatient", e);
        }

        return medicalCard;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoMedicalCard#getIdMedicalCard(long)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public long getIdMedicalCard(long idPerson) throws DaoException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_FIND_ID_CARD)) {

            statement.setLong(1, idPerson);

            ResultSet resultSet = statement.executeQuery();
            long idMedicalCard = 0;

            if (resultSet.next()) {
               idMedicalCard = resultSet.getLong(1);
            }

            return idMedicalCard;

        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method getCardPatient", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method getCardPatient", e);
        }
    }
}
