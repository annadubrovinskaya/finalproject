package com.epam.hospital.dao.impl;

import com.epam.hospital.dao.DaoSpeciality;
import com.epam.hospital.dao.connection.ConnectionPool;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.Speciality;
import com.epam.hospital.dao.exception.ConnectionPoolException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class implements {@code DaoSpeciality} interface and realize
 * its methods for {@code Speciality} objects.
 */

public class SqlDaoSpeciality implements DaoSpeciality {
    private static final String QUERY_FIND_ID = "SELECT id FROM speciality WHERE name = ?";
    private static final String QUERY_SELECT_ALL_SPECIALITIES = "SELECT name FROM speciality";
    private static final int DEFAULT_INT_VALUE = 0;

    private SqlDaoSpeciality(){}

    private static class SingletonHolder {
        private static final SqlDaoSpeciality INSTANCE = new SqlDaoSpeciality();
    }

    public static SqlDaoSpeciality getInstance() {
        return  SingletonHolder.INSTANCE;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoSpeciality#getId(Speciality)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public long getId(Speciality speciality) throws DaoException {
        long generatedId = DEFAULT_INT_VALUE;

        try (Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(QUERY_FIND_ID)){
            statement.setString(1, speciality.getName());
            statement.executeQuery();

            ResultSet resultSet = statement.getResultSet();

            if(resultSet.next()) {
                generatedId = resultSet.getLong(1);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method getId", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method getId", e);
        }

        return generatedId;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoSpeciality#getListSpecialities()
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public List<Speciality> getListSpecialities() throws DaoException {
        List<Speciality> specialities = new ArrayList<>();

        try (Connection connection = ConnectionPool.getInstance().getConnection();
            PreparedStatement statement = connection.prepareStatement(QUERY_SELECT_ALL_SPECIALITIES)){
            ResultSet resultSet =  statement.executeQuery();

            while(resultSet.next()) {
                Speciality speciality = new Speciality();
                speciality.setName(resultSet.getString(1));
                specialities.add(speciality);
            }

        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method getListSpecialities", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method getListSpecialities", e);
        }

        return specialities;
    }
}
