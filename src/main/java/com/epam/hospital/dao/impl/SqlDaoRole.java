package com.epam.hospital.dao.impl;

import com.epam.hospital.dao.DaoRole;
import com.epam.hospital.dao.connection.ConnectionPool;
import com.epam.hospital.dao.exception.ConnectionPoolException;
import com.epam.hospital.dao.exception.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Class implements {@code DaoRole} interface and realize
 * its methods for {@code Role} objects.
 */

public class SqlDaoRole implements DaoRole {
    private static final String QUERY_FIND_ROLE = "SELECT name FROM role WHERE id = ?";

    private SqlDaoRole(){}

    private static class SingletonHolder {
        private static final SqlDaoRole INSTANCE = new SqlDaoRole();
    }

    public static SqlDaoRole getInstance() {
        return  SqlDaoRole.SingletonHolder.INSTANCE;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoRole#getRoleName(long)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public String getRoleName(long idRole) throws DaoException {
        String role = null;

        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_FIND_ROLE)){
            statement.setLong(1, idRole);
            statement.executeQuery();

            ResultSet resultSet = statement.getResultSet();
            if (resultSet.next()) {
               role = resultSet.getString(1);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method findUser", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method findUser", e);
        }

        return role;
    }
}
