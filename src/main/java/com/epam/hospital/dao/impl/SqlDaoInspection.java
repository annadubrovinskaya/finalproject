package com.epam.hospital.dao.impl;

import com.epam.hospital.dao.DaoInspection;
import com.epam.hospital.dao.connection.ConnectionPool;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.view.InspectionVO;
import com.epam.hospital.domain.bean.Inspection;
import com.epam.hospital.dao.exception.ConnectionPoolException;
import com.epam.hospital.model.exception.InvalidDataException;
import com.epam.hospital.model.util.DateTimeManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class implements {@code DaoInspection} interface and realize
 * its methods for {@code Inspection} objects.
 */

public class SqlDaoInspection implements DaoInspection {
    private static final String QUERY_INSERT_DOCTOR = "INSERT INTO inspection (date, conclusion, id_diagnosis, id_medical_card, " +
            "id_doctor) values(?, ?, ?, ?, ?)";
    private static final String QUERY_FIND_INSPECTION = "SELECT inspection.date, person.name, person.second_name, " +
            "person.patronymic, speciality.name, inspection.conclusion, diagnosis.name " +
            "FROM inspection " +
            "JOIN diagnosis ON inspection.id_diagnosis = diagnosis.id " +
            "JOIN doctor ON inspection.id_doctor = doctor.id " +
            "JOIN speciality ON doctor.id_speciality = speciality.id " +
            "JOIN person ON person.id = doctor.id_person " +
            "WHERE inspection.id_medical_card = ?";

    private SqlDaoInspection(){}

    private static class SingletonHolder {
        private static final SqlDaoInspection INSTANCE = new SqlDaoInspection();
    }

    public static SqlDaoInspection getInstance() {
        return  SingletonHolder.INSTANCE;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoInspection#insert(Inspection)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public void insert(Inspection inspection) throws DaoException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_INSERT_DOCTOR)) {
            statement.setString(1, inspection.getDate().toString());
            statement.setString(2, inspection.getConclusion());
            statement.setLong(3, inspection.getIdDiagnosis());
            statement.setLong(4, inspection.getIdMedicalCard());
            statement.setLong(5, inspection.getIdDoctor());

            statement.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method insert", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method insert", e);
        }
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoInspection#getInspectionVO(long)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public List<InspectionVO> getInspectionVO(long idMedicalCard) throws DaoException {
        List<InspectionVO> inspectionsVO = new ArrayList<>();
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_FIND_INSPECTION)) {
            statement.setLong(1, idMedicalCard);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                InspectionVO inspectionVO = new InspectionVO();

                inspectionVO.setDate(DateTimeManager.getDate(resultSet.getString(1)));
                inspectionVO.setNameDoctor(resultSet.getString(2));
                inspectionVO.setSecondNameDoctor(resultSet.getString(3));
                inspectionVO.setPatronymicDoctor(resultSet.getString(4));
                inspectionVO.setSpeciality(resultSet.getString(5));
                inspectionVO.setConclusion(resultSet.getString(6));
                inspectionVO.setDiagnosis(resultSet.getString(7));

                inspectionsVO.add(inspectionVO);
            }

        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method getInspectionVO", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method getInspectionVO", e);
        } catch (InvalidDataException e) {
            throw new DaoException("InvalidDataException in method getInspectionVO", e);
        }

        return inspectionsVO;
    }
}
