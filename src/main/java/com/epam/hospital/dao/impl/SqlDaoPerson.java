package com.epam.hospital.dao.impl;

import com.epam.hospital.dao.DaoPerson;
import com.epam.hospital.dao.connection.ConnectionPool;
import com.epam.hospital.dao.exception.ConnectionPoolException;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.Person;

import java.sql.*;

/**
 * Class implements {@code DaoPerson} interface and realize
 * its methods for {@code Person} objects.
 */

public class SqlDaoPerson implements DaoPerson{
    private static final String QUERY_INSERT_PERSON_DATA = "INSERT INTO person " +
            "(name, second_name, patronymic, sex, date_birth) values (?, ?, ?, ?, ?)";
    private static final String QUERY_DELETE_PD = "DELETE FROM person WHERE name = ? AND second_name = ? " +
            "AND patronymic = ? AND date_birth = ?";
    private static final String QUERY_FIND_ID = "SELECT id FROM person WHERE name = ? AND second_name = ? AND " +
            "patronymic = ? AND date_birth = ?";

    private SqlDaoPerson(){}

    private static class SingletonHolder {
        private static final SqlDaoPerson INSTANCE = new SqlDaoPerson();
    }

    public static SqlDaoPerson getInstance() {
        return  SqlDaoPerson.SingletonHolder.INSTANCE;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoPerson#insert(Person)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public long insert(Person person) throws DaoException {
        long generatedId = 0;

        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_INSERT_PERSON_DATA, Statement.RETURN_GENERATED_KEYS)){
            statement.setString(1, person.getName());
            statement.setString(2, person.getSecondName());
            statement.setString(3, person.getPatronymic());
            statement.setString(4, person.getSex());
            statement.setString(5, person.getDateOfBirth().toString());
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            resultSet.next();

            generatedId = resultSet.getLong(1);
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method insert", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method insert", e);
        }

        return generatedId;
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoPerson#delete(Person)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public void delete(Person person) throws DaoException {
        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_DELETE_PD)){
            statement.setString(1, person.getName());
            statement.setString(2, person.getSecondName());
            statement.setString(3, person.getPatronymic());
            statement.setString(4, person.getDateOfBirth().toString());

            statement.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method delete", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method delete", e);
        }
    }

    /**
     * First, method is taken {@code Connection} from {@code ConnectionPool}.
     * Then {@code PrepareStatement} are taken from {@code Connection} and all
     * dynamic parameters are filling. After that step {@code PrepareStatement}
     * is executed.
     *
     * @see DaoPerson#getIdPerson(Person)
     *
     * @throws DaoException if occurs errors while getting {@code Connection}
     *  or tying to execute sql statement.
     */

    @Override
    public long getIdPerson(Person person) throws DaoException {
        long idPerson = 0;

        try (Connection connection = ConnectionPool.getInstance().getConnection();
             PreparedStatement statement = connection.prepareStatement(QUERY_FIND_ID)){

            statement.setString(1, person.getName());
            statement.setString(2, person.getSecondName());
            statement.setString(3, person.getPatronymic());
            statement.setString(4, person.getDateOfBirth().toString());

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                idPerson = resultSet.getLong(1);
            }

            return idPerson;

        } catch (ConnectionPoolException e) {
            throw new DaoException("ConnectionPoolException in method delete", e);
        } catch (SQLException e) {
            throw new DaoException("SQLException in method delete", e);
        }
    }
}
