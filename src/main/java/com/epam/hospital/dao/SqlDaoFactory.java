package com.epam.hospital.dao;

import com.epam.hospital.dao.impl.*;

public class SqlDaoFactory extends DaoFactory{
    private SqlDaoFactory(){}

    private static class SingletonHolder {
        private static final SqlDaoFactory INSTANCE = new SqlDaoFactory();
    }

    public static SqlDaoFactory getInstance() {
        return  SingletonHolder.INSTANCE;
    }

    @Override
    public DaoUser getDaoUser() {
        return SqlDaoUser.getInstance();
    }

    @Override
    public DaoDoctor getDaoDoctor() {
        return SqlDaoDoctor.getInstance();
    }

    @Override
    public DaoSpeciality getDaoSpeciality() {
        return SqlDaoSpeciality.getInstance();
    }

    @Override
    public DaoMedicalCard getDaoMedicalCard() {
        return SqlDaoMedicalCard.getInstance();
    }

    @Override
    public DaoSchedule getDaoSchedule() {
        return SqlDaoSchedule.getInstance();
    }

    @Override
    public DaoAppointment getDaoAppointment() {
        return SqlDaoAppointment.getInstance();
    }

    @Override
    public DaoDiagnosis getDaoDiagnosis() {
        return SqlDaoDiagnosis.getInstance();
    }

    @Override
    public DaoInspection getDaoInspection() {
        return SqlDaoInspection.getInstance();
    }

    @Override
    public DaoPerson getDaoPerson() {
        return SqlDaoPerson.getInstance();
    }

    @Override
    public DaoRole getDaoRole() {
        return SqlDaoRole.getInstance();
    }
}
