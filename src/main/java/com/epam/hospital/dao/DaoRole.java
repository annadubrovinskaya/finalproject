package com.epam.hospital.dao;

import com.epam.hospital.dao.exception.DaoException;

/**
 * Interface defines methods for working with the table role
 * in database hospital.
 */

public interface DaoRole {

    /**
     * Method returns name role which corresponds to the {@code idRole}.
     *
     * @return {@code roleName} that contains role name.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    String getRoleName(long idRole) throws DaoException;
}
