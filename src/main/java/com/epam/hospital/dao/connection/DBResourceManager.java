package com.epam.hospital.dao.connection;

import java.util.ResourceBundle;

public class DBResourceManager {
    private static final String DB_PROPERTIES_PATH = "db/db";
    private ResourceBundle bundle;

    private DBResourceManager() {
        bundle = ResourceBundle.getBundle(DB_PROPERTIES_PATH);
    }

    private static class SingletonHolder {
        private static final DBResourceManager INSTANCE = new DBResourceManager();
    }

    public static DBResourceManager getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public String getValue(String key) {
        return bundle.getString(key);
    }
}