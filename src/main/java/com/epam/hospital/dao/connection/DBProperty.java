package com.epam.hospital.dao.connection;

public enum DBProperty {
    DB_DRIVER("db.driver"), DB_URL("db.url"), DB_USER("db.user"),
    DB_PASSWORD("db.password"), DB_POOL_SIZE("db.poolsize");

    private String value;

    DBProperty(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}