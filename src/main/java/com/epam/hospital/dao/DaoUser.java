package com.epam.hospital.dao;

import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.User;

/**
 * Interface defines methods for working with the table user
 * in database hospital.
 */

public interface DaoUser {

    /**
     * Method allows to find user using {@code login} and {@code password}.
     *
     * @return {@code User} that contains {@code id} and {@code idRole}.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    User findUser(String login, String password) throws DaoException;

    /**
     * Method allows to insert the object data {@code User} in the table user.
     *
     * @param user contains all information to be entered in the table user.
     * @return {@code idUser} that contains id inserted record.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    long insert(User user) throws DaoException;

    /**
     * Method allows to delete record from the table user.
     *
     * @param user contains all information to be deleted from the table user.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    void delete(User user) throws DaoException;
}