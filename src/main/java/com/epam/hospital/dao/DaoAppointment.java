package com.epam.hospital.dao;

import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.view.AppointmentVO;
import com.epam.hospital.domain.view.DoctorAppointment;
import com.epam.hospital.domain.view.PatientAppointment;
import com.epam.hospital.domain.bean.Appointment;

import java.time.LocalDate;
import java.util.List;

/**
 * Interface defines methods for working with the table appointment
 * in database hospital.
 */

public interface DaoAppointment {

    /**
     * Method gives the list of {@code Appointment}.
     *
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    List<Appointment> getAppointments(LocalDate date, long idDoctor) throws DaoException;

    /**
     * Method allows to insert the object data {@code Appointment} in the table appointment.
     *
     * @param appointment contains all information to be entered in the table appointment.
     * @return {@code idAppointment} that contains id inserted record.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    long insert(Appointment appointment) throws DaoException;

    /**
     * Method allows to check whether there is such an entry in the table appointment.
     *
     * @param appointment contains necessary information to do the query.
     * @return true if there is a record in the table, else false.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    boolean isWrittenReceipt(Appointment appointment) throws DaoException;

    /**
     * Method allows to check whether there is such a record in the database, if it exists, returns
     *  true, else false.
     *
     * @return true if there is a record in the table appointment, else false.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    boolean isBookingAppointment(long idAppointment) throws DaoException;

    /**
     * Method allows to delete record from the table appointment.
     *
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    void deleteAppointment(long idAppointment) throws DaoException;

    /**
     * Method gives the object {@code AppointmentVO} that contains necessary information for use.
     *
     * @return {@code AppointmentVO} that contains necessary information for use.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    AppointmentVO getAppointmentVO(long idAppointment) throws DaoException;

    /**
     * Method gives the list of {@code PatientAppointment}.
     *
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    List<PatientAppointment> getPatientAppointments(long idDoctor, LocalDate date) throws DaoException;

    /**
     * Method gives the list of {@code DoctorAppointment}.
     *
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    List<DoctorAppointment> getDoctorAppointments(long idMedicalCard) throws DaoException;

    /**
     * Method gives the object Appointment.
     *
     * @return {@code Appointment} that contains necessary information for use.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    Appointment getAppointment(long idAppointment) throws DaoException;

    /**
     * Method delete all expired tickets.
     *
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    void deleteExpiredTicket() throws DaoException;
}