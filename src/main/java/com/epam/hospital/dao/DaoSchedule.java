package com.epam.hospital.dao;

import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.ScheduleVisit;

import java.util.List;

/**
 * Interface defines methods for working with the table schedule_visit
 * in database hospital.
 */

public interface DaoSchedule {

    /**
     * Method allows to insert the object data {@code ScheduleVisit} in the table schedule_visit.
     *
     * @param scheduleVisit contains all information to be entered in the table schedule_visit.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    void insert(ScheduleVisit scheduleVisit) throws DaoException;

    /**
     * Method gives the list of {@code ScheduleVisit}.
     *
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    List<ScheduleVisit> getSchedule(long idDoctor) throws DaoException;

    /**
     * Method allows to delete record from the table schedule_visit.
     *
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    void deleteSchedule(long idDoctor) throws DaoException;
}