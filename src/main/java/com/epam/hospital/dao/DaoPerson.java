package com.epam.hospital.dao;

import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.Person;

/**
 * Interface defines methods for working with the table person
 * in database hospital.
 */

public interface DaoPerson {

    /**
     * Method allows to insert the object data {@code Person} in the table person.
     *
     * @param person contains all information to be entered in the table person.
     * @return {@code idPerson} that contains id inserted record.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    long insert(Person person) throws DaoException;

    /**
     * Method allows to delete record in the table person.
     *
     * @param person contains all information to be deleted from the table person.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    void delete(Person person) throws DaoException;

    /**
     * Method allows to get {@code idMedicalCard} using {@code Person} object.
     *
     * @param person contains all information to do the query.
     * @return {@code idPerson}.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    long getIdPerson(Person person) throws DaoException;
}
