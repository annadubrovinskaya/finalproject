package com.epam.hospital.dao;

public abstract class DaoFactory {
    public abstract DaoUser getDaoUser();
    public abstract DaoDoctor getDaoDoctor();
    public abstract DaoSpeciality getDaoSpeciality();
    public abstract DaoMedicalCard getDaoMedicalCard();
    public abstract DaoSchedule getDaoSchedule();
    public abstract DaoAppointment getDaoAppointment();
    public abstract DaoDiagnosis getDaoDiagnosis();
    public abstract DaoInspection getDaoInspection();
    public abstract DaoPerson getDaoPerson();
    public abstract DaoRole getDaoRole();
}
