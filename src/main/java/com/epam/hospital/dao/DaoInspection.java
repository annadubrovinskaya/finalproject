package com.epam.hospital.dao;

import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.view.InspectionVO;
import com.epam.hospital.domain.bean.Inspection;

import java.util.List;

/**
 * Interface defines methods for working with the table inspection
 * in database hospital.
 */

public interface DaoInspection {

    /**
     * Method allows to insert the object data {@code Inspection} in the table inspection.
     *
     * @param inspection contains necessary information to do the query.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    void insert(Inspection inspection) throws DaoException;

    /**
     * Method gives the list of {@code InspectionVO}.
     *
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    List<InspectionVO> getInspectionVO(long idMedicalCard) throws DaoException;
}
