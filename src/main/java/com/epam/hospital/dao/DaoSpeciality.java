package com.epam.hospital.dao;

import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.Speciality;

import java.util.List;

/**
 * Interface defines methods for working with the table speciality
 * in database hospital.
 */

public interface DaoSpeciality {

    /**
     * Method gives id diagnosis which corresponds to the {@code speciality}.
     *
     * @param speciality contains {@code nameSpeciality}.
     * @return {@code idSpeciality} which corresponds to the {@code speciality}.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    long getId(Speciality speciality) throws DaoException;

    /**
     * Method gives the list of all {@code Speciality}.
     *
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    List<Speciality> getListSpecialities() throws DaoException;
}
