package com.epam.hospital.dao;

import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.view.DoctorVO;
import com.epam.hospital.domain.bean.Doctor;
import com.epam.hospital.domain.bean.Speciality;

import java.time.DayOfWeek;
import java.util.List;

/**
 * Interface defines methods for working with the table doctor
 * in database hospital.
 */

public interface DaoDoctor {

    /**
     * Method allows to insert the object data {@code Doctor} in the table doctor.
     *
     * @param doctor contains necessary information to do the query.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    void insert(Doctor doctor) throws DaoException;

    /**
     * Method gives the list of {@code DoctorVO} that contains doctors without schedule.
     *
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    List<DoctorVO> getDoctorsWithoutSchedule() throws DaoException;

    /**
     * Method gives the list of {@code DoctorVO}.
     *
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    List<DoctorVO> getDoctorsVO(Speciality speciality, DayOfWeek dayOfWeek) throws DaoException;

    /**
     * Method gives the object {@code DoctorVO} using {@code idDoctor} and {@code dayOfWeek}.
     *
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    DoctorVO getDoctorVO(long idDoctor, DayOfWeek dayOfWeek) throws DaoException;

    /**
     * Method check are there doctors with a specialty, and whether they have a schedule.
     *
     * @return true if there are doctors with speciality and they have a schedule, else false
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    boolean hasSpecialitySchedule(String specialityName) throws DaoException;

    /**
     * Method gives id doctor.
     *
     * @param person contains necessary information about doctor.
     * @return {@code idDoctor}.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    long getDoctorId(Person person, String specialityName) throws DaoException;

    /**
     * Method gives room number.
     *
     * @return {@code roomNumber}.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    int getRoomNumber(long idDoctor) throws DaoException;

    /**
     * Method gives name speciality using doctor id.
     *
     * @return {@code specialityName}.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    String findDoctorSpeciality(long idDoctor) throws DaoException;
}
