package com.epam.hospital.dao;

import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.Diagnosis;

import java.util.List;

/**
 * Interface defines methods for working with the table diagnosis
 * in database hospital.
 */

public interface DaoDiagnosis {

    /**
     * Method gives the list of {@code Diagnosis}.
     *
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    List<Diagnosis> getDiagnosis() throws DaoException;

    /**
     * Method gives id diagnosis which corresponds to the {@code diagnosisName}.
     *
     * @return {@code idDiagnosis} which corresponds to the {@code diagnosisName}.
     * @throws DaoException when occur exception while getting connection from {@code ConnectionPool} or
     *  trying execute sql query.
     */

    long getIdDiagnosis(String diagnosisName) throws DaoException;
}