package com.epam.hospital.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UserAccessFilter implements Filter {
    private static final String ROLE = "role";
    private static final String USER_ROLE = "doctor";
    private static final String ERROR_PAGE = "/error";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;

        String role = (String) request.getSession().getAttribute(ROLE);

        if (role != null && !role.equals(USER_ROLE)) {
            ((HttpServletResponse)servletResponse).sendRedirect(ERROR_PAGE);
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
