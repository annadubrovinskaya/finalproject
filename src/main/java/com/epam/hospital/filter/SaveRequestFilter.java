package com.epam.hospital.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SaveRequestFilter implements Filter {
    private static final String REGEX_FIND_REMOVE_PREFIX = "^remove_.*";
    private static final int INDEX_ATTRIBUTE = 7;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;

        HttpSession session = request.getSession();

        Enumeration<String> attributeNames = session.getAttributeNames();

        while (attributeNames.hasMoreElements()) {
            String attribute = attributeNames.nextElement();
            Pattern pattern = Pattern.compile(REGEX_FIND_REMOVE_PREFIX);
            Matcher matcher = pattern.matcher(attribute);

            if (matcher.find()) {
                String trueAttribute = attribute.substring(INDEX_ATTRIBUTE);
                request.setAttribute(trueAttribute, session.getAttribute(attribute));
                session.removeAttribute(attribute);
            }
        }
        filterChain.doFilter(request, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
