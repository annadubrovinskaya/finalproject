package com.epam.hospital.view;


import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class FootTag extends TagSupport {
    private static final String BEFORE_TITLE = "<p>&copy;2016 <br/><em>";
    private static final String AFTER_TITLE = "</em></p>";

    private String title;

    public int doStartTag() throws JspException {
        try {
            JspWriter writer = pageContext.getOut();

            writer.write(BEFORE_TITLE);
            writer.write(title);
            writer.write(AFTER_TITLE);
        } catch (IOException ioe) {
            throw new JspTagException(ioe.getMessage());
        }
        return SKIP_BODY;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void release() {
        super.release();
        title = null;
    }
}
