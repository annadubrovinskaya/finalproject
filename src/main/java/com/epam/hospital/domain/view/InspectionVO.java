package com.epam.hospital.domain.view;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class InspectionVO implements Serializable {
    private LocalDate date;
    private String nameDoctor;
    private String secondNameDoctor;
    private String patronymicDoctor;
    private String speciality;
    private String conclusion;
    private String diagnosis;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getNameDoctor() {
        return nameDoctor;
    }

    public void setNameDoctor(String nameDoctor) {
        this.nameDoctor = nameDoctor;
    }

    public String getSecondNameDoctor() {
        return secondNameDoctor;
    }

    public void setSecondNameDoctor(String secondNameDoctor) {
        this.secondNameDoctor = secondNameDoctor;
    }

    public String getPatronymicDoctor() {
        return patronymicDoctor;
    }

    public void setPatronymicDoctor(String patronymicDoctor) {
        this.patronymicDoctor = patronymicDoctor;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InspectionVO that = (InspectionVO) o;

        return Objects.equals(date, that.date) && Objects.equals(nameDoctor, that.nameDoctor) &&
                Objects.equals(secondNameDoctor, that.secondNameDoctor) &&
                Objects.equals(patronymicDoctor, that.patronymicDoctor) &&
                Objects.equals(speciality, that.speciality) && Objects.equals(conclusion, that.conclusion) &&
                Objects.equals(diagnosis, that.diagnosis);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, nameDoctor, secondNameDoctor, patronymicDoctor, speciality, conclusion, diagnosis);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("InspectionVO{date=");
        sb.append(date);
        sb.append(", nameDoctor='");
        sb.append(nameDoctor);
        sb.append("', secondNameDoctor='");
        sb.append(secondNameDoctor);
        sb.append("', patronymicDoctor='");
        sb.append(patronymicDoctor);
        sb.append("', speciality='");
        sb.append(speciality);
        sb.append("', conclusion='");
        sb.append(conclusion);
        sb.append("', diagnosis=");
        sb.append(diagnosis);
        sb.append("}");

        return sb.toString();
    }
}
