package com.epam.hospital.domain.view;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

public class DoctorAppointment implements Serializable {
    private LocalDate date;
    private LocalTime time;
    private int roomNumber;
    private String nameDoctor;
    private String secondNameDoctor;
    private String patronymicDoctor;
    private String speciality;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getNameDoctor() {
        return nameDoctor;
    }

    public void setNameDoctor(String nameDoctor) {
        this.nameDoctor = nameDoctor;
    }

    public String getSecondNameDoctor() {
        return secondNameDoctor;
    }

    public void setSecondNameDoctor(String secondNameDoctor) {
        this.secondNameDoctor = secondNameDoctor;
    }

    public String getPatronymicDoctor() {
        return patronymicDoctor;
    }

    public void setPatronymicDoctor(String patronymicDoctor) {
        this.patronymicDoctor = patronymicDoctor;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DoctorAppointment that = (DoctorAppointment) o;

        return Objects.equals(date, that.date) && Objects.equals(time, that.time) &&
                Objects.equals(roomNumber, that.roomNumber) && Objects.equals(nameDoctor, that.nameDoctor) &&
                Objects.equals(secondNameDoctor, that.secondNameDoctor) &&
                Objects.equals(patronymicDoctor, that.patronymicDoctor) && Objects.equals(speciality, that.speciality);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, time, roomNumber, nameDoctor, secondNameDoctor, patronymicDoctor, speciality);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("DoctorAppointment{date=");
        sb.append(date);
        sb.append(", time=");
        sb.append(time);
        sb.append(", roomNumber=");
        sb.append(roomNumber);
        sb.append(", nameDoctor='");
        sb.append(nameDoctor);
        sb.append("', secondNameDoctor='");
        sb.append(secondNameDoctor);
        sb.append("', patronymicDoctor='");
        sb.append(patronymicDoctor);
        sb.append("', speciality='");
        sb.append(speciality);
        sb.append("'}");

        return sb.toString();
    }
}
