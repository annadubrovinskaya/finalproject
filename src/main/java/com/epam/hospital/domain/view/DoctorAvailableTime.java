package com.epam.hospital.domain.view;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Objects;

public class DoctorAvailableTime implements Serializable {
    private String speciality;
    private String name;
    private String secondName;
    private String patronymic;
    private LocalDate availableDate;
    private List<LocalTime> availableTime;

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public LocalDate getAvailableDate() {
        return availableDate;
    }

    public void setAvailableDate(LocalDate availableDate) {
        this.availableDate = availableDate;
    }

    public List<LocalTime> getAvailableTime() {
        return availableTime;
    }

    public void setAvailableTime(List<LocalTime> availableTime) {
        this.availableTime = availableTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DoctorAvailableTime that = (DoctorAvailableTime) o;

        return Objects.equals(speciality, that.speciality) && Objects.equals(name, that.name) &&
                Objects.equals(secondName, that.secondName) && Objects.equals(patronymic, that.patronymic) &&
                Objects.equals(availableDate, that.availableDate) && Objects.equals(availableTime, that.availableTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(speciality, name, secondName, patronymic, availableDate, availableTime);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("DoctorAvailableTime{speciality='");
        sb.append(speciality);
        sb.append("', name='");
        sb.append(name);
        sb.append("', secondName='");
        sb.append(secondName);
        sb.append("', patronymic='");
        sb.append(patronymic);
        sb.append("', availableDate=");
        sb.append(availableDate);
        sb.append("', availableTime='");
        sb.append(availableTime);
        sb.append("}");

        return sb.toString();
    }
}
