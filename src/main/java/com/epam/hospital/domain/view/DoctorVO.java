package com.epam.hospital.domain.view;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.Objects;

public class DoctorVO implements Serializable {
    private long id;
    private String name;
    private String secondName;
    private String patronymic;
    private String speciality;
    private LocalTime timeFrom;
    private LocalTime timeTo;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public LocalTime getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(LocalTime timeFrom) {
        this.timeFrom = timeFrom;
    }

    public LocalTime getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(LocalTime timeTo) {
        this.timeTo = timeTo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DoctorVO that = (DoctorVO) o;

        return Objects.equals(id, that.id) && Objects.equals(name, that.name) &&
                Objects.equals(secondName, that.secondName) && Objects.equals(patronymic, that.patronymic) &&
                Objects.equals(speciality, that.speciality) && Objects.equals(timeFrom, that.timeFrom) &&
                Objects.equals(timeTo, that.timeTo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, secondName, patronymic, speciality, timeFrom, timeTo);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("DoctorVO{id=");
        sb.append(id);
        sb.append(", name='");
        sb.append(name);
        sb.append("', secondName='");
        sb.append(secondName);
        sb.append("', patronymic='");
        sb.append(patronymic);
        sb.append("', speciality='");
        sb.append(speciality);
        sb.append("', timeFrom=");
        sb.append(timeFrom);
        sb.append(", timeTo=");
        sb.append(timeTo);
        sb.append("}");

        return sb.toString();
    }
}
