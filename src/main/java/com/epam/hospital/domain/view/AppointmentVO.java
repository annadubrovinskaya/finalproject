package com.epam.hospital.domain.view;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class AppointmentVO implements Serializable {
    private long id;
    private String secondNamePatient;
    private String namePatient;
    private String patronymicPatient;
    private LocalDate date;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSecondNamePatient() {
        return secondNamePatient;
    }

    public void setSecondNamePatient(String secondNamePatient) {
        this.secondNamePatient = secondNamePatient;
    }

    public String getNamePatient() {
        return namePatient;
    }

    public void setNamePatient(String namePatient) {
        this.namePatient = namePatient;
    }

    public String getPatronymicPatient() {
        return patronymicPatient;
    }

    public void setPatronymicPatient(String patronymicPatient) {
        this.patronymicPatient = patronymicPatient;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AppointmentVO that = (AppointmentVO) o;

        return Objects.equals(id, that.id) && Objects.equals(namePatient, that.namePatient) &&
                Objects.equals(secondNamePatient, that.secondNamePatient) &&
                Objects.equals(patronymicPatient, that.patronymicPatient) &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, namePatient, secondNamePatient, patronymicPatient, date);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("AppointmentVO{id=");
        sb.append(id);
        sb.append(", namePatient='");
        sb.append(namePatient);
        sb.append("', secondNamePatient='");
        sb.append(secondNamePatient);
        sb.append("', patronymicPatient='");
        sb.append(patronymicPatient);
        sb.append("', date=");
        sb.append(date);
        sb.append("}");

        return sb.toString();
    }
}
