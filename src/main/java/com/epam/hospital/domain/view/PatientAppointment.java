package com.epam.hospital.domain.view;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.Objects;

public class PatientAppointment implements Serializable {
    private LocalTime time;
    private String namePatient;
    private String secondNamePatient;
    private String patronymicPatient;

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public String getNamePatient() {
        return namePatient;
    }

    public void setNamePatient(String namePatient) {
        this.namePatient = namePatient;
    }

    public String getSecondNamePatient() {
        return secondNamePatient;
    }

    public void setSecondNamePatient(String secondNamePatient) {
        this.secondNamePatient = secondNamePatient;
    }

    public String getPatronymicPatient() {
        return patronymicPatient;
    }

    public void setPatronymicPatient(String patronymicPatient) {
        this.patronymicPatient = patronymicPatient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PatientAppointment that = (PatientAppointment) o;

        return Objects.equals(time, that.time) && Objects.equals(namePatient, that.namePatient) &&
                Objects.equals(secondNamePatient, that.secondNamePatient) &&
                Objects.equals(patronymicPatient, that.patronymicPatient);
    }

    @Override
    public int hashCode() {
        return Objects.hash(time, namePatient, secondNamePatient, patronymicPatient);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("PatientAppointment{time=");
        sb.append(time);
        sb.append(", namePatient='");
        sb.append(namePatient);
        sb.append("', secondNamePatient='");
        sb.append(secondNamePatient);
        sb.append("', patronymicPatient='");
        sb.append(patronymicPatient);
        sb.append("'}");

        return sb.toString();
    }
}
