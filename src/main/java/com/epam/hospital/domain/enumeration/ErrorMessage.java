package com.epam.hospital.domain.enumeration;

public enum ErrorMessage {
    EMPTY_FIELDS("error.empty_fields"), TICKET_NOT_EXIST("error.no_ticket"),
    USER_ALREADY_EXIST("error.user_already_exist"), NOT_VALID_USER("error.not_valid_user"),
    ERROR_DATE_INSPECTION("error.date_inspection"), SYSTEM_ERROR("error.system_error"),
    USER_NOT_FOUND("error.user_not_found"), INVALID_DATA("error.invalid_data"),
    SPECIALITY_NOT_EXIST("error.speciality_not_exist"), NO_DOC_SPECIALITY("error.no_doc_speciality"),
    PATIENT_NOT_EXIST("error.no_patient"), DOCTOR_NOT_EXIST("error.no_doctor"),
    PATIENT_ALREADY_WRITTEN("error.pat_already_written"), DIAGNOSIS_NOT_FOUND("error.diagnosis_not_found"),
    ERROR_NO_APPOINTMENTS("error.no_appointment"), NO_SCHEDULE("error.no_schedule"),
    PAT_ALREADY_EXIST("error.patient_already_exist");

    private String value;

    ErrorMessage(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
