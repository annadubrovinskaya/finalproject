package com.epam.hospital.domain.enumeration;

public enum BloodType {
    FIRST_POSITIVE("1 positive"), FIRST_NEGATIVE("1 negative"), SECOND_POSITIVE("2 positive"), SECOND_NEGATIVE("2 negative"),
    THIRD_POSITIVE("3 positive"), THIRD_NEGATIVE("3 negative"), FOURTH_POSITIVE("4 positive"), FOURTH_NEGATIVE("4 negative");

    private String value;

    BloodType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
