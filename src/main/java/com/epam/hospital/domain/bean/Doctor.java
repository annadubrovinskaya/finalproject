package com.epam.hospital.domain.bean;

import java.io.Serializable;
import java.util.Objects;

public class Doctor implements Serializable {
    private long id;
    private int roomNumber;
    private long idPerson;
    private long idSpeciality;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public long getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(long idPerson) {
        this.idPerson = idPerson;
    }

    public long getIdSpeciality() {
        return idSpeciality;
    }

    public void setIdSpeciality(long idSpeciality) {
        this.idSpeciality = idSpeciality;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Doctor that = (Doctor) o;

        return Objects.equals(id, that.id) && Objects.equals(roomNumber, that.roomNumber) &&
                Objects.equals(idPerson, that.idPerson) && Objects.equals(idSpeciality, that.idSpeciality);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, roomNumber, idPerson, idSpeciality);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Doctor{id=");
        sb.append(id);
        sb.append(", roomNumber=");
        sb.append(roomNumber);
        sb.append(", idPerson=");
        sb.append(idPerson);
        sb.append(", idSpeciality=");
        sb.append(idSpeciality);
        sb.append('}');

        return sb.toString();
    }
}
