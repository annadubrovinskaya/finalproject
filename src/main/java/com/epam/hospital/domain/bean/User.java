package com.epam.hospital.domain.bean;

import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable {
    private long id;
    private String login;
    private String password;
    private long idRole;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getIdRole() {
        return idRole;
    }

    public void setIdRole(long idRole) {
        this.idRole = idRole;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        User that = (User) o;

        return Objects.equals(id, that.id) && Objects.equals(login, that.login) &&
                Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, password);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("User{id=");
        sb.append(id);
        sb.append(", login='");
        sb.append(login);
        sb.append("', password='");
        sb.append(password);
        sb.append("', idRole=");
        sb.append(idRole);
        sb.append("}");

        return sb.toString();
    }
}
