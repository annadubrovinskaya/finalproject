package com.epam.hospital.domain.bean;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Objects;

public class Appointment implements Serializable {
    private long id;
    private LocalDate date;
    private LocalTime time;
    private long idMedicalCard;
    private long idDoctor;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public long getIdMedicalCard() {
        return idMedicalCard;
    }

    public void setIdMedicalCard(long idMedicalCard) {
        this.idMedicalCard = idMedicalCard;
    }

    public long getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(long idDoctor) {
        this.idDoctor = idDoctor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Appointment that = (Appointment) o;

        return Objects.equals(id, that.id) && Objects.equals(date, that.date) &&
                Objects.equals(time, that.time) && Objects.equals(idMedicalCard, that.idMedicalCard) &&
                Objects.equals(idDoctor, that.idDoctor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, time, idMedicalCard, idDoctor);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Appointment{id=");
        sb.append(id);
        sb.append(", date=");
        sb.append(date);
        sb.append(", time=");
        sb.append(time);
        sb.append(", idMedicalCard=");
        sb.append(idMedicalCard);
        sb.append(", idDoctor=");
        sb.append(idDoctor);
        sb.append('}');

        return sb.toString();
    }
}
