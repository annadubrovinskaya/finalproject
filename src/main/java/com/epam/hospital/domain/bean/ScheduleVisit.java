package com.epam.hospital.domain.bean;

import java.io.Serializable;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Objects;

public class ScheduleVisit implements Serializable {
    private long id;
    private DayOfWeek dayWeek;
    private LocalTime timeFrom;
    private LocalTime timeTo;
    private long idDoctor;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DayOfWeek getDayWeek() {
        return dayWeek;
    }

    public void setDayWeek(DayOfWeek dayWeek) {
        this.dayWeek = dayWeek;
    }

    public LocalTime getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(LocalTime timeFrom) {
        this.timeFrom = timeFrom;
    }

    public LocalTime getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(LocalTime timeTo) {
        this.timeTo = timeTo;
    }

    public long getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(long idDoctor) {
        this.idDoctor = idDoctor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ScheduleVisit that = (ScheduleVisit) o;

        return Objects.equals(id, that.id) && Objects.equals(dayWeek, that.dayWeek) &&
                Objects.equals(timeFrom, that.timeFrom) && Objects.equals(timeTo, that.timeTo) &&
                Objects.equals(idDoctor, that.idDoctor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dayWeek, timeFrom, timeTo, idDoctor);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("ScheduleVisit{id=");
        sb.append(id);
        sb.append(", dayWeek=");
        sb.append(dayWeek);
        sb.append(", timeFrom=");
        sb.append(timeFrom);
        sb.append(", timeTo=");
        sb.append(timeTo);
        sb.append(", idDoctor=");
        sb.append(idDoctor);
        sb.append("}");

        return sb.toString();
    }
}
