package com.epam.hospital.domain.bean;

import java.io.Serializable;
import java.util.Objects;

public class Role implements Serializable {
    private long id;
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Role that = (Role) o;

        return Objects.equals(id, that.id) && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Role{id=");
        sb.append(id);
        sb.append(", name='");
        sb.append(name);
        sb.append("'}");

        return sb.toString();
    }
}
