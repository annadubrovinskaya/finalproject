package com.epam.hospital.domain.bean;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class Inspection implements Serializable {
    private long id;
    private LocalDate date;
    private String conclusion;
    private long idDiagnosis;
    private long idMedicalCard;
    private long idDoctor;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public long getIdDiagnosis() {
        return idDiagnosis;
    }

    public void setIdDiagnosis(long idDiagnosis) {
        this.idDiagnosis = idDiagnosis;
    }

    public long getIdMedicalCard() {
        return idMedicalCard;
    }

    public void setIdMedicalCard(long idMedicalCard) {
        this.idMedicalCard = idMedicalCard;
    }

    public long getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(long idDoctor) {
        this.idDoctor = idDoctor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Inspection that = (Inspection) o;

        return Objects.equals(id, that.id) && Objects.equals(date, that.date) &&
                Objects.equals(conclusion, that.conclusion) && Objects.equals(idDiagnosis, that.idDiagnosis) &&
                Objects.equals(idMedicalCard, that.idMedicalCard) && Objects.equals(idDoctor, that.idDoctor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, conclusion, idDiagnosis, idMedicalCard, idDoctor);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Inspection{id=");
        sb.append(id);
        sb.append(", date=");
        sb.append(date);
        sb.append(", conclusion='");
        sb.append(conclusion);
        sb.append("', idDiagnosis=");
        sb.append(idDiagnosis);
        sb.append(", idMedicalCard=");
        sb.append(idMedicalCard);
        sb.append(", idDoctor=");
        sb.append(idDoctor);
        sb.append('}');

        return sb.toString();
    }
}
