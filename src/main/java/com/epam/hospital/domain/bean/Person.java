package com.epam.hospital.domain.bean;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class Person implements Serializable {
    private long id;
    private String name;
    private String secondName;
    private String patronymic;
    private LocalDate dateOfBirth;
    private String sex;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Person that = (Person) o;

        return Objects.equals(id, that.id) && Objects.equals(name, that.name) &&
                Objects.equals(secondName, that.secondName) && Objects.equals(patronymic, that.patronymic) &&
                Objects.equals(dateOfBirth, that.dateOfBirth) && Objects.equals(sex, that.sex);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, secondName, patronymic, dateOfBirth, sex);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Person{id=");
        sb.append(id);
        sb.append(", name='");
        sb.append(name);
        sb.append("', secondName='");
        sb.append(secondName);
        sb.append("', patronymic='");
        sb.append(patronymic);
        sb.append("', dateOfBirth=");
        sb.append(dateOfBirth);
        sb.append(", sex='");
        sb.append(sex);
        sb.append("'}");

        return sb.toString();
    }
}
