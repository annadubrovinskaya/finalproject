package com.epam.hospital.domain.bean;

import java.io.Serializable;
import java.util.Objects;

public class MedicalCard implements Serializable {
    private long id;
    private float heightPatient;
    private float weightPatient;
    private String bloodType;
    private long idPerson;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getHeightPatient() {
        return heightPatient;
    }

    public void setHeightPatient(float heightPatient) {
        this.heightPatient = heightPatient;
    }

    public float getWeightPatient() {
        return weightPatient;
    }

    public void setWeightPatient(float weightPatient) {
        this.weightPatient = weightPatient;
    }

    public String getBloodType() {
        return bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public long getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(long idPerson) {
        this.idPerson = idPerson;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MedicalCard that = (MedicalCard) o;

        return Objects.equals(id, that.id) && Objects.equals(heightPatient, that.heightPatient) &&
                Objects.equals(weightPatient, that.weightPatient) && Objects.equals(bloodType, that.bloodType) &&
                Objects.equals(idPerson, that.idPerson);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, heightPatient, weightPatient, bloodType, idPerson);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("MedicalCard{id=");
        sb.append(id);
        sb.append(", heightPatient=");
        sb.append(heightPatient);
        sb.append(", weightPatient=");
        sb.append(weightPatient);
        sb.append(", bloodType='");
        sb.append(bloodType);
        sb.append("', idPerson=");
        sb.append(idPerson);
        sb.append("}");

        return sb.toString();
    }
}
