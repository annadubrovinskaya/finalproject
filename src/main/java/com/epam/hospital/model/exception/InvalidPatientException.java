package com.epam.hospital.model.exception;

public class InvalidPatientException extends InvalidDataException {
    private static final long serialVersionUID = 1L;

    public InvalidPatientException(String message) {
        super(message);
    }

    public InvalidPatientException(String message, Exception e) {
        super(message, e);
    }
}
