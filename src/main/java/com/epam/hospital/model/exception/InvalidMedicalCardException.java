package com.epam.hospital.model.exception;

public class InvalidMedicalCardException extends InvalidDataException {
    private static final long serialVersionUID = 1L;

    public InvalidMedicalCardException(String message) {
        super(message);
    }

    public InvalidMedicalCardException(String message, Exception e) {
        super(message, e);
    }
}
