package com.epam.hospital.model.exception;


public class InvalidUserException extends InvalidDataException {
    private static final long serialVersionUID = 1L;

    public InvalidUserException(String message) {
        super(message);
    }

    public InvalidUserException(String message, Exception e) {
        super(message, e);
    }
}
