package com.epam.hospital.model.exception;

public class EntityExistException extends ServiceException {
    private static final long serialVersionUID = 1L;

    public EntityExistException(String message) {
        super(message);
    }

    public EntityExistException(String message, Exception e) {
        super(message, e);
    }
}
