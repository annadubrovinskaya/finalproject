package com.epam.hospital.model.exception;

public class InvalidDoctorException extends InvalidDataException {
    private static final long serialVersionUID = 1L;

    public InvalidDoctorException(String message) {
        super(message);
    }

    public InvalidDoctorException(String message, Exception e) {
        super(message, e);
    }
}
