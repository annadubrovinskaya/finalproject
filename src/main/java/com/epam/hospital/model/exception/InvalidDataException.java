package com.epam.hospital.model.exception;

public class InvalidDataException extends ServiceException {
    private static final long serialVersionUID = 1L;

    public InvalidDataException(String message) {
        super(message);
    }

    public InvalidDataException(String message, Exception e) {
        super(message, e);
    }
}
