package com.epam.hospital.model.exception;

public class InvalidPersonalDataException extends InvalidDataException {
    private static final long serialVersionUID = 1L;

    public InvalidPersonalDataException(String message) {
        super(message);
    }

    public InvalidPersonalDataException(String message, Exception e) {
        super(message, e);
    }
}
