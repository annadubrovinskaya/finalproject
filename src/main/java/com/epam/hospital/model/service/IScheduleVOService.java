package com.epam.hospital.model.service;

import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.bean.ScheduleVisit;
import com.epam.hospital.model.exception.ServiceException;

import java.util.Map;

public interface IScheduleVOService {
    Map<String, ScheduleVisit> getSchedule(Person person, String specialityName) throws ServiceException;
    void createSchedule(Person person, String specialityName, String[] daysWeek) throws ServiceException;
    void updateSchedule(Person person, String specialityName, String[] daysWeek) throws ServiceException;
}
