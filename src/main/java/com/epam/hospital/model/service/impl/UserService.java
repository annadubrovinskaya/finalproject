package com.epam.hospital.model.service.impl;

import com.epam.hospital.dao.DaoRole;
import com.epam.hospital.dao.DaoUser;
import com.epam.hospital.dao.SqlDaoFactory;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.User;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.model.exception.EntityExistException;
import com.epam.hospital.model.exception.InvalidDataException;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IUserService;
import com.epam.hospital.model.validation.Validation;
import com.epam.hospital.model.validation.impl.UserValidation;
import org.apache.commons.codec.digest.DigestUtils;

public class UserService implements IUserService {
    private static final String EMPTY_STRING = "";
    private static final long DEFAULT_ROLE_INDEX = 2;
    private static final long DEFAULT_INT_VALUE = 0;

    private static final DaoUser daoUser = SqlDaoFactory.getInstance().getDaoUser();
    private static final DaoRole daoRole = SqlDaoFactory.getInstance().getDaoRole();
    private static final Validation<User> userValidation = UserValidation.getInstance();

    private UserService(){}

    private static class SingletonHolder {
        private static final UserService INSTANCE = new UserService();
    }

    public static UserService getInstance() {
        return  SingletonHolder.INSTANCE;
    }

    @Override
    public void checkRegistrationUser(User user) throws ServiceException {
        if (user.getLogin() == null || user.getLogin().equals(EMPTY_STRING) || user.getPassword() == null ||
                user.getPassword().equals(EMPTY_STRING)) {
            throw new InvalidDataException(ErrorMessage.EMPTY_FIELDS.getValue());
        }

        if (!userValidation.validate(user)) {
            throw new InvalidDataException(ErrorMessage.NOT_VALID_USER.getValue());
        }

        String encodedPassword = DigestUtils.md5Hex(user.getPassword());
        user.setPassword(encodedPassword);

        User findingUser = findUser(user.getLogin(), user.getPassword());
        long idRole = findingUser.getIdRole();

        if (idRole != DEFAULT_INT_VALUE) {
            throw new EntityExistException(ErrorMessage.USER_ALREADY_EXIST.getValue());
        } else {
            user.setIdRole(DEFAULT_ROLE_INDEX);
        }
    }

    @Override
    public User findUser(String login, String password) throws ServiceException {
        try {
            return daoUser.findUser(login, password);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public long insert(User user) throws ServiceException {
        try {
            checkRegistrationUser(user);

            return daoUser.insert(user);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public String checkAuthorization(User user) throws ServiceException {
        try {
            String encodedPassword = DigestUtils.md5Hex(user.getPassword());

            user.setPassword(encodedPassword);
            checkUser(user);

            return daoRole.getRoleName(user.getIdRole());
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    private void checkUser(User user) throws ServiceException {
        if (user.getLogin() == null || user.getLogin().equals(EMPTY_STRING) || user.getPassword() == null ||
                user.getPassword().equals(EMPTY_STRING)) {
            throw new InvalidDataException(ErrorMessage.EMPTY_FIELDS.getValue());
        }

        User findingUser = findUser(user.getLogin(), user.getPassword());
        long idRole = findingUser.getIdRole();

        if (idRole == 0) {
            throw new EntityExistException(ErrorMessage.USER_NOT_FOUND.getValue());
        } else {
            user.setId(findingUser.getId());
            user.setIdRole(idRole);
        }
    }
}