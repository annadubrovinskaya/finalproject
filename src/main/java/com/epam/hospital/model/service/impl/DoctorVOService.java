package com.epam.hospital.model.service.impl;

import com.epam.hospital.domain.bean.Doctor;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.bean.Speciality;
import com.epam.hospital.domain.bean.User;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.model.exception.EntityExistException;
import com.epam.hospital.model.exception.InvalidDataException;
import com.epam.hospital.model.exception.InvalidDoctorException;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.*;
import com.epam.hospital.model.validation.Validation;
import com.epam.hospital.model.validation.impl.DoctorValidation;
import com.epam.hospital.model.validation.impl.PersonValidation;

import java.time.LocalDate;

public class DoctorVOService implements IDoctorVOService {
    private static final int INT_DEFAULT_VALUE = 0;
    private static final int ADDED_MAX_YEARS = 70;
    private static final int ADDED_MIN_YEARS = 25;

    private static final Validation<Doctor> doctorValidation = DoctorValidation.getInstance();
    private static final Validation<Person> personValidation = PersonValidation.getInstance();
    private static final IPersonService personService = PersonService.getInstance();
    private static final ISpecialityService specialityService = SpecialityService.getInstance();
    private static final IUserService userService = UserService.getInstance();
    private static final IDoctorService doctorService = DoctorService.getInstance();

    private DoctorVOService() {
    }

    private static class SingletonHolder {
        private static final DoctorVOService INSTANCE = new DoctorVOService();
    }

    public static DoctorVOService getInstance() {
        return DoctorVOService.SingletonHolder.INSTANCE;
    }

    @Override
    public long getDoctorId(Person person, String specialityName) throws ServiceException {
        personService.checkFullName(person);

        long idDoctor = doctorService.getDoctorId(person, specialityName);

        if (idDoctor == INT_DEFAULT_VALUE) {
            throw new EntityExistException(ErrorMessage.DOCTOR_NOT_EXIST.getValue());
        }

        return idDoctor;
    }

    @Override
    public long checkInDoctor(Person person, User user, Doctor doctor, Speciality speciality) throws ServiceException {
        checkDoctor(doctor, person);
        specialityService.checkSpeciality(speciality);

        long idUser = userService.insert(user);
        long idPerson = personService.insert(person);
        long idSpeciality = specialityService.getIdSpeciality(speciality);

        doctor.setId(idUser);
        doctor.setIdPerson(idPerson);
        doctor.setIdSpeciality(idSpeciality);

        doctorService.insert(doctor);

        return idUser;
    }

    private void checkDoctor(Doctor doctor, Person person) throws InvalidDataException {
        LocalDate date = person.getDateOfBirth();
        LocalDate nowDate = LocalDate.now();
        LocalDate minDate = nowDate.minusYears(ADDED_MAX_YEARS);
        LocalDate maxDate = nowDate.minusYears(ADDED_MIN_YEARS);

        if (!(date.equals(maxDate) || date.equals(minDate) || date.isAfter(minDate) &&
                date.isBefore(maxDate))) {
            throw new InvalidDataException(ErrorMessage.INVALID_DATA.getValue());
        }

        if (!doctorValidation.validate(doctor) && !personValidation.validate(person)) {
            throw new InvalidDoctorException(ErrorMessage.INVALID_DATA.getValue());
        }
    }
}
