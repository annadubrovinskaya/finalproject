package com.epam.hospital.model.service;

import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.model.exception.ServiceException;

public interface IPatientService {
    void checkPatient(Person person) throws ServiceException;
    void checkRegistrationDataPatient(Person person) throws ServiceException;
    void delete(Person person) throws ServiceException;
}
