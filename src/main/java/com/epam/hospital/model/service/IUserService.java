package com.epam.hospital.model.service;

import com.epam.hospital.domain.bean.User;
import com.epam.hospital.model.exception.ServiceException;

public interface IUserService {
    void checkRegistrationUser(User user) throws ServiceException;
    User findUser(String login, String password) throws ServiceException;
    long insert(User user) throws ServiceException;
    String checkAuthorization(User user) throws ServiceException;
}
