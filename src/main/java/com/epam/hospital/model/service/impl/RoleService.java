package com.epam.hospital.model.service.impl;

import com.epam.hospital.dao.DaoRole;
import com.epam.hospital.dao.SqlDaoFactory;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IRoleService;

public class RoleService implements IRoleService {
    private static final DaoRole sqlDaoRole = SqlDaoFactory.getInstance().getDaoRole();

    private RoleService(){}

    private static class SingletonHolder {
        private static final RoleService INSTANCE = new RoleService();
    }

    public static RoleService getInstance() {
        return  RoleService.SingletonHolder.INSTANCE;
    }

    @Override
    public String getRoleName(long idRole) throws ServiceException {
        try {
            return sqlDaoRole.getRoleName(idRole);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }
}