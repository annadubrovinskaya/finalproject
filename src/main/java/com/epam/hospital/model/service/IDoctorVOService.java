package com.epam.hospital.model.service;

import com.epam.hospital.domain.bean.Doctor;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.bean.Speciality;
import com.epam.hospital.domain.bean.User;
import com.epam.hospital.model.exception.ServiceException;

public interface IDoctorVOService {
    long getDoctorId(Person person, String specialityName) throws ServiceException;
    long checkInDoctor(Person person, User user, Doctor doctor, Speciality speciality) throws ServiceException;
}
