package com.epam.hospital.model.service;

import com.epam.hospital.domain.bean.Diagnosis;
import com.epam.hospital.model.exception.ServiceException;

import java.util.List;

public interface IDiagnosisService {
    List<Diagnosis> getDiagnosis() throws ServiceException;
    long getIdDiagnosis(String diagnosisName) throws ServiceException;
}
