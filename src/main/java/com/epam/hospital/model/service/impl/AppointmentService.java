package com.epam.hospital.model.service.impl;

import com.epam.hospital.dao.DaoAppointment;
import com.epam.hospital.dao.SqlDaoFactory;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.*;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.domain.view.*;
import com.epam.hospital.model.exception.EntityExistException;
import com.epam.hospital.model.exception.InvalidDataException;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.*;

import java.time.LocalDate;
import java.util.List;

public class AppointmentService implements IAppointmentService {
    private static final int MAX_ADDED_DAYS = 20;
    private static final int DEFAULT_INT_VALUE = 0;

    private static final DaoAppointment sqlDaoAppointment = SqlDaoFactory.getInstance().getDaoAppointment();

    private AppointmentService() {
    }

    private static class SingletonHolder {
        private static final AppointmentService INSTANCE = new AppointmentService();
    }

    public static AppointmentService getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @Override
    public List<Appointment> getAppointment(LocalDate date, long idDoctor) throws ServiceException {
        try {
            return sqlDaoAppointment.getAppointments(date, idDoctor);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public long insert(Appointment appointment) throws ServiceException {
        try {
            return sqlDaoAppointment.insert(appointment);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public void deleteAppointment(long idAppointment) throws ServiceException {
        try {
            checkBookingAppointment(idAppointment);
            sqlDaoAppointment.deleteAppointment(idAppointment);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public AppointmentVO getAppointmentVO(long idAppointment) throws ServiceException {
        try {
            return sqlDaoAppointment.getAppointmentVO(idAppointment);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public List<PatientAppointment> getPatientAppointments(long idDoctor, LocalDate date) throws ServiceException {
        try {
            checkAppointmentDate(date);
            List<PatientAppointment> patientAppointments = sqlDaoAppointment.getPatientAppointments(idDoctor, date);

            if (patientAppointments.size() == DEFAULT_INT_VALUE) {
                throw new EntityExistException(ErrorMessage.ERROR_NO_APPOINTMENTS.getValue());
            }

            return patientAppointments;
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public List<DoctorAppointment> getDoctorAppointments(long idMedicalCard) throws ServiceException {
        try {
            return sqlDaoAppointment.getDoctorAppointments(idMedicalCard);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public void deleteExpiredTicket() throws ServiceException {
        try {
            sqlDaoAppointment.deleteExpiredTicket();
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public Appointment getAppointment(long idAppointment) throws ServiceException {
        try {
            checkBookingAppointment(idAppointment);

            Appointment appointment = sqlDaoAppointment.getAppointment(idAppointment);

            if (!appointment.getDate().equals(LocalDate.now())) {
                throw new InvalidDataException(ErrorMessage.ERROR_DATE_INSPECTION.getValue());
            }

            return appointment;

        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public void checkWrittenReceipt(Appointment appointment) throws ServiceException {
        try {
            if (sqlDaoAppointment.isWrittenReceipt(appointment)) {
                throw new EntityExistException(ErrorMessage.PATIENT_ALREADY_WRITTEN.getValue());
            }
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public void checkAppointmentDate(LocalDate requiredDate) throws InvalidDataException {
        LocalDate minDate = LocalDate.now();
        LocalDate maxDate = minDate.plusDays(MAX_ADDED_DAYS);

        if (!(requiredDate.equals(maxDate) || requiredDate.equals(minDate) || requiredDate.isAfter(minDate) &&
                requiredDate.isBefore(maxDate))) {
            throw new InvalidDataException(ErrorMessage.INVALID_DATA.getValue());
        }
    }

    private void checkBookingAppointment(long idAppointment) throws ServiceException {
        try {
            if (!sqlDaoAppointment.isBookingAppointment(idAppointment)) {
                throw new EntityExistException(ErrorMessage.TICKET_NOT_EXIST.getValue());
            }
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }
}