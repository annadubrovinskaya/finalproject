package com.epam.hospital.model.service.impl;

import com.epam.hospital.dao.DaoPerson;
import com.epam.hospital.dao.SqlDaoFactory;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.model.exception.InvalidDataException;
import com.epam.hospital.model.exception.InvalidPersonalDataException;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IPersonService;
import com.epam.hospital.model.validation.IPersonValidation;
import com.epam.hospital.model.validation.impl.PersonValidation;

public class PersonService implements IPersonService {
    private static final String EMPTY_STRING = "";

    private static final DaoPerson sqlDaoPerson = SqlDaoFactory.getInstance().getDaoPerson();
    private static final IPersonValidation personValidation = PersonValidation.getInstance();

    private PersonService(){}

    private static class SingletonHolder {
        private static final PersonService INSTANCE = new PersonService();
    }

    public static PersonService getInstance() {
        return  PersonService.SingletonHolder.INSTANCE;
    }

    @Override
    public void checkFullName(Person person) throws InvalidDataException {
        if (person.getName() == null || person.getName().equals(EMPTY_STRING) ||
                person.getSecondName() == null || person.getSecondName().equals(EMPTY_STRING) ||
                person.getPatronymic() == null || person.getPatronymic().equals(EMPTY_STRING)) {
            throw new InvalidDataException(ErrorMessage.EMPTY_FIELDS.getValue());
        }

        if (!personValidation.validFullName(person)) {
            throw new InvalidPersonalDataException(ErrorMessage.INVALID_DATA.getValue());
        }
    }

    @Override
    public void checkPerson(Person person) throws InvalidDataException {
        if (person.getName() == null || person.getName().equals(EMPTY_STRING) ||
                person.getSecondName() == null || person.getSecondName().equals(EMPTY_STRING) ||
                person.getPatronymic() == null || person.getSecondName().equals(EMPTY_STRING) ||
                person.getDateOfBirth() == null ||
                person.getSex() == null || person.getSex().equals(EMPTY_STRING)) {
            throw new InvalidDataException(ErrorMessage.EMPTY_FIELDS.getValue());
        }

        if (!personValidation.validate(person)) {
            throw new InvalidPersonalDataException(ErrorMessage.INVALID_DATA.getValue());
        }
    }

    @Override
    public long insert(Person person) throws ServiceException {
        try {
            return sqlDaoPerson.insert(person);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public void delete(Person person) throws ServiceException {
        try {
            sqlDaoPerson.delete(person);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public long getIdPerson(Person person) throws ServiceException {
        try {
            return sqlDaoPerson.getIdPerson(person);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }
}