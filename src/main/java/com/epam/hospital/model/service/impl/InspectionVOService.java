package com.epam.hospital.model.service.impl;

import com.epam.hospital.domain.bean.Appointment;
import com.epam.hospital.domain.bean.Inspection;
import com.epam.hospital.domain.bean.MedicalCard;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.domain.view.InspectionVO;
import com.epam.hospital.model.exception.InvalidDataException;
import com.epam.hospital.model.exception.InvalidMedicalCardException;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.*;
import com.epam.hospital.model.validation.IMedicalCardValidation;
import com.epam.hospital.model.validation.Validation;
import com.epam.hospital.model.validation.impl.InspectionValidation;
import com.epam.hospital.model.validation.impl.MedicalCardValidation;

import java.time.LocalDate;

public class InspectionVOService implements IInspectionVOService {
    private static final String EMPTY_STRING = "";
    private static final String THERAPIST = "therapist";

    private static final Validation<Inspection> inspectionValidation = InspectionValidation.getInstance();
    private static final IMedicalCardValidation medicalCardValidation = MedicalCardValidation.getInstance();
    private static final IAppointmentService appointmentService = AppointmentService.getInstance();
    private static final IDiagnosisService diagnosisService = DiagnosisService.getInstance();
    private static final IMedicalCardService medicalCardService = MedicalCardService.getInstance();
    private static final IInspectionService inspectionService = InspectionService.getInstance();

    private InspectionVOService(){}

    private static class SingletonHolder {
        private static final InspectionVOService INSTANCE = new InspectionVOService();
    }

    public static InspectionVOService getInstance() {
        return  InspectionVOService.SingletonHolder.INSTANCE;
    }

    @Override
    public void saveInspection(InspectionVO inspectionVO, MedicalCard medicalCard, long idAppointment) throws ServiceException {
        Appointment appointment = appointmentService.getAppointment(idAppointment);
        Inspection inspection = new Inspection();

        inspection.setDate(inspectionVO.getDate());
        inspection.setConclusion(inspectionVO.getConclusion());
        inspection.setIdDoctor(appointment.getIdDoctor());
        inspection.setIdMedicalCard(appointment.getIdMedicalCard());

        long idDiagnosis = diagnosisService.getIdDiagnosis(inspectionVO.getDiagnosis());

        inspection.setIdDiagnosis(idDiagnosis);
        checkInspection(inspection);

        if (inspectionVO.getSpeciality().equals(THERAPIST)) {
            if (!medicalCardValidation.validateHeightWeight(medicalCard)) {
                throw new InvalidMedicalCardException(ErrorMessage.INVALID_DATA.getValue());
            }

            long idMedicalCard = appointment.getIdMedicalCard();
            medicalCard.setId(idMedicalCard);
            medicalCardService.updateCard(medicalCard);
        }

        inspectionService.insert(inspection);
        appointmentService.deleteAppointment(idAppointment);
    }

    private void checkInspection(Inspection inspection) throws InvalidDataException {
        if (inspection.getConclusion() == null || inspection.getConclusion().equals(EMPTY_STRING)) {
            throw new InvalidDataException(ErrorMessage.EMPTY_FIELDS.getValue());
        }

        if (!inspectionValidation.validate(inspection)) {
            throw new InvalidDataException(ErrorMessage.INVALID_DATA.getValue());
        }

        LocalDate date = LocalDate.now();
        if (!inspection.getDate().equals(date)) {
            throw new InvalidDataException(ErrorMessage.ERROR_DATE_INSPECTION.getValue());
        }
    }
}