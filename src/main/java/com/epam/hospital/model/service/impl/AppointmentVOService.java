package com.epam.hospital.model.service.impl;

import com.epam.hospital.domain.bean.Appointment;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.bean.ScheduleVisit;
import com.epam.hospital.domain.bean.Speciality;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.domain.view.DoctorAppointment;
import com.epam.hospital.domain.view.DoctorAvailableTime;
import com.epam.hospital.domain.view.DoctorVO;
import com.epam.hospital.model.exception.EntityExistException;
import com.epam.hospital.model.exception.InvalidDataException;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.*;
import com.epam.hospital.model.util.AppointmentManager;
import com.epam.hospital.model.util.AvailableTimeComparator;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AppointmentVOService implements IAppointmentVOService {
    private static final String EMPTY_STRING = "";
    private static final int DEFAULT_INT_VALUE = 0;
    private static final int ONE_DAY = 1;

    private static final IPersonService personService = PersonService.getInstance();
    private static final IPatientService patientService = PatientService.getInstance();
    private static final IMedicalCardService medicalCardService = MedicalCardService.getInstance();
    private static final IAppointmentService appointmentService = AppointmentService.getInstance();
    private static final ISpecialityService specialityService = SpecialityService.getInstance();
    private static final IDoctorService doctorService = DoctorService.getInstance();
    private static final IScheduleService scheduleService = ScheduleService.getInstance();

    private AppointmentVOService() {
    }

    private static class SingletonHolder {
        private static final AppointmentVOService INSTANCE = new AppointmentVOService();
    }

    public static AppointmentVOService getInstance() {
        return AppointmentVOService.SingletonHolder.INSTANCE;
    }

    @Override
    public List<DoctorAppointment> getDoctorAppointments(Person person) throws ServiceException {
        patientService.checkPatient(person);

        long idPerson = personService.getIdPerson(person);
        long idMedicalCard = medicalCardService.getIdMedicalCard(idPerson);
        List<DoctorAppointment> appointmentList = appointmentService.getDoctorAppointments(idMedicalCard);

        if (appointmentList.size() == DEFAULT_INT_VALUE) {
            throw new EntityExistException(ErrorMessage.ERROR_NO_APPOINTMENTS.getValue());
        }

        return appointmentList;
    }

    @Override
    public DoctorAvailableTime getAvailableTime(DoctorAppointment appointment) throws ServiceException {
        List<DoctorAvailableTime> availableTimes = new ArrayList<>();

        checkDoctorAppointment(appointment);

        LocalDate date = appointment.getDate();
        Speciality speciality = new Speciality();
        speciality.setName(appointment.getSpeciality());

        for (int day = ONE_DAY; availableTimes.size() == DEFAULT_INT_VALUE; date = date.plusDays(day)) {
            DayOfWeek dayOfWeek = date.getDayOfWeek();
            List<DoctorVO> doctorsVO = doctorService.getDoctorsVO(speciality, dayOfWeek);

            if (doctorsVO.size() != DEFAULT_INT_VALUE) {
                passOnDoctors(doctorsVO, date, speciality.getName(), availableTimes);
            }
        }

        Collections.sort(availableTimes, new AvailableTimeComparator());
        Collections.reverse(availableTimes);

        return availableTimes.get(DEFAULT_INT_VALUE);
    }

    @Override
    public DoctorAvailableTime getAvailableTime(LocalDate date, long idDoctor) throws ServiceException {
        List<DoctorAvailableTime> availableTimes = new ArrayList<>();

        appointmentService.checkAppointmentDate(date);

        List<ScheduleVisit> listSchedule = scheduleService.getListSchedule(idDoctor);
        if (listSchedule.size() == 0) {
            throw new EntityExistException(ErrorMessage.NO_SCHEDULE.getValue());
        }

        for (int day = ONE_DAY; availableTimes.size() == 0; date = date.plusDays(day)) {
            DayOfWeek dayOfWeek = date.getDayOfWeek();
            DoctorVO doctorVO = doctorService.getDoctorVO(idDoctor, dayOfWeek);

            if (doctorVO.getTimeFrom() != null && doctorVO.getTimeTo() != null) {
                String specialityName = doctorService.findDoctorSpeciality(idDoctor);
                getAvailableTime(doctorVO, date, specialityName, availableTimes);
            }
        }

        Collections.sort(availableTimes, new AvailableTimeComparator());
        Collections.reverse(availableTimes);

        return availableTimes.get(DEFAULT_INT_VALUE);
    }

    @Override
    public long makeAppointment(DoctorAppointment doctorAppointment, Person person) throws ServiceException {
        patientService.checkPatient(person);
        Person doctorPD = new Person();
        doctorPD.setName(doctorAppointment.getNameDoctor());
        doctorPD.setSecondName(doctorAppointment.getSecondNameDoctor());
        doctorPD.setPatronymic(doctorAppointment.getPatronymicDoctor());

        long idDoctor = doctorService.getDoctorId(doctorPD, doctorAppointment.getSpeciality());
        long idPerson = personService.getIdPerson(person);
        long idMedicalCard = medicalCardService.getIdMedicalCard(idPerson);
        Appointment appointment = new Appointment();

        appointment.setDate(doctorAppointment.getDate());
        appointment.setTime(doctorAppointment.getTime());
        appointment.setIdDoctor(idDoctor);
        appointment.setIdMedicalCard(idMedicalCard);
        appointmentService.checkWrittenReceipt(appointment);

        long idAppointment = appointmentService.insert(appointment);

        int roomNumber = doctorService.getRoomNumber(idDoctor);
        doctorAppointment.setRoomNumber(roomNumber);

        return idAppointment;
    }

    private void passOnDoctors(List<DoctorVO> doctorsVO, LocalDate date, String specialityName, List<DoctorAvailableTime> availableTimes) throws ServiceException {
        for (DoctorVO doctorVO : doctorsVO) {
            getAvailableTime(doctorVO, date, specialityName, availableTimes);
        }
    }

    private void getAvailableTime(DoctorVO doctorVO, LocalDate date, String specialityName, List<DoctorAvailableTime> availableTimes)
            throws ServiceException {
        LocalTime timeFrom = doctorVO.getTimeFrom();
        LocalTime timeTo = doctorVO.getTimeTo();
        List<Appointment> appointments = appointmentService.getAppointment(date, doctorVO.getId());
        List<LocalTime> availableTime = AppointmentManager.getAvailableTime(appointments, date, timeFrom, timeTo);

        if (availableTime.size() != DEFAULT_INT_VALUE) {
            DoctorAvailableTime doctorAvailableTime = new DoctorAvailableTime();
            doctorAvailableTime.setName(doctorVO.getName());
            doctorAvailableTime.setSecondName(doctorVO.getSecondName());
            doctorAvailableTime.setPatronymic(doctorVO.getPatronymic());
            doctorAvailableTime.setSpeciality(specialityName);
            doctorAvailableTime.setAvailableTime(availableTime);
            doctorAvailableTime.setAvailableDate(date);

            availableTimes.add(doctorAvailableTime);
        }
    }

    private void checkDoctorAppointment(DoctorAppointment appointment) throws ServiceException {
        if (appointment.getSpeciality() == null || appointment.getSpeciality().equals(EMPTY_STRING)) {
            throw new InvalidDataException(ErrorMessage.EMPTY_FIELDS.getValue());
        }

        appointmentService.checkAppointmentDate(appointment.getDate());

        Speciality speciality = new Speciality();
        speciality.setName(appointment.getSpeciality());

        if (specialityService.getIdSpeciality(speciality) == DEFAULT_INT_VALUE) {
            throw new EntityExistException(ErrorMessage.SPECIALITY_NOT_EXIST.getValue());
        }

        if (!doctorService.hasSpecialitySchedule(speciality.getName())) {
            throw new InvalidDataException(ErrorMessage.NO_DOC_SPECIALITY.getValue());
        }
    }
}