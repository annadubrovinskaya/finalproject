package com.epam.hospital.model.service;

import com.epam.hospital.domain.bean.Speciality;
import com.epam.hospital.model.exception.ServiceException;

import java.util.List;

public interface ISpecialityService {
    boolean checkSpeciality(Speciality speciality) throws ServiceException;
    List<Speciality> getListSpecialities() throws ServiceException;
    long getIdSpeciality(Speciality speciality) throws ServiceException;
}
