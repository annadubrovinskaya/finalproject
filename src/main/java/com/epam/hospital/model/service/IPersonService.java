package com.epam.hospital.model.service;

import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.model.exception.InvalidDataException;
import com.epam.hospital.model.exception.ServiceException;

public interface IPersonService {
    void checkFullName(Person person) throws InvalidDataException;
    void checkPerson(Person person) throws InvalidDataException;
    long insert(Person person) throws ServiceException;
    void delete(Person person) throws ServiceException;
    long getIdPerson(Person person) throws ServiceException;
}