package com.epam.hospital.model.service;

import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.view.DoctorAppointment;
import com.epam.hospital.domain.view.DoctorAvailableTime;
import com.epam.hospital.model.exception.ServiceException;

import java.time.LocalDate;
import java.util.List;

public interface IAppointmentVOService {
    List<DoctorAppointment> getDoctorAppointments(Person person) throws ServiceException;
    DoctorAvailableTime getAvailableTime(DoctorAppointment appointment) throws ServiceException;
    DoctorAvailableTime getAvailableTime(LocalDate date, long idDoctor) throws ServiceException;
    long makeAppointment(DoctorAppointment doctorAppointment, Person person) throws ServiceException;
}
