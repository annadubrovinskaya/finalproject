package com.epam.hospital.model.service;

import com.epam.hospital.domain.bean.ScheduleVisit;
import com.epam.hospital.model.exception.ServiceException;

import java.util.List;
import java.util.Map;

public interface IScheduleService {
    void insert(ScheduleVisit scheduleVisit) throws ServiceException;
    List<ScheduleVisit> getListSchedule(long idDoctor) throws ServiceException;
    Map<String, ScheduleVisit> getSchedule(long idDoctor) throws ServiceException;
    void deleteSchedule(long idDoctor) throws ServiceException;
}