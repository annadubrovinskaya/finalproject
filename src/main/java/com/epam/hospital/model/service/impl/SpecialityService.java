package com.epam.hospital.model.service.impl;

import com.epam.hospital.dao.DaoSpeciality;
import com.epam.hospital.dao.SqlDaoFactory;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.Speciality;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.model.exception.EntityExistException;
import com.epam.hospital.model.exception.InvalidDataException;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.ISpecialityService;

import java.util.List;

public class SpecialityService implements ISpecialityService{
    private static final String EMPTY_STRING = "";
    private static final int DEFAULT_INT_VALUE = 0;

    private static final DaoSpeciality sqlDaoSpeciality = SqlDaoFactory.getInstance().getDaoSpeciality();

    private SpecialityService(){}

    private static class SingletonHolder {
        private static final SpecialityService INSTANCE = new SpecialityService();
    }

    public static SpecialityService getInstance() {
        return  SingletonHolder.INSTANCE;
    }

    @Override
    public boolean checkSpeciality(Speciality speciality) throws ServiceException {
        try {
            if (speciality.getName() == null || speciality.getName().equals(EMPTY_STRING)) {
                throw new InvalidDataException(ErrorMessage.EMPTY_FIELDS.getValue());
            }

            if (sqlDaoSpeciality.getId(speciality) == DEFAULT_INT_VALUE) {
                throw new EntityExistException(ErrorMessage.SPECIALITY_NOT_EXIST.getValue());
            }
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }

        return true;
    }

    @Override
    public List<Speciality> getListSpecialities() throws ServiceException {
        try {
            return sqlDaoSpeciality.getListSpecialities();
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public long getIdSpeciality(Speciality speciality) throws ServiceException {
        try {
            return sqlDaoSpeciality.getId(speciality);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }
}