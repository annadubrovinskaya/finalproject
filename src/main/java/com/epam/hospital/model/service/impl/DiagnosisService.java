package com.epam.hospital.model.service.impl;

import com.epam.hospital.dao.DaoDiagnosis;
import com.epam.hospital.dao.SqlDaoFactory;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.Diagnosis;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.model.exception.EntityExistException;
import com.epam.hospital.model.exception.InvalidDataException;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IDiagnosisService;

import java.util.List;

public class DiagnosisService implements IDiagnosisService {
    private static final String EMPTY_STRING = "";
    private static final int DEFAULT_INT_VALUE = 0;

    private static final DaoDiagnosis sqlDaoDiagnosis = SqlDaoFactory.getInstance().getDaoDiagnosis();

    private DiagnosisService(){}

    private static class SingletonHolder {
        private static final DiagnosisService INSTANCE = new DiagnosisService();
    }

    public static DiagnosisService getInstance() {
        return  SingletonHolder.INSTANCE;
    }

    @Override
    public List<Diagnosis> getDiagnosis() throws ServiceException {
        try {
            return sqlDaoDiagnosis.getDiagnosis();
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public long getIdDiagnosis(String diagnosisName) throws ServiceException {
        try {
            if (diagnosisName == null || diagnosisName.equals(EMPTY_STRING)) {
                throw new InvalidDataException(ErrorMessage.EMPTY_FIELDS.getValue());
            }

            long idDiagnosis = sqlDaoDiagnosis.getIdDiagnosis(diagnosisName);

            if (idDiagnosis == DEFAULT_INT_VALUE) {
                throw new EntityExistException(ErrorMessage.DIAGNOSIS_NOT_FOUND.getValue());
            }

            return idDiagnosis;
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

}
