package com.epam.hospital.model.service;

import com.epam.hospital.domain.bean.Inspection;
import com.epam.hospital.domain.bean.MedicalCard;
import com.epam.hospital.domain.view.InspectionVO;
import com.epam.hospital.model.exception.InvalidDataException;
import com.epam.hospital.model.exception.ServiceException;

import java.util.List;

public interface IInspectionService {
    void insert(Inspection inspection) throws ServiceException;
    List<InspectionVO> getInspectionVO(long idMedicalCard) throws ServiceException;
}
