package com.epam.hospital.model.service.impl;

import com.epam.hospital.domain.bean.MedicalCard;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.model.exception.InvalidMedicalCardException;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IMedicalCardService;
import com.epam.hospital.model.service.IMedicalCardVOService;
import com.epam.hospital.model.service.IPatientService;
import com.epam.hospital.model.service.IPersonService;
import com.epam.hospital.model.validation.IMedicalCardValidation;
import com.epam.hospital.model.validation.impl.MedicalCardValidation;

public class MedicalCardVOService implements IMedicalCardVOService {
    private static final IMedicalCardValidation medicalCardValidation = MedicalCardValidation.getInstance();
    private static final IPersonService personService = PersonService.getInstance();
    private static final IPatientService patientService = PatientService.getInstance();
    private static final IMedicalCardService medicalCardService = MedicalCardService.getInstance();

    private MedicalCardVOService(){}

    private static class SingletonHolder {
        private static final MedicalCardVOService INSTANCE = new MedicalCardVOService();
    }

    public static MedicalCardVOService getInstance() {
        return  MedicalCardVOService.SingletonHolder.INSTANCE;
    }

    @Override
    public void createCard(Person person, MedicalCard medicalCard) throws ServiceException {
        patientService.checkRegistrationDataPatient(person);

        if (!medicalCardValidation.validate(medicalCard)) {
            throw new InvalidMedicalCardException(ErrorMessage.INVALID_DATA.getValue());
        }

        long idPerson = personService.insert(person);

        medicalCard.setIdPerson(idPerson);
        medicalCardService.insert(medicalCard);
    }

    @Override
    public long getIdMedicalCard(Person person) throws ServiceException {
        patientService.checkPatient(person);
        long idPerson = personService.getIdPerson(person);

        return medicalCardService.getIdMedicalCard(idPerson);
    }
}
