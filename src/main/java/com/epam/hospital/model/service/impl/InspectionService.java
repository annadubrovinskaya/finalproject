package com.epam.hospital.model.service.impl;

import com.epam.hospital.dao.DaoInspection;
import com.epam.hospital.dao.SqlDaoFactory;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.domain.view.InspectionVO;
import com.epam.hospital.domain.bean.Inspection;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IInspectionService;

import java.util.List;

public class InspectionService implements IInspectionService {
    private static final DaoInspection sqlDaoInspection = SqlDaoFactory.getInstance().getDaoInspection();

    private InspectionService(){}

    private static class SingletonHolder {
        private static final InspectionService INSTANCE = new InspectionService();
    }

    public static InspectionService getInstance() {
        return  SingletonHolder.INSTANCE;
    }

    @Override
    public void insert(Inspection inspection) throws ServiceException {
        try {
            sqlDaoInspection.insert(inspection);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public List<InspectionVO> getInspectionVO(long idMedicalCard) throws ServiceException {
        try {
            return sqlDaoInspection.getInspectionVO(idMedicalCard);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }
}