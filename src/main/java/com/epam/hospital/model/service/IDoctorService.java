package com.epam.hospital.model.service;

import com.epam.hospital.domain.bean.Doctor;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.bean.Speciality;
import com.epam.hospital.domain.bean.User;
import com.epam.hospital.domain.view.DoctorVO;
import com.epam.hospital.model.exception.ServiceException;

import java.time.DayOfWeek;
import java.util.List;

public interface IDoctorService {
    void insert(Doctor doctor) throws ServiceException;
    List<DoctorVO> getDoctorsWithoutSchedule() throws ServiceException;
    long getDoctorId(Person person, String specialityName) throws ServiceException;
    List<DoctorVO> getDoctorsVO(Speciality speciality, DayOfWeek dayOfWeek) throws ServiceException;
    boolean hasSpecialitySchedule(String specialityName) throws ServiceException;
    int getRoomNumber(long idDoctor) throws ServiceException;
    String findDoctorSpeciality(long idDoctor) throws ServiceException;
    DoctorVO getDoctorVO(long idDoctor, DayOfWeek dayOfWeek) throws ServiceException;
}
