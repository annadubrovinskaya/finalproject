package com.epam.hospital.model.service.impl;

import com.epam.hospital.dao.DaoSchedule;
import com.epam.hospital.dao.SqlDaoFactory;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.ScheduleVisit;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IScheduleService;

import java.time.DayOfWeek;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ScheduleService implements IScheduleService {
    private DaoSchedule sqlDaoSchedule = SqlDaoFactory.getInstance().getDaoSchedule();

    private ScheduleService(){}

    private static class SingletonHolder {
        private static final ScheduleService INSTANCE = new ScheduleService();
    }

    public static ScheduleService getInstance() {
        return  SingletonHolder.INSTANCE;
    }

    @Override
    public void insert(ScheduleVisit scheduleVisit) throws ServiceException {
        try {
            sqlDaoSchedule.insert(scheduleVisit);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public List<ScheduleVisit> getListSchedule(long idDoctor) throws ServiceException {
        try {
            return sqlDaoSchedule.getSchedule(idDoctor);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public Map<String, ScheduleVisit> getSchedule(long idDoctor) throws ServiceException {
        try {
            List<ScheduleVisit> scheduleList = getListSchedule(idDoctor);

            Map<String, ScheduleVisit> schedule = new LinkedHashMap<>();

            for (DayOfWeek dayOfWeek : DayOfWeek.values()) {
                schedule.put(dayOfWeek.toString().toLowerCase(), null);
            }

            for (ScheduleVisit scheduleVisit : scheduleList) {
                schedule.put(scheduleVisit.getDayWeek().toString().toLowerCase(), scheduleVisit);
            }

            return schedule;
        } catch (ServiceException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public void deleteSchedule(long idDoctor) throws ServiceException {
        try {
            sqlDaoSchedule.deleteSchedule(idDoctor);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }
}