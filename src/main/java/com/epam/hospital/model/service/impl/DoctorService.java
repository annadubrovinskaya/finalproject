package com.epam.hospital.model.service.impl;

import com.epam.hospital.dao.DaoDoctor;
import com.epam.hospital.dao.SqlDaoFactory;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.domain.view.DoctorVO;
import com.epam.hospital.domain.bean.Doctor;
import com.epam.hospital.domain.bean.Speciality;
import com.epam.hospital.model.exception.EntityExistException;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IDoctorService;

import java.time.DayOfWeek;
import java.util.List;

public class DoctorService implements IDoctorService {
    private static final int INT_DEFAULT_VALUE = 0;

    private static final DaoDoctor sqlDaoDoctor = SqlDaoFactory.getInstance().getDaoDoctor();


    private DoctorService() {
    }

    private static class SingletonHolder {
        private static final DoctorService INSTANCE = new DoctorService();
    }

    public static DoctorService getInstance() {
        return SingletonHolder.INSTANCE;
    }

    @Override
    public void insert(Doctor doctor) throws ServiceException {
        try {
            sqlDaoDoctor.insert(doctor);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public List<DoctorVO> getDoctorsWithoutSchedule() throws ServiceException {
        try {
            return sqlDaoDoctor.getDoctorsWithoutSchedule();
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public long getDoctorId(Person person, String specialityName) throws ServiceException {
        try {
            long idDoctor = sqlDaoDoctor.getDoctorId(person, specialityName);

            if (idDoctor == INT_DEFAULT_VALUE) {
                throw new EntityExistException(ErrorMessage.DOCTOR_NOT_EXIST.getValue());
            }

            return sqlDaoDoctor.getDoctorId(person, specialityName);

        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public List<DoctorVO> getDoctorsVO(Speciality speciality, DayOfWeek dayOfWeek) throws ServiceException {
        try {
            return sqlDaoDoctor.getDoctorsVO(speciality, dayOfWeek);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public boolean hasSpecialitySchedule(String specialityName) throws ServiceException {
        try {
            return sqlDaoDoctor.hasSpecialitySchedule(specialityName);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public int getRoomNumber(long idDoctor) throws ServiceException {
        try {
            return sqlDaoDoctor.getRoomNumber(idDoctor);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public String findDoctorSpeciality(long idDoctor) throws ServiceException {
        try {
            return sqlDaoDoctor.findDoctorSpeciality(idDoctor);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public DoctorVO getDoctorVO(long idDoctor, DayOfWeek dayOfWeek) throws ServiceException {
        try {
            return sqlDaoDoctor.getDoctorVO(idDoctor, dayOfWeek);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }
}