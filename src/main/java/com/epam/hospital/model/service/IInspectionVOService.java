package com.epam.hospital.model.service;

import com.epam.hospital.domain.bean.MedicalCard;
import com.epam.hospital.domain.view.InspectionVO;
import com.epam.hospital.model.exception.ServiceException;

public interface IInspectionVOService {
    void saveInspection(InspectionVO inspectionVO, MedicalCard medicalCard, long idAppointment) throws ServiceException;
}
