package com.epam.hospital.model.service.impl;

import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.model.exception.EntityExistException;
import com.epam.hospital.model.exception.InvalidDataException;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IPatientService;
import com.epam.hospital.model.service.IPersonService;

import java.time.LocalDate;

public class PatientService implements IPatientService {
    private static final int MAX_ADDED_YEARS = 100;
    private static final int DEFAULT_INT_VALUE = 0;

    private static final IPersonService personService = PersonService.getInstance();

    private PatientService(){}

    private static class SingletonHolder {
        private static final PatientService INSTANCE = new PatientService();
    }

    public static PatientService getInstance() {
        return  SingletonHolder.INSTANCE;
    }

    @Override
    public void checkPatient(Person person) throws ServiceException {
        personService.checkFullName(person);
        checkDateBirth(person.getDateOfBirth());

        if (personService.getIdPerson(person) == DEFAULT_INT_VALUE) {
            throw new EntityExistException(ErrorMessage.PATIENT_NOT_EXIST.getValue());
        }
    }

    @Override
    public void checkRegistrationDataPatient(Person person) throws ServiceException {
        personService.checkPerson(person);
        checkDateBirth(person.getDateOfBirth());

        if (personService.getIdPerson(person) != DEFAULT_INT_VALUE) {
            throw new EntityExistException(ErrorMessage.PAT_ALREADY_EXIST.getValue());
        }
    }

    @Override
    public void delete(Person person) throws ServiceException {
        checkPatient(person);
        personService.delete(person);
    }

    private void checkDateBirth(LocalDate date) throws InvalidDataException {
        LocalDate maxDate = LocalDate.now();
        LocalDate minDate = maxDate.minusYears(MAX_ADDED_YEARS);

        if (!(date.equals(maxDate) || date.equals(minDate) || date.isAfter(minDate) &&
                date.isBefore(maxDate))) {
            throw new InvalidDataException(ErrorMessage.INVALID_DATA.getValue());
        }
    }
}