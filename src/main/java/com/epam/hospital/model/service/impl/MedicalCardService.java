package com.epam.hospital.model.service.impl;

import com.epam.hospital.dao.DaoMedicalCard;
import com.epam.hospital.dao.SqlDaoFactory;
import com.epam.hospital.dao.exception.DaoException;
import com.epam.hospital.domain.bean.MedicalCard;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IMedicalCardService;

public class MedicalCardService implements IMedicalCardService {
    private static final DaoMedicalCard sqlDaoMedicalCard = SqlDaoFactory.getInstance().getDaoMedicalCard();

    private MedicalCardService(){}

    private static class SingletonHolder {
        private static final MedicalCardService INSTANCE = new MedicalCardService();
    }

    public static MedicalCardService getInstance() {
        return  SingletonHolder.INSTANCE;
    }

    @Override
    public void insert(MedicalCard medicalCard) throws ServiceException {
        try {
            sqlDaoMedicalCard.insert(medicalCard);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public void updateCard(MedicalCard medicalCard) throws ServiceException {
        try {
            sqlDaoMedicalCard.updateCard(medicalCard);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public MedicalCard getMedicalCard(long idPerson) throws ServiceException {
        try {
            return sqlDaoMedicalCard.getMedicalCard(idPerson);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }

    @Override
    public long getIdMedicalCard(long idPerson) throws ServiceException {
        try {
            return sqlDaoMedicalCard.getIdMedicalCard(idPerson);
        } catch (DaoException e) {
            throw new ServiceException(ErrorMessage.SYSTEM_ERROR.getValue(), e);
        }
    }
}