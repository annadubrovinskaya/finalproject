package com.epam.hospital.model.service;

import com.epam.hospital.domain.bean.MedicalCard;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.model.exception.InvalidDataException;
import com.epam.hospital.model.exception.InvalidMedicalCardException;
import com.epam.hospital.model.exception.ServiceException;

public interface IMedicalCardService {
    void insert(MedicalCard medicalCard) throws ServiceException;
    void updateCard(MedicalCard medicalCard) throws ServiceException;
    MedicalCard getMedicalCard(long idPerson) throws ServiceException;
    long getIdMedicalCard(long idPerson) throws ServiceException;
}
