package com.epam.hospital.model.service;

import com.epam.hospital.model.exception.ServiceException;

public interface IRoleService {
    String getRoleName(long idRole) throws ServiceException;
}
