package com.epam.hospital.model.service;

import com.epam.hospital.domain.bean.MedicalCard;
import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.model.exception.ServiceException;

public interface IMedicalCardVOService {
    void createCard(Person person, MedicalCard medicalCard) throws ServiceException;
    long getIdMedicalCard(Person person) throws ServiceException;
}
