package com.epam.hospital.model.service.impl;

import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.bean.ScheduleVisit;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.model.exception.InvalidDataException;
import com.epam.hospital.model.exception.ServiceException;
import com.epam.hospital.model.service.IDoctorService;
import com.epam.hospital.model.service.IScheduleService;
import com.epam.hospital.model.service.IScheduleVOService;
import com.epam.hospital.model.util.DateTimeManager;
import com.epam.hospital.model.util.ScheduleManager;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Map;

public class ScheduleVOService implements IScheduleVOService {
    private static final String EMPTY_STRING = "";
    private static final String DASH = "-";
    private static final int INDEX_TIME_FROM = 0;
    private static final int INDEX_TIME_TO = 1;

    private static final IDoctorService doctorService = DoctorService.getInstance();
    private static final IScheduleService scheduleService = ScheduleService.getInstance();

    private ScheduleVOService(){}

    private static class SingletonHolder {
        private static final ScheduleVOService INSTANCE = new ScheduleVOService();
    }

    public static ScheduleVOService getInstance() {
        return  ScheduleVOService.SingletonHolder.INSTANCE;
    }

    @Override
    public Map<String, ScheduleVisit> getSchedule(Person person, String specialityName) throws ServiceException {
        long doctorId = doctorService.getDoctorId(person, specialityName);

        return scheduleService.getSchedule(doctorId);
    }

    @Override
    public void createSchedule(Person person, String specialityName, String[] daysWeek) throws ServiceException {
        long idDoctor = doctorService.getDoctorId(person, specialityName);

        checkDayWeeks(daysWeek);
        createSchedule(idDoctor, daysWeek);
    }

    @Override
    public void updateSchedule(Person person, String specialityName, String[] daysWeek) throws ServiceException {
        long idDoctor = doctorService.getDoctorId(person, specialityName);

        checkDayWeeks(daysWeek);
        scheduleService.deleteSchedule(idDoctor);
        createSchedule(idDoctor, daysWeek);
    }

    private void createSchedule(long idDoctor, String[] daysWeek) throws ServiceException {
        ScheduleVisit scheduleVisit;
        DayOfWeek[] dayOfWeek = DayOfWeek.values();

        for (int i = 0; i < dayOfWeek.length; i++) {
            if (daysWeek[i] != null && !daysWeek[i].equals(EMPTY_STRING)) {
                scheduleVisit = ScheduleManager.getScheduleDayWeek(daysWeek[i], dayOfWeek[i]);
                scheduleVisit.setIdDoctor(idDoctor);
                scheduleService.insert(scheduleVisit);
            }
        }
    }

    private void checkDayWeeks(String[] daysWeek) throws InvalidDataException {
        int checkFlag = 0;
        for (String dayWeek : daysWeek) {
            if (dayWeek != null && !dayWeek.equals(EMPTY_STRING)) {
                checkTime(dayWeek);
                checkFlag++;
            }
        }

        if (checkFlag == 0) {
            throw new InvalidDataException(ErrorMessage.EMPTY_FIELDS.getValue());
        }
    }

    private boolean checkTime(String dayWeek) throws InvalidDataException {
        try{
            String[] splitTime = dayWeek.split(DASH);

            LocalTime timeFrom = DateTimeManager.getTime(splitTime[INDEX_TIME_FROM]);
            LocalTime timeTo = DateTimeManager.getTime(splitTime[INDEX_TIME_TO]);

            if (timeFrom.equals(timeTo) || timeFrom.isAfter(timeTo)) {
                throw new InvalidDataException(ErrorMessage.INVALID_DATA.getValue());
            }

        } catch (RuntimeException e) {
            throw new InvalidDataException(ErrorMessage.INVALID_DATA.getValue());
        }

        return true;
    }
}