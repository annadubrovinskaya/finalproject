package com.epam.hospital.model.service;

import com.epam.hospital.domain.bean.Appointment;
import com.epam.hospital.domain.view.AppointmentVO;
import com.epam.hospital.domain.view.DoctorAppointment;
import com.epam.hospital.domain.view.PatientAppointment;
import com.epam.hospital.model.exception.InvalidDataException;
import com.epam.hospital.model.exception.ServiceException;

import java.time.LocalDate;
import java.util.List;

public interface IAppointmentService {
    List<Appointment> getAppointment(LocalDate date, long idDoctor) throws ServiceException;
    long insert(Appointment appointment) throws ServiceException;
    void deleteAppointment(long idAppointment) throws ServiceException;
    AppointmentVO getAppointmentVO(long idAppointment) throws ServiceException;
    List<PatientAppointment> getPatientAppointments(long idDoctor, LocalDate date) throws ServiceException;
    List<DoctorAppointment> getDoctorAppointments(long idMedicalCard) throws ServiceException;
    void deleteExpiredTicket() throws ServiceException;
    Appointment getAppointment(long idAppointment) throws ServiceException;
    void checkWrittenReceipt(Appointment appointment) throws ServiceException;
    void checkAppointmentDate(LocalDate requiredDate) throws InvalidDataException;
}
