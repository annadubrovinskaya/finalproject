package com.epam.hospital.model.util;

public class DoctorManager {
    private static final String SPACE_SYMBOL = " ";
    private static final int SPECIALITY_NAME_INDEX = 3;

    private DoctorManager() {}

    public static String getSpeciality(String doctorData) {
        String[] masPersonalData = doctorData.split(SPACE_SYMBOL);

        return masPersonalData[SPECIALITY_NAME_INDEX];
    }
}
