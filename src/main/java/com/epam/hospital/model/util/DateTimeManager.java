package com.epam.hospital.model.util;

import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.model.exception.InvalidDataException;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateTimeManager {
    private static final String REGEX_VALID_DATE = "^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$";
    private static final String REGEX_VALID_TIME = "^[0-9]{1,2}:[0-9]{1,2}";
    private static final String DELIMITER = "-";
    private static final int YEAR_NUMBER = 0;
    private static final int MONTH_NUMBER = 1;
    private static final int DAY_NUMBER = 2;
    private static final String COLON = ":";
    private static final int HOUR_NUMBER = 0;
    private static final int MINUTE_NUMBER = 1;

    private DateTimeManager() {}

    public static LocalDate getDate(String strDate) throws InvalidDataException {
        if (validate(strDate, REGEX_VALID_DATE)) {
            String[] splitDate = strDate.split(DELIMITER);

            int year = Integer.parseInt(splitDate[YEAR_NUMBER]);
            int month = Integer.parseInt(splitDate[MONTH_NUMBER]);
            int day = Integer.parseInt(splitDate[DAY_NUMBER]);

            return LocalDate.of(year, month, day);
        } else {
            throw new InvalidDataException(ErrorMessage.INVALID_DATA.getValue());
        }
    }

    public static LocalTime getTime(String strTime) throws InvalidDataException {
        if (validate(strTime, REGEX_VALID_TIME)) {
            String[] splitTime = strTime.split(COLON);

            int hour = Integer.parseInt(splitTime[HOUR_NUMBER]);
            int minute = Integer.parseInt(splitTime[MINUTE_NUMBER]);

            return LocalTime.of(hour, minute);
        } else {
            throw new InvalidDataException(ErrorMessage.INVALID_DATA.getValue());
        }
    }

    private static boolean validate(String value, String regEx) {
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(value);

        return matcher.find();
    }
}
