package com.epam.hospital.model.util;

import com.epam.hospital.domain.bean.ScheduleVisit;
import com.epam.hospital.model.exception.InvalidDataException;

import java.time.DayOfWeek;
import java.time.LocalTime;

public class ScheduleManager {
    private static final String DASH = "-";
    private static final int INDEX_TIME_FROM = 0;
    private static final int INDEX_TIME_TO = 1;

    private ScheduleManager() {}

    public static ScheduleVisit getScheduleDayWeek(String dayWeek, DayOfWeek dayOfWeek) throws InvalidDataException {
        ScheduleVisit scheduleVisit = new ScheduleVisit();
        String[] scheduleTime = dayWeek.split(DASH);
        LocalTime time_from = DateTimeManager.getTime(scheduleTime[INDEX_TIME_FROM]);
        LocalTime time_to = DateTimeManager.getTime(scheduleTime[INDEX_TIME_TO]);

        scheduleVisit.setDayWeek(dayOfWeek);
        scheduleVisit.setTimeFrom(time_from);
        scheduleVisit.setTimeTo(time_to);

        return scheduleVisit;
    }
}
