package com.epam.hospital.model.util;

import com.epam.hospital.domain.bean.Appointment;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class AppointmentManager {
    private static final int DEFAULT_COUNT_MINUTE = 20;

    private AppointmentManager(){}

    public static List<LocalTime> getAvailableTime(List<Appointment> appointments, LocalDate date, LocalTime timeFrom, LocalTime timeTo) {
        List<LocalTime> fullTime = getFullTime(date, timeFrom, timeTo);

        if(appointments != null) {
            List<LocalTime> bookingTime = getBookingTime(appointments);
            fullTime.removeAll(bookingTime);
        }

        return fullTime;
    }

    private static List<LocalTime> getFullTime(LocalDate date, LocalTime timeFrom, LocalTime timeTo) {
        List<LocalTime> fullTime = new ArrayList<>();
        LocalDate nowDate = LocalDate.now();
        LocalTime nowTime = LocalTime.now();

        for (int minutes = DEFAULT_COUNT_MINUTE; !timeFrom.equals(timeTo); timeFrom = timeFrom.plusMinutes(minutes)) {
            if (date.equals(nowDate)) {
                if (!timeFrom.isBefore(nowTime)) {
                    fullTime.add(timeFrom);
                }
            } else {
                fullTime.add(timeFrom);
            }
        }

        return fullTime;
    }

    private static List<LocalTime> getBookingTime(List<Appointment> appointments) {
        List<LocalTime> bookingTime = new ArrayList<>();

        for (Appointment appointment: appointments) {
            bookingTime.add(appointment.getTime());
        }

        return bookingTime;
    }
}