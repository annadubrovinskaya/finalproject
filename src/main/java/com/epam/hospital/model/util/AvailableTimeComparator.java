package com.epam.hospital.model.util;

import com.epam.hospital.domain.view.DoctorAvailableTime;

import java.util.Comparator;

public class AvailableTimeComparator implements Comparator<DoctorAvailableTime> {
    @Override
    public int compare(DoctorAvailableTime o1, DoctorAvailableTime o2) {
        int size1 = o1.getAvailableTime().size();
        int size2 = o2.getAvailableTime().size();

        if(size1 == size2) {
            return 0;
        }

        if(size1 > size2) {
            return 1;
        } else {
            return -1;
        }
    }
}
