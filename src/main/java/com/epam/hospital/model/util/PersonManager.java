package com.epam.hospital.model.util;

import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.domain.enumeration.ErrorMessage;
import com.epam.hospital.model.exception.InvalidDataException;
import com.epam.hospital.model.exception.ServiceException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PersonManager {
    private static final String REGEX_VALID_PERSON = "^[a-zA-Z]{3,} [a-zA-Z]{2,} [a-zA-Z]{4,}";
    private static final String SPACE_SYMBOL = " ";
    private static final int SECOND_NAME_INDEX = 0;
    private static final int NAME_INDEX = 1;
    private static final int PATRONYMIC_INDEX = 2;

    private PersonManager() {}

    public static Person getPersonalData(String fullName) throws ServiceException {
        if (validate(fullName, REGEX_VALID_PERSON)) {
            String[] masPersonalData = fullName.split(SPACE_SYMBOL);

            String secondName = masPersonalData[SECOND_NAME_INDEX];
            String name = masPersonalData[NAME_INDEX];
            String patronymic = masPersonalData[PATRONYMIC_INDEX];

            Person person = new Person();
            person.setName(name.trim());
            person.setSecondName(secondName.trim());
            person.setPatronymic(patronymic.trim());

            return person;
        } else {
            throw new InvalidDataException(ErrorMessage.INVALID_DATA.getValue());
        }
    }

    private static boolean validate(String value, String regEx) {
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(value);

        return matcher.find();
    }
}
