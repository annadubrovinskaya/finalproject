package com.epam.hospital.model.validation.impl;

import com.epam.hospital.domain.bean.Person;
import com.epam.hospital.model.validation.IPersonValidation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PersonValidation implements IPersonValidation {
    private static final String REGEX_VALID_NAME = "^[a-zA-Z]{2,20}$";
    private static final String REGEX_VALID_SURNAME = "^[a-zA-Z-]{3,25}$";
    private static final String REGEX_VALID_PATRONYMIC = "^[a-zA-Z]{4,25}$";
    private static final String MEN = "men";
    private static final String WOMEN = "women";

    private PersonValidation(){}

    private static class SingletonHolder {
        private static final PersonValidation INSTANCE = new PersonValidation();
    }

    public static PersonValidation getInstance() {
        return  SingletonHolder.INSTANCE;
    }

    @Override
    public boolean validate(Person entity) {
        return validFullName(entity) &&
                validSex(entity);
    }

    @Override
    public boolean validFullName(Person person) {
        return validate(person.getName(), REGEX_VALID_NAME) &&
                validate(person.getSecondName(), REGEX_VALID_SURNAME) &&
                validate(person.getPatronymic(), REGEX_VALID_PATRONYMIC);
    }

    private boolean validSex(Person person) {
        String sex = person.getSex();
        return sex.equals(MEN) || sex.equals(WOMEN);
    }

    private boolean validate(String value, String regEx) {
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(value);

        return matcher.find();
    }
}
