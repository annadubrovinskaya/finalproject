package com.epam.hospital.model.validation;

import com.epam.hospital.domain.bean.MedicalCard;

public interface IMedicalCardValidation extends Validation<MedicalCard>{
    boolean validateHeightWeight(MedicalCard medicalCard);
}
