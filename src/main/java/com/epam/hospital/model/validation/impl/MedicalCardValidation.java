package com.epam.hospital.model.validation.impl;

import com.epam.hospital.domain.bean.MedicalCard;
import com.epam.hospital.domain.enumeration.BloodType;
import com.epam.hospital.model.validation.IMedicalCardValidation;

public class MedicalCardValidation implements IMedicalCardValidation {
    private static final float MIN_HEIGHT = .4f;
    private static final float MAX_HEIGHT = 2.8f;
    private static final float MIN_WEIGHT = 2.5f;
    private static final float MAX_WEIGHT = 500f;

    private MedicalCardValidation(){}

    private static class SingletonHolder {
        private static final MedicalCardValidation INSTANCE = new MedicalCardValidation();
    }

    public static MedicalCardValidation getInstance() {
        return  SingletonHolder.INSTANCE;
    }

    @Override
    public boolean validateHeightWeight(MedicalCard medicalCard) {
        float weight = medicalCard.getWeightPatient();
        float height = medicalCard.getHeightPatient();

        return weight >= MIN_WEIGHT && weight <= MAX_WEIGHT &&
                height >= MIN_HEIGHT && height <= MAX_HEIGHT;
    }

    @Override
    public boolean validate(MedicalCard entity) {
        return validateHeightWeight(entity) && validBloodType(entity.getBloodType());
    }

    private boolean validBloodType(String bloodType) {
        BloodType[] bloodTypes = BloodType.values();

        for (BloodType bloodType1 : bloodTypes) {
            if (bloodType1.getValue().equals(bloodType)) {
                return true;
            }
        }

        return false;
    }
}
