package com.epam.hospital.model.validation;

import com.epam.hospital.domain.bean.Person;

public interface IPersonValidation extends Validation<Person>{
    boolean validFullName(Person person);
}
