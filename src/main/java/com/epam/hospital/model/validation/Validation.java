package com.epam.hospital.model.validation;

public interface Validation<E> {
    boolean validate(E entity);
}
