package com.epam.hospital.model.validation.impl;

import com.epam.hospital.domain.bean.Doctor;
import com.epam.hospital.model.validation.Validation;

public class DoctorValidation implements Validation<Doctor> {
    private static final int MIN_ROOM_NUMBER = 0;
    private static final int MAX_ROOM_NUMBER = 100_000;

    private DoctorValidation(){}

    private static class SingletonHolder {
        private static final DoctorValidation INSTANCE = new DoctorValidation();
    }

    public static DoctorValidation getInstance() {
        return  SingletonHolder.INSTANCE;
    }

    @Override
    public boolean validate(Doctor entity) {
        return entity.getRoomNumber() > MIN_ROOM_NUMBER && entity.getRoomNumber()  < MAX_ROOM_NUMBER;
    }
}
