package com.epam.hospital.model.validation.impl;

import com.epam.hospital.domain.bean.Inspection;
import com.epam.hospital.model.validation.Validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InspectionValidation implements Validation<Inspection> {
    private static final String REGEX_VALID_CONCLUSION = "^[\\d\\s\\w,.]+$";

    private InspectionValidation(){}

    private static class SingletonHolder {
        private static final InspectionValidation INSTANCE = new InspectionValidation();
    }

    public static InspectionValidation getInstance() {
        return  InspectionValidation.SingletonHolder.INSTANCE;
    }

    @Override
    public boolean validate(Inspection entity) {
        Pattern pattern = Pattern.compile(REGEX_VALID_CONCLUSION);
        Matcher matcher = pattern.matcher(entity.getConclusion());

        return matcher.find();
    }
}
