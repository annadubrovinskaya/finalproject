package com.epam.hospital.model.validation.impl;

import com.epam.hospital.domain.bean.User;
import com.epam.hospital.model.validation.Validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserValidation implements Validation<User> {
    private static final String REGEX_VALID_LOGIN = "^[a-zA-Z][a-zA-Z0-9-_\\.]{1,20}$";
    private static final String REGEX_VALID_PASSWORD = "^(?=.*[a-z])(?=.*[A-Z]).*$";

    private UserValidation(){}

    private static class SingletonHolder {
        private static final UserValidation INSTANCE = new UserValidation();
    }

    public static UserValidation getInstance() {
        return  SingletonHolder.INSTANCE;
    }

    @Override
    public boolean validate(User entity) {
        return validate(entity.getLogin(), REGEX_VALID_LOGIN) &&
                validate(entity.getPassword(), REGEX_VALID_PASSWORD);
    }

    private boolean validate(String value, String regEx) {
        Pattern pattern = Pattern.compile(regEx);
        Matcher matcher = pattern.matcher(value);

        return matcher.find();
    }
}
