package com.epam.hospital.listener;

import com.epam.hospital.dao.connection.ConnectionPool;
import com.epam.hospital.dao.exception.ConnectionPoolException;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSession;

public class ApplicationListener implements ServletContextListener {
    private static final Logger logger = Logger.getLogger(ApplicationListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
            ConnectionPool.getInstance().init();
            logger.info("ConnectionPool has been initialized");
        } catch (ConnectionPoolException e) {
            logger.error("ConnectionPoolException in method contextInitialized", e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        try {
            ConnectionPool.getInstance().dispose();
            logger.info("ConnectionPool has been disposed");
        } catch (ConnectionPoolException e) {
            logger.error("ConnectionPoolException in method contextDestroyed", e);
        }
    }
}
