<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error</title>
    <link href="css/main.css" rel="stylesheet">
    <link href="css/boot.css" rel="stylesheet">
</head>
<body class="error-image">
<div class="btn-position">
    <a href="/controller?command=logout" class="btn btn-success">I want home!</a>
</div>
</body>
</html>
