<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tld/taglib.tld" prefix="jstlpg" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <core:set var="language"
              value="${not empty param.language ? param.language : not empty language ? language : 'ru'}"
              scope="session"/>
    <fmt:setLocale value="${language}"/>
    <fmt:setBundle basename="localization.hospital" var="lang"/>
    <meta charset="utf-8">
    <title>Doctor schedule visit</title>
    <!-- Le styles -->
    <link href="css/boot.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
</head>

<body>
<div class="container">
    <!-- Navbar
      ================================================== -->
    <div class="navbar">
        <div class="container">
            <div class="row">
                <h1 class="loadhome span3"><img src="images/logo.png" alt="Medical Center"/></h1>

                <div class="nav-collapse collapse"><br/>
                    <ul class="nav pull-right">
                        <li><a href="/indexUser"><fmt:message key="home" bundle="${lang}"/></a></li>
                        <li><a href="/controller?command=logout"><fmt:message key="logout" bundle="${lang}"/></a></li>
                        <li class="dropdown">
                            <a href="/schedule" class="dropdown-toggle" data-toggle="dropdown" ><fmt:message key="setting" bundle="${lang}"/></a>
                            <ul class="dropdown-menu">
                                <li class="nav-header"><fmt:message key="language" bundle="${lang}"/></li>
                                <li class="divider"></li>
                                <li><a href="/schedule?language=en_US"><fmt:message key="english"
                                                                                       bundle="${lang}"/></a>
                                </li>
                                <li><a href="/schedule?language=ru_RU"><fmt:message key="russian"
                                                                                       bundle="${lang}"/></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="label label-info" style="display:block;"></div>
    <!-- ================Carousel and pages======= -->
    <div id="authorization-body">
        <br>
        <ul class="nav nav-tabs">
            <li role="presentation">
                <a href="/indexUser" class="btnB btnB-default menu-btn"><fmt:message key="patient_inspection"
                                                                                     bundle="${lang}"/></a>
            </li>
            <li role="presentation">
                <a href="/appointment" class="btnB btnB-default menu-btn"><fmt:message key="patient_appointments"
                                                                                       bundle="${lang}"/></a>
            </li>
            <li role="presentation">
                <a href="/patientCard" class="btnB btnB-default menu-btn"><fmt:message key="patient_card"
                                                                                       bundle="${lang}"/></a>
            </li>
            <li class="dropdown">
                <a href="/indexUser" class="btnB btnB-default active-button" class="dropdown-toggle"
                   data-toggle="dropdown"><fmt:message key="data_view" bundle="${lang}"/></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="/dvDoctor"><fmt:message key="patients" bundle="${lang}"/></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="/controller?command=show_schedule"><fmt:message key="schedule" bundle="${lang}"/></a>
                    </li>
                </ul>
            </li>
        </ul>
        <div id="admin-nav-content">
            <div id="admin-ehl-content">
                <h3 align="center" class="registration-head"><fmt:message key="schedule_visit"
                                                                          bundle="${lang}"/></h3><br>

                <form class="form-horizontal" action="/controller" method="POST">
                    <table class="table table-bordered">
                        <core:forEach items="${sessionScope.schedule}" var="entry">
                            <tr>
                                <td>
                                    <b><fmt:message key="${entry.key}" bundle="${lang}"/></b>
                                </td>
                                <core:if test="${not empty entry.value}">
                                    <td>
                                        <core:out value="${entry.value.timeFrom}-${entry.value.timeTo}"></core:out>
                                    </td>
                                </core:if>
                                <core:if test="${empty entry.value}">
                                    <td>
                                        <core:out value="-"></core:out>
                                    </td>
                                </core:if>
                            </tr>
                        </core:forEach>
                    </table>
                </form>
            </div>
        </div>
    </div>

    <!-- Footer
    ================================================== -->
    <footer class="footer">
        <div class="container">
            <jstlpg:foot title='Medical Center'/>
        </div>
    </footer>
    <br/>
</div>
<!-- /container -->
<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="js/boot.js"></script>
</body>
</html>