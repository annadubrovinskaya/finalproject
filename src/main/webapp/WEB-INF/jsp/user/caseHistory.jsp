<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tld/taglib.tld" prefix="jstlpg" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <core:set var="language"
              value="${not empty param.language ? param.language : not empty language ? language : 'ru'}"
              scope="session"/>
    <fmt:setLocale value="${language}"/>
    <fmt:setBundle basename="localization.hospital" var="lang"/>
    <meta charset="utf-8">
    <title>Patient's case history</title>
    <!-- Le styles -->
    <link href="css/boot.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
</head>

<body>
<div class="container">
    <!-- Navbar
      ================================================== -->
    <div class="navbar">
        <div class="container">
            <div class="row">
                <h1 class="loadhome span3"><img src="images/logo.png" alt="Medical Center"/></h1>

                <div class="nav-collapse collapse"><br/>
                    <ul class="nav pull-right">
                        <li><a href="/indexUser"><fmt:message key="home" bundle="${lang}"/></a></li>
                        <li><a href="/controller?command=logout"><fmt:message key="logout" bundle="${lang}"/></a></li>
                        <li class="dropdown">
                            <a href="/caseHistory" class="dropdown-toggle" data-toggle="dropdown" ><fmt:message key="setting" bundle="${lang}"/></a>
                            <ul class="dropdown-menu">
                                <li class="nav-header"><fmt:message key="language" bundle="${lang}"/></li>
                                <li class="divider"></li>
                                <li><a href="/patientCard?language=en_US"><fmt:message key="english"
                                                                                     bundle="${lang}"/></a>
                                </li>
                                <li><a href="/patientCard?language=ru_RU"><fmt:message key="russian"
                                                                                     bundle="${lang}"/></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="label label-info" style="display:block;"></div>
    <!-- ================Carousel and pages======= -->
    <div id="admin-page-body_caseHistory">
        <br>
        <ul class="nav nav-tabs">
            <li role="presentation">
                <a href="/indexUser" class="btnB btnB-default menu-btn"><fmt:message key="patient_inspection"
                                                                                     bundle="${lang}"/></a>
            </li>
            <li role="presentation">
                <a href="/appointment" class="btnB btnB-default menu-btn"><fmt:message key="patient_appointments"
                                                                                       bundle="${lang}"/></a>
            </li>
            <li role="presentation">
                <a href="/patientCard" class="btnB btnB-default active-button"><fmt:message key="patient_card"
                                                                                       bundle="${lang}"/></a>
            </li>
            <li class="dropdown">
                <a href="/indexUser" class="btnB btnB-default" class="dropdown-toggle"
                   data-toggle="dropdown"><fmt:message key="data_view" bundle="${lang}"/></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="/dvDoctor"><fmt:message key="patients" bundle="${lang}"/></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="/controller?command=show_schedule"><fmt:message key="schedule" bundle="${lang}"/></a>
                    </li>
                </ul>
            </li>
        </ul>
        <div id="admin-nav-content">
            <div id="admin-ehl-content">
                <h3 align="center" class="registration-head"><fmt:message key="patient_card"
                                                                          bundle="${lang}"/></h3><br>

                <form class="form-horizontal" >
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="card_number"><fmt:message key="card_number"
                                                                                          bundle="${lang}"/>:</label>
                        <div class="col-xs-9">
                            <input type="text" class="form-control" id="card_number" name="card_number"
                                   value="${requestScope.medical_card.id}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="name"><fmt:message key="patient.name"
                                                                                          bundle="${lang}"/>:</label>
                        <div class="col-xs-9">
                            <input type="text" class="form-control" id="name" name="name"
                                   value="${requestScope.patient.name}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="second_name"><fmt:message key="patient.second_name"
                                                                                           bundle="${lang}"/>:</label>

                        <div class="col-xs-9">
                            <input type="text" class="form-control" id="second_name" name="second_name"
                                   value="${requestScope.patient.secondName}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="patronymic"><fmt:message key="patient.patronymic"
                                                                                            bundle="${lang}"/>:</label>

                        <div class="col-xs-9">
                            <input type="text" class="form-control" id="patronymic" name="patronymic"
                                   value="${requestScope.patient.patronymic}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="inputDate"><fmt:message key="patient.date_birth" bundle="${lang}"/>:</label>
                        <div class="col-xs-3">
                            <input type="date" id="inputDate" class="form-control" name="date"
                                   value="${requestScope.patient.dateOfBirth}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="height"><fmt:message key="patient.height"
                                                                                            bundle="${lang}"/>:</label>

                        <div class="col-xs-9">
                            <input type="text" class="form-control" id="height" name="height"
                                   value="${requestScope.medical_card.heightPatient}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="weight"><fmt:message key="patient.weight"
                                                                                        bundle="${lang}"/>:</label>

                        <div class="col-xs-9">
                            <input type="text" class="form-control" id="weight" name="weight"
                                   value="${requestScope.medical_card.weightPatient}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="bloodType"><fmt:message key="patient.blood_type"
                                                                                        bundle="${lang}"/>:</label>

                        <div class="col-xs-9">
                            <input type="text" class="form-control" id="bloodType" name="bloodType"
                                   value="${requestScope.medical_card.bloodType}" readonly>
                        </div>
                    </div>
                    <br/>

                    <h4 align="center" class="registration-head"><fmt:message key="case_history"
                                                                              bundle="${lang}"/></h4><br>
                    <div class="form-group" id="scroll-table-ch">
                        <table class="table table-hover">
                            <tr>
                                <td><fmt:message key="date" bundle="${lang}"/></td>
                                <td><fmt:message key="doctor" bundle="${lang}"/></td>
                                <td><fmt:message key="speciality" bundle="${lang}"/></td>
                                <td><fmt:message key="conclusion" bundle="${lang}"/></td>
                                <td><fmt:message key="diagnosis" bundle="${lang}"/></td>
                            </tr>
                            <core:forEach items="${requestScope.inspection}"  var="inspection">
                                <tr>
                                    <td><core:out value="${inspection.date}"/></td>
                                    <td><core:out value="${inspection.secondNameDoctor} ${inspection.nameDoctor} ${inspection.patronymicDoctor}"/></td>
                                    <td><core:out value="${inspection.speciality}"/></td>
                                    <td><core:out value="${inspection.conclusion}"/></td>
                                    <td><core:out value="${inspection.diagnosis}"/></td>
                                </tr>
                            </core:forEach>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Footer
    ================================================== -->
    <footer class="footer">
        <div class="container">
            <jstlpg:foot title='Medical Center'/>
        </div>
    </footer>
    <br/>
</div>
<!-- /container -->
<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="js/boot.js"></script>
</body>
</html>