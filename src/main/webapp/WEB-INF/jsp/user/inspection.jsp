<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tld/taglib.tld" prefix="jstlpg" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <core:set var="language"
              value="${not empty param.language ? param.language : not empty language ? language : 'ru'}"
              scope="session"/>
    <fmt:setLocale value="${language}"/>
    <fmt:setBundle basename="localization.hospital" var="lang"/>
    <meta charset="utf-8">
    <title>Inspection patient</title>
    <!-- Le styles -->
    <link href="css/boot.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
</head>

<body>
<div class="container">
    <!-- Navbar
      ================================================== -->
    <div class="navbar">
        <div class="container">
            <div class="row">
                <h1 class="loadhome span3"><img src="images/logo.png" alt="Medical Center"/></h1>

                <div class="nav-collapse collapse"><br/>
                    <ul class="nav pull-right">
                        <li><a href="/indexUser"><fmt:message key="home" bundle="${lang}"/></a></li>
                        <li><a href="/controller?command=logout"><fmt:message key="logout" bundle="${lang}"/></a></li>
                        <li class="dropdown">
                            <a href="/inspection" class="dropdown-toggle" data-toggle="dropdown"><fmt:message
                                    key="setting" bundle="${lang}"/></a>
                            <ul class="dropdown-menu">
                                <li class="nav-header"><fmt:message key="language" bundle="${lang}"/></li>
                                <li class="divider"></li>
                                <li><a href="/inspection?language=en_US"><fmt:message key="english"
                                                                                      bundle="${lang}"/></a>
                                </li>
                                <li><a href="/inspection?language=ru_RU"><fmt:message key="russian"
                                                                                      bundle="${lang}"/></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="label label-info" style="display:block;"></div>
    <!-- ================Carousel and pages======= -->
    <div id="admin-page-body">
        <br>
        <ul class="nav nav-tabs">
            <li role="presentation">
                <a href="/indexUser" class="btnB btnB-default active-button"><fmt:message key="patient_inspection"
                                                                                          bundle="${lang}"/></a>
            </li>
            <li role="presentation">
                <a href="/appointment" class="btnB btnB-default menu-btn"><fmt:message key="patient_appointments"
                                                                                       bundle="${lang}"/></a>
            </li>
            <li role="presentation">
                <a href="/patientCard" class="btnB btnB-default menu-btn"><fmt:message key="patient_card"
                                                                                       bundle="${lang}"/></a>
            </li>
            <li class="dropdown">
                <a href="/indexUser" class="btnB btnB-default" class="dropdown-toggle"
                   data-toggle="dropdown"><fmt:message key="data_view" bundle="${lang}"/></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="/dvDoctor"><fmt:message key="patients" bundle="${lang}"/></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="/controller?command=show_schedule"><fmt:message key="schedule" bundle="${lang}"/></a>
                    </li>
                </ul>
            </li>
        </ul>
        <div id="admin-nav-content">
            <div id="admin-ehl-content">
                <h3 align="center" class="registration-head"><fmt:message key="patient_inspection"
                                                                          bundle="${lang}"/></h3><br>


                <form class="form-horizontal" action="/controller" method="POST">
                    <core:if test="${empty requestScope.success}">
                        <div class="form-group">
                            <label class="control-label col-xs-3" for="ticket_number"><fmt:message key="ticket_number"
                                                                                                   bundle="${lang}"/>:</label>

                            <div class="col-xs-9">
                                <input type="text" class="form-control" id="ticket_number" name="ticket_number"
                                       value="${sessionScope.appointment.id}" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-3" for="lastName"><fmt:message key="last_name"
                                                                                              bundle="${lang}"/>:</label>

                            <div class="col-xs-9">
                                <input type="text" class="form-control" id="lastName" name="lastName"
                                       value="${sessionScope.appointment.secondNamePatient}" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-3" for="firstName"><fmt:message key="name_user"
                                                                                               bundle="${lang}"/>:</label>

                            <div class="col-xs-9">
                                <input type="text" class="form-control" id="firstName" name="firstName"
                                       value="${sessionScope.appointment.namePatient}" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-3" for="patronymic"><fmt:message key="patronymic"
                                                                                                bundle="${lang}"/>:</label>

                            <div class="col-xs-9">
                                <input type="text" class="form-control" id="patronymic" name="patronymic"
                                       value="${sessionScope.appointment.patronymicPatient}" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-3"><fmt:message key="date" bundle="${lang}"/>:</label>

                            <div class="col-xs-3">
                                <input type="date" id="inputDate" class="form-control" name="date"
                                       value="${sessionScope.appointment.date}" readonly>
                            </div>
                        </div>
                        <core:if test="${sessionScope.speciality eq 'therapist'}">
                            <div class="form-group">
                                <label class="control-label col-xs-3"><fmt:message key="height"
                                                                                   bundle="${lang}"/>:</label>

                                <div class="col-xs-3">
                                    <input type="text" class="form-control" name="height" pattern="^[0-9].?[0-9]?$"
                                           value="${requestScope.medical_card.heightPatient}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-3"><fmt:message key="weight"
                                                                                   bundle="${lang}"/>:</label>

                                <div class="col-xs-3">
                                    <input type="text" class="form-control" name="weight" pattern="^[0-9]+.?[0-9]?$"
                                           value="${requestScope.medical_card.weightPatient}">
                                </div>
                            </div>
                        </core:if>
                        <div class="form-group">
                            <label class="control-label col-xs-3"><fmt:message key="conclusion"
                                                                               bundle="${lang}"/>:</label>

                            <div class="col-xs-3">
                                <textarea id="conclusion" class="form-control" name="conclusion" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-3" for="diagnosis"><fmt:message key="diagnosis"
                                                                                               bundle="${lang}"/>:</label>

                            <div class="col-xs-9">
                                <select class="form-control" id="diagnosis" name="diagnosis"
                                        value="${sessionScope.diagnosis}">
                                    <core:forEach items="${sessionScope.diagnosis}" var="diagnosis">
                                        <option><core:out value="${diagnosis.name}"/></option>
                                    </core:forEach>
                                </select>
                            </div>
                        </div>
                    </core:if>
                    <br>
                    <table align="center">
                        <core:if test="${not empty requestScope.error}">
                            <tr>
                                <td colspan="2" class="error">
                                    <fmt:message key="${requestScope.error}" bundle="${lang}"/>
                                </td>
                            </tr>
                        </core:if>
                        <core:if test="${not empty requestScope.success}">
                            <tr>
                                <td colspan="2" class="success">
                                    <fmt:message key="success.inspection" bundle="${lang}"/>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <a href="/indexUser"><fmt:message key="back" bundle="${lang}"/></a>
                                    <%--<input type="hidden" name="command" value="forward_appointment">--%>
                                    <%--<input type="submit" class="btn btn-primary"--%>
                                           <%--value="<fmt:message key="back" bundle="${lang}"/>">--%>
                                </td>
                            </tr>
                        </core:if>
                        <core:if test="${empty requestScope.success}">
                            <tr>
                                <td align="center">
                                    <input type="hidden" name="command" value="save_inspection">
                                    <input type="submit" class="btn btn-primary"
                                           value="<fmt:message key="save" bundle="${lang}"/>">
                                </td>
                            </tr>
                        </core:if>
                    </table>
                </form>
            </div>
        </div>
    </div>

    <!-- Footer
    ================================================== -->
    <footer class="footer">
        <div class="container">
            <jstlpg:foot title='Medical Center'/>
        </div>
    </footer>
    <br/>
</div>
<!-- /container -->
<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="js/boot.js"></script>
</body>
</html>