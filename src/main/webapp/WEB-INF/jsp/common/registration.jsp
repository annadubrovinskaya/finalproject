<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tld/taglib.tld" prefix="jstlpg" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <core:set var="language"
              value="${not empty param.language ? param.language : not empty language ? language : 'ru'}"
              scope="session"/>
    <fmt:setLocale value="${language}"/>
    <fmt:setBundle basename="localization.hospital" var="lang"/>
    <meta charset="utf-8">
    <title>Registration user</title>
    <!-- Le styles -->
    <link href="css/boot.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
</head>

<body>
<div class="container">
    <!-- Navbar
      ================================================== -->
    <div class="navbar">
        <div class="container">
            <div class="row">
                <h1 class="loadhome span3"><img src="images/logo.png" alt="Medical Center"/></h1>

                <div class="nav-collapse collapse"><br/>
                    <ul class="nav pull-right">
                        <li><a href="/index.jsp"><fmt:message key="home" bundle="${lang}"/></a>
                        <li>
                            <a href="/authorization"><fmt:message key="login" bundle="${lang}"/></a>
                        </li>
                        <li class="dropdown">
                            <a href="/registration" class="dropdown-toggle" data-toggle="dropdown" ><fmt:message key="setting" bundle="${lang}"/></a>
                            <ul class="dropdown-menu">
                                <li class="nav-header"><fmt:message key="language" bundle="${lang}"/></li>
                                <li class="divider"></li>
                                <li><a href="/registration?language=en_US"><fmt:message key="english"
                                                                                        bundle="${lang}"/></a>
                                </li>
                                <li><a href="/registration?language=ru_RU"><fmt:message key="russian"
                                                                                        bundle="${lang}"/></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="label label-info" style="display:block;"></div>
    <!-- ================Carousel and pages======= -->
    <div id="registration-body">
        <div id="registration-form">
            <h2 align="center" class="registration-head"><fmt:message key="registration" bundle="${lang}"/></h2><br>

            <form class="form-horizontal" action="/controller" method="POST">
                <div class="form-group">
                    <label class="control-label col-xs-3" for="lastName"><fmt:message key="last_name" bundle="${lang}"/>:</label>

                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="lastName" name="lastName"
                               value="${requestScope.lastName}" pattern="^[a-zA-Z-]{3,25}$"
                               placeholder="<fmt:message key="input_last_name" bundle="${lang}"/>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-3" for="firstName"><fmt:message key="name_user"
                                                                                       bundle="${lang}"/>:</label>

                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="firstName" name="firstName"
                               value="${requestScope.firstName}" pattern="^[a-zA-Z]{2,20}$"
                               placeholder="<fmt:message key="input_name" bundle="${lang}"/>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-3" for="patronymic"><fmt:message key="patronymic"
                                                                                        bundle="${lang}"/>:</label>

                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="patronymic" name="patronymic"
                               value="${requestScope.patronymic}" pattern="^[a-zA-Z-]{4,25}$"
                               placeholder="<fmt:message key="input_patronymic" bundle="${lang}"/>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-3"><fmt:message key="date_birth" bundle="${lang}"/>:</label>
                    <div class="col-xs-3">
                        <input type="date" id="inputDate" class="form-control" name="date"
                               max="${sessionScope.maxDateBirthDoctor}"
                               min="${sessionScope.minBirthDoctor}" value="${requestScope.date}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-3" for="inputLogin"><fmt:message key="login_user"
                                                                                        bundle="${lang}"/>:</label>
                    <div class="col-xs-9">
                        <input type="text" class="form-control" id="inputLogin" name="login"
                               value="${requestScope.login}" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$"
                               placeholder="<fmt:message key="input_login_user" bundle="${lang}"/>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-3" for="inputPassword"><fmt:message key="password"
                                                                                           bundle="${lang}"/>:</label>
                    <div class="col-xs-9">
                        <input type="password" class="form-control" id="inputPassword" name="password"
                               value="${requestScope.password}" pattern="^(?=.*[a-z])(?=.*[A-Z]).*$"
                               placeholder="<fmt:message key="input_password" bundle="${lang}"/>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-3" for="roomNumber"><fmt:message key="room_number"
                                                                                        bundle="${lang}"/>:</label>

                    <div class="col-xs-9">
                        <input type="number" class="form-control" id="roomNumber" name="roomNumber"
                               value="${requestScope.roomNumber}" min="1" max="100000"
                               placeholder="<fmt:message key="input_room_number" bundle="${lang}"/>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-3" for="speciality"><fmt:message key="speciality"
                                                                                        bundle="${lang}"/>:</label>

                    <div class="col-xs-9">
                        <select class="form-control" id="speciality" name="speciality"
                                value="${requestScope.speciality}">
                            <core:forEach items="${sessionScope.specialities}" var="speciality">
                                <option><core:out value="${speciality.name}"/></option>
                            </core:forEach>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-xs-3"><fmt:message key="sex" bundle="${lang}"/>:</label>

                    <div class="col-xs-2">
                        <label class="radio-inline">
                            <input type="radio" name="sex" value="men" checked> <fmt:message key="sex.men"
                                                                                             bundle="${lang}"/>
                        </label>
                    </div>
                    <div class="col-xs-2">
                        <label class="radio-inline">
                            <input type="radio" name="sex" value="women"> <fmt:message key="sex.women"
                                                                                       bundle="${lang}"/>
                        </label>
                    </div>
                </div>
                <br/>
                <div class="form-group">
                    <table align="center">
                        <core:if test="${not empty requestScope.error}">
                            <tr>
                                <td colspan="2" class="error">
                                    <fmt:message key="${requestScope.error}" bundle="${lang}"/>
                                </td>
                            </tr>
                        </core:if>
                        <tr>
                            <td align="center">
                                <input type="hidden" name="command" value="check_in">
                                <input type="submit" class="btn btn-primary"
                                       value="<fmt:message key="registration" bundle="${lang}"/>">
                            </td>
                        </tr>
                    </table>
                </div>
            </form>
        </div>
    </div>
    <!-- Footer
    ================================================== -->
    <footer class="footer">
        <div class="container">
            <jstlpg:foot title='Medical Center'/>
        </div>
    </footer>
    <br/>
</div>
<!-- /container -->
<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="js/boot.js"></script>
</body>
</html>