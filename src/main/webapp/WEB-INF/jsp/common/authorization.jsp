<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tld/taglib.tld" prefix="jstlpg" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <core:set var="language"
              value="${not empty param.language ? param.language : not empty language ? language : 'ru'}"
              scope="session"/>
    <fmt:setLocale value="${language}"/>
    <fmt:setBundle basename="localization.hospital" var="lang"/>
    <meta charset="utf-8">
    <title>Authorization user</title>
    <!-- Le styles -->
    <link href="css/boot.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
</head>

<body>
<div class="container">
    <!-- Navbar
      ================================================== -->
    <div class="navbar">
        <div class="container">
            <div class="row">
                <h1 class="loadhome span3"><img src="images/logo.png" alt="Medical Center"/></h1>

                <div class="nav-collapse collapse"><br/>
                    <ul class="nav pull-right">
                        <li><a href="/index.jsp"><fmt:message key="home" bundle="${lang}"/></a>
                        <li>
                        <a href="/controller?command=registration"><fmt:message key="registration" bundle="${lang}"/></a>
                        <%--<form action="/controller" method="GET">--%>
                            <%--<input type="submit" class="btnB btnB-default"--%>
                                   <%--value="<fmt:message key="registration" bundle="${lang}"/>"/>--%>
                            <%--<input type="hidden" name="command" value="registration">--%>
                        <%--</form>--%>
                        </li>
                        <li class="dropdown">
                            <a href="/authorization" class="dropdown-toggle" data-toggle="dropdown" ><fmt:message key="setting" bundle="${lang}"/></a>
                            <ul class="dropdown-menu">
                                <li class="nav-header"><fmt:message key="language" bundle="${lang}"/></li>
                                <li class="divider"></li>
                                <li><a href="/authorization?language=en_US"><fmt:message key="english" bundle="${lang}"/></a>
                                </li>
                                <li><a href="/authorization?language=ru_RU"><fmt:message key="russian" bundle="${lang}"/></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="label label-info" style="display:block;"></div>
    <!-- ================Carousel and pages======= -->
    <div id="authorization-body">
        <div id="authorization-form">
            <form action="/controller" method="POST">
                <h2><fmt:message key="sign_in_message" bundle="${lang}"/></h2><br>

                <input type="text" id="inputLogin" class="form-control" name="login" pattern="^[a-zA-Z][a-zA-Z0-9-_\\.]{1,20}$"
                       placeholder="<fmt:message key="sign_in.login" bundle="${lang}"/>" autofocus=""> <br>

                <input type="password" class="form-control" id="inputPassword" name="password" pattern="^(?=.*[a-z])(?=.*[A-Z]).*$"
                       placeholder="<fmt:message key="input_password" bundle="${lang}"/>"> <br>


                <table align="center">
                    <tr>
                        <core:if test="${not empty requestScope.error}">
                            <td class="error">
                                <fmt:message key="${requestScope.error}" bundle="${lang}"/>
                            </td>
                        </core:if>
                    </tr>
                    <tr>
                        <td>
                            <button class="btnB btnB-primary" type="submit"><fmt:message key="sign_in" bundle="${lang}"/></button>
                            <input type="hidden" name="command" value="authorization">
                            <input type="hidden" name="secure_param" value="${requestScope.secure_param}">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </div>

    <!-- Footer
    ================================================== -->
    <footer class="footer">
        <div class="container">
            <jstlpg:foot title='Medical Center'/>
        </div>
    </footer>
    <br/>
</div>
<!-- /container -->
<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="js/boot.js"></script>
</body>
</html>