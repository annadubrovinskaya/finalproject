<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tld/taglib.tld" prefix="jstlpg" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <core:set var="language"
              value="${not empty param.language ? param.language : not empty language ? language : 'ru'}"
              scope="session"/>
    <fmt:setLocale value="${language}"/>
    <fmt:setBundle basename="localization.hospital" var="lang"/>
    <meta charset="utf-8">
    <title>Doctor schedule</title>
    <!-- Le styles -->
    <link href="css/boot.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
</head>

<body>
<div class="container">
    <!-- Navbar
      ================================================== -->
    <div class="navbar">
        <div class="container">
            <div class="row">
                <h1 class="loadhome span3"><img src="images/logo.png" alt="Medical Center"/></h1>

                <div class="nav-collapse collapse"><br/>
                    <ul class="nav pull-right">
                        <li><a href="/indexAdmin"><fmt:message key="home" bundle="${lang}"/></a></li>
                        <li><a href="/controller?command=logout"><fmt:message key="logout" bundle="${lang}"/></a></li>
                        <li class="dropdown">
                            <a href="/doctorSchedule" class="dropdown-toggle" data-toggle="dropdown" ><fmt:message key="setting" bundle="${lang}"/></a>
                            <ul class="dropdown-menu">
                                <li class="nav-header"><fmt:message key="language" bundle="${lang}"/></li>
                                <li class="divider"></li>
                                <li><a href="/doctorSchedule?language=en_US"><fmt:message key="english"
                                                                                           bundle="${lang}"/></a>
                                </li>
                                <li><a href="/doctorSchedule?language=ru_RU"><fmt:message key="russian"
                                                                                           bundle="${lang}"/></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="label label-info" style="display:block;"></div>
    <!-- ================Carousel and pages======= -->
    <div id="admin-page-body">
        <br>
        <ul class="nav nav-tabs">
            <li class="dropdown">
                <a href="/indexAdmin" class="btnB btnB-default" class="dropdown-toggle"
                   data-toggle="dropdown"><fmt:message key="patient_card" bundle="${lang}"/></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="/indexAdmin" class="btnB btnB-default menu-btn"><fmt:message key="fill"
                                                                                              bundle="${lang}"/></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="/cardDelete" class="btnB btnB-default menu-btn"><fmt:message key="delete"
                                                                                              bundle="${lang}"/></a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="/indexAdmin" class="btnB btnB-default active-button" class="dropdown-toggle"
                   data-toggle="dropdown"><fmt:message key="schedule_of_doctors" bundle="${lang}"/></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="/controller?command=forward_schedule" class="btnB btnB-default menu-btn">
                            <fmt:message key="create" bundle="${lang}"/></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="/controller?command=forward_edit_schedule" class="btnB btnB-default menu-btn">
                            <fmt:message key="edit" bundle="${lang}"/></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="/controller?command=forward_doc_schedule" class="btnB btnB-default menu-btn">
                            <fmt:message key="show" bundle="${lang}"/></a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="/indexAdmin" class="btnB btnB-default menu-btn" class="dropdown-toggle"
                   data-toggle="dropdown"><fmt:message key="appointments" bundle="${lang}"/></a>
                <ul class="dropdown-menu">
                    <li class="nav-header"><fmt:message key="appointments" bundle="${lang}"/></li>
                    <li class="divider"></li>
                    <li>
                        <a href="/appointmentSpeciality" class="btnB btnB-default menu-btn">
                            <fmt:message key="appointments.speciality" bundle="${lang}"/></a>
                    </li>
                    <li class="divider"></li>
                    <li class="nav-header"><fmt:message key="appointments.cancel" bundle="${lang}"/></li>
                    <li class="divider"></li>
                    <li>
                        <a href="/appointmentCancel" class="btnB btnB-default menu-btn">
                            <fmt:message key="appointments.cancel" bundle="${lang}"/></a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="/indexAdmin" class="btnB btnB-default menu-btn" class="dropdown-toggle"
                   data-toggle="dropdown"><fmt:message key="data_view" bundle="${lang}"/></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="/dataViewDoctor" class="btnB btnB-default menu-btn">
                            <fmt:message key="dw.doctor" bundle="${lang}"/></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="/dataViewPatient" class="btnB btnB-default menu-btn">
                            <fmt:message key="dw.patient" bundle="${lang}"/></a>
                    </li>
                </ul>
            </li>
        </ul>
        <div id="admin-nav-content">
            <div id="admin-ehl-content">
                <h3 align="center" class="registration-head"><fmt:message key="create_schedule" bundle="${lang}"/></h3>
                <br>

                <form class="form-horizontal" action="/controller" method="POST">
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="doctor"><fmt:message key="doctor"
                                                                                        bundle="${lang}"/>:</label>

                        <div class="col-xs-9">
                            <select class="form-control" id="doctor" name="doctor">
                                <core:forEach items="${sessionScope.doctors}" var="doctor">
                                    <option><core:out
                                            value="${doctor.secondName} ${doctor.name} ${doctor.patronymic} ${doctor.speciality}"/></option>
                                </core:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" for="monday"><fmt:message key="monday"
                                                                                             bundle="${lang}"/>:</label>
                        <div class="col-xs-9">
                            <input type="text" class="form-control" id="monday" name="monday"
                                   pattern="^(([0,1][0-9])|(2[0-3])):00-(([0,1][0-9])|(2[0-3])):00$"
                                   placeholder="<fmt:message key="time" bundle="${lang}"/>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" for="tuesday"><fmt:message key="tuesday"
                                                                                         bundle="${lang}"/>:</label>

                        <div class="col-xs-9">
                            <input type="text" class="form-control" id="tuesday" name="tuesday"
                                   pattern="^(([0,1][0-9])|(2[0-3])):00-(([0,1][0-9])|(2[0-3])):00$"
                                   placeholder="<fmt:message key="time" bundle="${lang}"/>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" for="wednesday"><fmt:message key="wednesday"
                                                                                           bundle="${lang}"/>:</label>

                        <div class="col-xs-9">
                            <input type="text" class="form-control" id="wednesday" name="wednesday"
                                   pattern="^(([0,1][0-9])|(2[0-3])):00-(([0,1][0-9])|(2[0-3])):00$"
                                   placeholder="<fmt:message key="time" bundle="${lang}"/>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" for="thursday"><fmt:message key="thursday"
                                                                                          bundle="${lang}"/>:</label>

                        <div class="col-xs-9">
                            <input type="text" class="form-control" id="thursday" name="thursday"
                                   pattern="^(([0,1][0-9])|(2[0-3])):00-(([0,1][0-9])|(2[0-3])):00$"
                                   placeholder="<fmt:message key="time" bundle="${lang}"/>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" for="friday"><fmt:message key="friday"
                                                                                        bundle="${lang}"/>:</label>

                        <div class="col-xs-9">
                            <input type="text" class="form-control" id="friday" name="friday"
                                   pattern="^(([0,1][0-9])|(2[0-3])):00-(([0,1][0-9])|(2[0-3])):00$"
                                   placeholder="<fmt:message key="time" bundle="${lang}"/>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" for="saturday"><fmt:message key="saturday"
                                                                                          bundle="${lang}"/>:</label>

                        <div class="col-xs-9">
                            <input type="text" class="form-control" id="saturday" name="saturday"
                                   pattern="^(([0,1][0-9])|(2[0-3])):00-(([0,1][0-9])|(2[0-3])):00$"
                                   placeholder="<fmt:message key="time" bundle="${lang}"/>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" for="sunday"><fmt:message key="sunday"
                                                                                        bundle="${lang}"/>:</label>

                        <div class="col-xs-9">
                            <input type="text" class="form-control" id="sunday" name="sunday"
                                   pattern="^(([0,1][0-9])|(2[0-3])):00-(([0,1][0-9])|(2[0-3])):00$"
                                   placeholder="<fmt:message key="time" bundle="${lang}"/>">
                        </div>
                    </div>

                    <br>
                    <table align="center">
                        <core:if test="${not empty requestScope.error}">
                            <tr>
                                <td colspan="2" class="error">
                                    <fmt:message key="${requestScope.error}" bundle="${lang}"/>
                                </td>
                            </tr>
                        </core:if>
                        <core:if test="${not empty requestScope.success}">
                            <tr>
                                <td colspan="2" class="success">
                                    <fmt:message key="success.create_schedule" bundle="${lang}"/>
                                </td>
                            </tr>
                        </core:if>
                        <tr>
                            <td align="center">
                                <input type="hidden" name="command" value="create_schedule">
                                <input type="submit" class="btn btn-primary"
                                       value="<fmt:message key="save" bundle="${lang}"/>">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>

    <!-- Footer
    ================================================== -->
    <footer class="footer">
        <div class="container">
            <jstlpg:foot title='Medical Center'/>
        </div>
    </footer>
    <br/>
</div>
<!-- /container -->
<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="js/boot.js"></script>
</body>
</html>