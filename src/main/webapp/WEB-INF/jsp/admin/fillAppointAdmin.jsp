<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tld/taglib.tld" prefix="jstlpg" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <core:set var="language"
              value="${not empty param.language ? param.language : not empty language ? language : 'ru'}"
              scope="session"/>
    <fmt:setLocale value="${language}"/>
    <fmt:setBundle basename="localization.hospital" var="lang"/>
    <meta charset="utf-8">
    <title>Fill appointment</title>
    <!-- Le styles -->
    <link href="css/boot.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
</head>

<body>
<div class="container">
    <!-- Navbar
      ================================================== -->
    <div class="navbar">
        <div class="container">
            <div class="row">
                <h1 class="loadhome span3"><img src="images/logo.png" alt="Medical Center"/></h1>

                <div class="nav-collapse collapse"><br/>
                    <ul class="nav pull-right">
                        <li><a href="/indexAdmin"><fmt:message key="home" bundle="${lang}"/></a></li>
                        <li><a href="/controller?command=logout"><fmt:message key="logout" bundle="${lang}"/></a></li>
                        <li class="dropdown">
                            <a href="/fillAppointAdmin" class="dropdown-toggle" data-toggle="dropdown" ><fmt:message key="setting" bundle="${lang}"/></a>
                            <ul class="dropdown-menu">
                                <li class="nav-header"><fmt:message key="language" bundle="${lang}"/></li>
                                <li class="divider"></li>
                                <li><a href="/fillAppointAdmin?language=en_US"><fmt:message key="english"
                                                                                        bundle="${lang}"/></a>
                                </li>
                                <li><a href="/fillAppointAdmin?language=ru_RU"><fmt:message key="russian"
                                                                                        bundle="${lang}"/></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="label label-info" style="display:block;"></div>
    <!-- ================Carousel and pages======= -->
    <div id="admin-page-body">
        <br>
        <ul class="nav nav-tabs">
            <li class="dropdown">
                <a href="/indexAdmin" class="btnB btnB-default" class="dropdown-toggle"
                   data-toggle="dropdown"><fmt:message key="patient_card" bundle="${lang}"/></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="/indexAdmin" class="btnB btnB-default menu-btn"><fmt:message key="fill"
                                                                                              bundle="${lang}"/></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="/cardDelete" class="btnB btnB-default menu-btn"><fmt:message key="delete"
                                                                                              bundle="${lang}"/></a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="/indexAdmin" class="btnB btnB-default  menu-btn" class="dropdown-toggle"
                   data-toggle="dropdown"><fmt:message key="schedule_of_doctors" bundle="${lang}"/></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="/controller?command=forward_schedule" class="btnB btnB-default menu-btn">
                            <fmt:message key="create" bundle="${lang}"/></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="/controller?command=forward_edit_schedule" class="btnB btnB-default menu-btn">
                            <fmt:message key="edit" bundle="${lang}"/></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="/controller?command=forward_doc_schedule" class="btnB btnB-default menu-btn">
                            <fmt:message key="show" bundle="${lang}"/></a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="/indexAdmin" class="btnB btnB-default active-button" class="dropdown-toggle"
                   data-toggle="dropdown"><fmt:message key="appointments" bundle="${lang}"/></a>
                <ul class="dropdown-menu">
                    <li class="nav-header"><fmt:message key="appointments" bundle="${lang}"/></li>
                    <li class="divider"></li>
                    <li>
                        <a href="/appointmentSpeciality" class="btnB btnB-default menu-btn">
                            <fmt:message key="appointments.speciality" bundle="${lang}"/></a>
                    </li>
                    <li class="divider"></li>
                    <li class="nav-header"><fmt:message key="appointments.cancel" bundle="${lang}"/></li>
                    <li class="divider"></li>
                    <li>
                        <a href="/appointmentCancel" class="btnB btnB-default menu-btn">
                            <fmt:message key="appointments.cancel" bundle="${lang}"/></a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="/indexAdmin" class="btnB btnB-default menu-btn" class="dropdown-toggle"
                   data-toggle="dropdown"><fmt:message key="data_view" bundle="${lang}"/></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="/dataViewDoctor" class="btnB btnB-default menu-btn">
                            <fmt:message key="dw.doctor" bundle="${lang}"/></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="/dataViewPatient" class="btnB btnB-default menu-btn">
                            <fmt:message key="dw.patient" bundle="${lang}"/></a>
                    </li>
                </ul>
            </li>
        </ul>
        <div id="admin-nav-content">
            <div id="admin-ehl-content">
                <h3 align="center" class="registration-head"><fmt:message key="patient_appointments"
                                                                          bundle="${lang}"/></h3>
                <br>

                <form class="form-horizontal" action="/controller" method="POST">
                    <%--<core:if test="${not empty requestScope.available_time}">--%>
                    <div class="form-group">
                        <label class="control-label col-xs-3"><fmt:message key="doctor" bundle="${lang}"/>:</label>

                        <div class="col-xs-3">
                            <input class="form-control" id="disabledInput" type="text" name="doctor"
                                   value="${sessionScope.available_time.secondName} ${sessionScope.available_time.name} ${sessionScope.available_time.patronymic}"
                                   readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3"><fmt:message key="speciality" bundle="${lang}"/>:</label>

                        <div class="col-xs-3">
                            <input class="form-control" id="speciality" type="text" name="speciality"
                                   value="${sessionScope.available_time.speciality}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3"><fmt:message key="available_date"
                                                                           bundle="${lang}"/>:</label>

                        <div class="col-xs-3">
                            <input class="form-control" type="text" name="available_date"
                                   value="${sessionScope.available_time.availableDate}" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="available_time"><fmt:message key="available_time"
                                                                                                bundle="${lang}"/>:</label>

                        <div class="col-xs-9">
                            <select class="form-control" id="available_time" name="available_time">
                                <core:forEach items="${sessionScope.available_time.availableTime}"
                                              var="available_time">
                                    <option><core:out value="${available_time}"/></option>
                                </core:forEach>
                            </select>
                        </div>
                    </div>

                    <core:if test="${not empty requestScope.appointment.secondNamePatient}">
                        <div class="form-group">
                            <label class="control-label col-xs-3" for="lastName"><fmt:message key="last_name"
                                                                                              bundle="${lang}"/>:</label>

                            <div class="col-xs-9">
                                <input type="text" class="form-control" id="lastName" name="lastName"
                                       value="${requestScope.appointment.secondNamePatient}" readonly>
                            </div>
                        </div>
                    </core:if>
                    <core:if test="${empty requestScope.appointment.secondNamePatient}">
                        <div class="form-group">
                            <label class="control-label col-xs-3" for="lastNameEm"><fmt:message key="last_name"
                                                                                                bundle="${lang}"/>:</label>

                            <div class="col-xs-9">
                                <input type="text" class="form-control" id="lastNameEm" name="lastName"
                                       value="${requestScope.lastName}" pattern="^[a-zA-Z-]{3,25}$"
                                       placeholder="<fmt:message key="input_last_name" bundle="${lang}"/>">
                            </div>
                        </div>
                    </core:if>
                    <core:if test="${not empty requestScope.appointment.namePatient}">
                        <div class="form-group">
                            <label class="control-label col-xs-3" for="firstName"><fmt:message key="name_user"
                                                                                               bundle="${lang}"/>:</label>

                            <div class="col-xs-9">
                                <input type="text" class="form-control" id="firstName" name="firstName"
                                       value="${requestScope.appointment.namePatient}" readonly>
                            </div>
                        </div>
                    </core:if>
                    <core:if test="${empty requestScope.appointment.namePatient}">
                        <div class="form-group">
                            <label class="control-label col-xs-3" for="firstNameEm"><fmt:message key="name_user"
                                                                                                 bundle="${lang}"/>:</label>

                            <div class="col-xs-9">
                                <input type="text" class="form-control" id="firstNameEm" name="firstName"
                                       value="${requestScope.firstName}" pattern="^[a-zA-Z-]{2,25}$"
                                       placeholder="<fmt:message key="input_name" bundle="${lang}"/>">
                            </div>
                        </div>
                    </core:if>
                    <core:if test="${not empty requestScope.appointment.patronymicPatient}">
                        <div class="form-group">
                            <label class="control-label col-xs-3" for="patronymic"><fmt:message key="patronymic"
                                                                                                bundle="${lang}"/>:</label>

                            <div class="col-xs-9">
                                <input type="text" class="form-control" id="patronymic" name="patronymic"
                                       value="${requestScope.appointment.patronymicPatient}" readonly>
                            </div>
                        </div>
                    </core:if>
                    <core:if test="${empty requestScope.appointment.patronymicPatient}">
                        <div class="form-group">
                            <label class="control-label col-xs-3" for="patronymicEm"><fmt:message key="patronymic"
                                                                                                  bundle="${lang}"/>:</label>

                            <div class="col-xs-9">
                                <input type="text" class="form-control" id="patronymicEm" name="patronymic"
                                       value="${requestScope.patronymic}" pattern="^[a-zA-Z-]{4,25}$"
                                       placeholder="<fmt:message key="input_patronymic" bundle="${lang}"/>">
                            </div>
                        </div>
                    </core:if>
                    <div class="form-group">
                        <label class="control-label col-xs-3"><fmt:message key="date_birth" bundle="${lang}"/>:</label>

                        <div class="col-xs-3">
                            <input type="date" id="inputDate" class="form-control" name="dateBirthPatient"
                                   value="${requestScope.dateBirthPatient}" max="${sessionScope.maxPatBirthDate}"
                                   min="${sessionScope.minPatBirthDate}">
                        </div>
                    </div>
                    <br/>
                    <br>
                    <table align="center">
                        <core:if test="${not empty requestScope.error}">
                            <tr>
                                <td colspan="2" class="error">
                                    <fmt:message key="${requestScope.error}" bundle="${lang}"/>
                                </td>
                            </tr>
                        </core:if>
                        <tr>
                            <td align="center">
                                <input type="hidden" name="command" value="make_appointment">
                                <input type="submit" class="btn btn-primary"
                                       value="<fmt:message key="make_appointment" bundle="${lang}"/>">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>

    <!-- Footer
    ================================================== -->
    <footer class="footer">
        <div class="container">
            <jstlpg:foot title='Medical Center'/>
        </div>
    </footer>
    <br/>
</div>
<!-- /container -->
<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="js/boot.js"></script>
</body>
</html>