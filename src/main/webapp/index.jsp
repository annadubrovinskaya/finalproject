<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/tld/taglib.tld" prefix="jstlpg" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <core:set var="language"
              value="${not empty param.language ? param.language : not empty language ? language : 'ru'}"
              scope="session"/>
    <fmt:setLocale value="${language}"/>
    <fmt:setBundle basename="localization.hospital" var="lang"/>
    <meta charset="utf-8">
    <title>Medical Center</title>
    <!-- Le styles -->
    <link href="css/boot.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
</head>

<body>
<div class="container">
    <!-- Navbar
      ================================================== -->
    <div class="navbar">
        <div class="container">
            <div class="row">
                <h1 class="loadhome span3"><img src="images/logo.png" alt="Medical Center"/></h1>
                <div class="nav-collapse collapse"><br/>
                    <ul class="nav pull-right">
                        <li><a href="/index.jsp"><fmt:message key="home" bundle="${lang}"/></a>
                            <%--<input type="submit" class="btnB btnB-default" value="<fmt:message key="home" bundle="${lang}"/>"/>--%>
                        </li>
                        <li>
                            <a href="/authorization"><fmt:message key="login" bundle="${lang}"/></a>
                        </li>
                        <li>
                            <a href="/controller?command=registration"><fmt:message key="registration" bundle="${lang}"/></a>
                        </li>
                        <li class="dropdown">
                            <a href="/index.jsp" class="dropdown-toggle" data-toggle="dropdown" ><fmt:message key="setting" bundle="${lang}"/></a>
                            <ul class="dropdown-menu">
                                <li class="nav-header"><fmt:message key="language" bundle="${lang}"/></li>
                                <li class="divider"></li>
                                <li><a href="index.jsp?language=en_US"><fmt:message key="english" bundle="${lang}"/></a></li>
                                <li><a href="index.jsp?language=ru_RU"><fmt:message key="russian" bundle="${lang}"/></a></li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="label label-info" style="display:block;"></div>
    <!-- ================Carousel and pages======= -->
    <div id="myCarousel" class="carousel slide">
        <div class="carousel-inner">
            <div class="item active">
                <img src="images/1.png" alt=""/>
            </div>
            <div class="item">
               <img src="images/2.png" alt=""/>
                <div class="carousel-caption">
                    <h4>Your slogan goes here</h4>
                    <p>
                        Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                        Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.Nullam id dolor id nibh ultricies vehicula ut id elit.
                    </p>
                </div>
            </div>
            <div class="item">
                <img src="images/3.png" alt=""/>
            </div>
            <div class="item">
                <img src="images/4.png" alt=""/>
                <div class="carousel-caption">
                    <h4>Your slogan goes here</h4>
                    <p>
                        Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
                        Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.Nullam id dolor id nibh ultricies vehicula ut id elit.
                    </p>
                </div>
            </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">&lsaquo;</a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">&rsaquo;</a>
    </div>
    <h1><fmt:message key="welcome" bundle="${lang}"/>!</h1>
    <hr/>
    <p class="msg"><em>
        "I'm a paragraph. Click here to add your own text and edit me.I am a great place for you to tell a story and
        let your users know a little more about you. I'm a paragraph. Click here to add your own text and edit me.I am a great place for you to tell a story and
        let your users know a little more about you. I'm a paragraph. Click here to add your own text and edit me.I am a great place for you to tell a story and
        let your users know a little more about you."
        <br/><strong>-Kim Cleary </strong></em>
    </p>
    <hr/>
    <div class="row">
        <div class="span12">
            <ul id="boxWrap" class="thumbnails">
                <li class="span3">
                    <div class="thumbnail">
                        <div class="caption">
                            <img src="images/block1.jpg" alt="">
                            <h4>BLOCK ONE</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.<br/>
                            </p>
                            <a id="blockOne" class="btn btn-large">more</a>
                        </div>
                    </div>
                </li>
                <li class="span3">
                    <div class="thumbnail">
                        <div class="caption">
                            <img src="images/block2.jpg" alt=""/>
                            <h4>BLOCK TWO</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.<br/>
                            </p>
                            <a id="blockTwo" class="btn btn-large">more</a>
                        </div>
                    </div>
                </li>
                <li class="span3">
                    <div class="thumbnail">
                        <div class="caption">
                            <img src="images/block3.jpg" alt=""/>
                            <h4>BLOCK THREE</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.<br/>
                            </p>
                            <a id="blockThree" class="btn btn-large">more</a>
                        </div>
                    </div>
                </li>
                <li class="span3">
                    <div class="thumbnail">
                        <div class="caption">
                            <img src="images/block4.jpg" alt=""/>
                            <h4>BLOCK FOUR</h4>
                            <p>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.<br/>
                            </p>
                            <a id="blockFour" class="btn btn-large">more</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- Footer
    ================================================== -->
    <footer class="footer">
        <div class="container">
            <jstlpg:foot title='Medical Center'/>
        </div>
    </footer>
    <br/>
</div><!-- /container -->
<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="js/boot.js"></script>
<script>
    // carousel demo
    $('#myCarousel').carousel()
</script>
</body>
</html>